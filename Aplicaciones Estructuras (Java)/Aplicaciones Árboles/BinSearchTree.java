package com.company;

public class BinSearchTree {

    //ATRIBUTOS

    private String preorder = "", inorder = "", posorder = "";
    private boolean search = false;
    public Node root;

    //MÉTODOS Y CONSTRUCTORES

    //Constructores

    public BinSearchTree(){
        this.root = null;
    }

    public BinSearchTree(Object o, int id){
        this.root = new Node(null, o, null, id);
    }

    //Método IsEmpty

    public boolean IsEmpty(){
        if (root == null){
            return true;
        }else{
            return false;
        }
    }

    //Método Insert
    public boolean Insert(Object o, int id){
        if (!IsEmpty()){
            Node inode = root;
            while (inode!=null){
                if (inode.GetID()<=id){
                    if(inode.right != null){
                        inode = inode.right;
                    }else{
                        inode.right = new Node(null,o,null,id);
                        break;
                    }
                }else{
                    if(inode.left != null){
                        inode = inode.left;
                    }else{
                        inode.left = new Node(null,o,null,id);
                        break;
                    }
                }
            }
        }else{
            root = new Node(null,o,null,id);
        }
        return true;
    }

    //Métodos necesarios para realizar el search en el árbol binario de búsqueda
    private void Search (Node n, Object o){
        if (!IsEmpty()){
            if (n.GetObject().toString().compareTo(o.toString()) == 0){
                search = true;
            }
            if (n.right != null){
                if (n.right.GetObject().toString().compareTo(o.toString()) == 0){
                    search = true;
                }else{
                    Search(n.right, o);
                }
            }
            if (n.left != null){
                if (n.left.GetObject().toString().compareTo(o.toString()) == 0){
                    search = true;
                }else{
                    Search(n.left, o);
                }
            }
        }
    }

    public boolean SearchObject(Object o){
        Search(root,o);
        if (search == true){
            search = false; //Se vuelve a dejar la variable booleana en false para en un futuro poder volverla a usar
            return true;
        }else{
            return false;
        }
    }

    //Método remove
    public boolean Remove(int id){
        if (!IsEmpty()){
            Node inode = root;
            Node preInode = root;
            while(inode.GetID() != id){
               preInode = inode;
               if (inode.GetID()>id){
                   inode = inode.left;
               }else{
                   inode = inode.right;
               }
               if (inode == null){
                   return false;
               }
            }
            if (inode.right == null & inode.left== null){
                if (inode == root){
                    root = null;
                }else if(preInode.left == inode){
                    preInode.left = null;
                }else{
                    preInode.right = null;
                }
            }else if(inode.right == null){
                if (inode == root){
                    root = root.left;
                }else if (preInode.left == inode){
                    preInode.left = inode.left;
                }
                else{
                    preInode.right = inode.left;
                }
            }else if(inode.left == null){
                if (inode == root){
                    root = root.right;
                }else if (preInode.left == inode){
                    preInode.left = inode.right;
                }
                else{
                    preInode.right = inode.right;
                }
            }else{
                Node rePre = inode, re = inode, aux = inode.right;
                while (aux != null){
                    rePre = re;
                    re = aux;
                    aux = aux.left;
                }
                if (re != inode.right){
                    rePre.left = re.right;
                    re.right = inode.right;
                }

                if (inode==root){
                    root = re;
                }else if(preInode.left == inode){
                    preInode.left = re;
                }else{
                    preInode.right = re;
                }
                re.left = inode.left;
            }
        }else{
            return false;
        }
        return true;
    }

    //Métodos necesarios para realizar el preorder
    private void PreOrder(Node n){
        if (!IsEmpty()){
            preorder += (String) n.GetObject();
            if (n.left!=null){
                PreOrder(n.left);
            }
            if (n.right!=null){
                PreOrder(n.right);
            }
        }
    }

    public String GetPreOrder(){
        PreOrder(root);
        return preorder;
    }

    //Métodos necesarios para realizar el inorder
    private void InOrder(Node n){
        if (!IsEmpty()){
            if (n.left!=null){
                InOrder(n.left);
            }
            inorder += (String) n.GetObject();
            if (n.right!=null){
                InOrder(n.right);
            }
        }
    }

    public String GetInOrder(){
        InOrder(root);
        return inorder;
    }

    //Métodos necesarios para realizar el posorder
    private void PosOrder(Node n){
        if (!IsEmpty()){
            if (n.left!=null){
                PosOrder(n.left);
            }
            if (n.right!=null){
                PosOrder(n.right);
            }
            posorder += (String) n.GetObject();
        }
    }

    public String GetPosOrder(){
        PosOrder(root);
        return posorder;
    }
}