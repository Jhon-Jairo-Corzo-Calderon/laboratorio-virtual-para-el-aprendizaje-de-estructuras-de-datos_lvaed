package com.company;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        BinSearchTree arbol = new BinSearchTree(); // Árbol Binario
        ArrayList base = new ArrayList(); // ArrayList

        Scanner p = new Scanner(System.in);
        System.out.println("Digite el texto");
        String data = p.nextLine();
        String xd = "";
        for(int i = 0 ; i < data.length() ; i++){
            char u = data.charAt(i);
            if(u == ' ' || u == '.' || u== ';' || u ==',' || u == '!' || u == '¡'){ //Se separa las palabras por medio de estos caracteres
                base.add(xd);
                xd = "";
            }else{
                xd+=u;
            }
        }
        if(xd.length() != 0){ // Si al final del texto , este no posee un finalizador , el programa automaticamente lo agrega
            base.add(xd);
            xd="";
        }
        Set<Object> miSet = new HashSet<Object>(base);// Set para contar los elementos
        for(Object s: miSet){
            System.out.println(s + " " +Collections.frequency(base,s)); // Se identifican las palabras y su frecuencia dentro del texto
            int cont = Collections.frequency(base,s);
            arbol.Insert(s,cont); // Se agrega cada palabra al arbol basada en su frecuencia mediante el .insert
        }
        System.out.println("Su impresión en pre-order es" + " " + arbol.GetPreOrder());
    }
}