package com.company;

public class Cola implements ICola{

    private List lista = new List();

    @Override
    public void clear() {
        lista.clear();
    }

    @Override
    public boolean isEmpty() {
        return lista.isEmpty();
    }

    @Override
    public Object extract() {
        Object object = lista.head.getObject();
        lista.remove(lista.head);
        return object;
    }

    @Override
    public boolean insert(Object object) {
        lista.add(object);
        return true;
    }

    @Override
    public int size() {
        return lista.getSize();
    }

    @Override
    public boolean search(Object object) {
        try{
            if (lista.search(object).getObject().toString().equals(object.toString())){
                return true;
            }else {
                return false;
            }
        }catch (Exception e){
            return false;
        }

    }

    @Override
    public void sort() {
        lista.sortList();
    }

    @Override
    public void reverse() {
        List reverseList = new List();
        ListNode inode = new ListNode();
        inode = lista.tail;
        for (int i = lista.getSize(); i > 0; i-- ){
            reverseList.add(inode.getObject());
            inode = inode.previous;
        }
        lista = reverseList;
    }

    @Override
    public String toString(){
        return lista.recInicioFin();
    }
}
