package com.company;
import java.util.Random;
import java.util.Scanner;

public class Main {

    //Problema de Jose o Números De La Suerte (Ejercicio del libro de estructura de datos c++ , página 353 , basado en colas por listas//

    public static void main(String[] args) {

        Random r = new Random();
        Scanner p = new Scanner(System.in);

        //"Objetos" necesarios para la implementación

        System.out.println("Digite el tamaño de la lista");
        int tam = p.nextInt(); // Tamaño original de la cola

        Cola data = new Cola(); // Cola inicial
        Cola dataFinal = new Cola(); // Cola final (Números de la suerte)
        Cola dataTrash = new Cola(); // Cola basura

        for(int i = 0 ; i < tam ; i++){
            int Rd = r.nextInt(100000); // Se le agregan n (Tamaño ingresado por el usuario) numeros aleatorios
            data.insert(Rd);
        }

        System.out.println("Esta es la lista original"+ data.toString());
        int n = data.size();
        int n1;
        do{
            n1 = r.nextInt(n);
        }while (n1 < 1); // Se tiene que asegurar que n1 debe ser positivo

        while (n1<=n){

            for(int i = 0 ; i < n ; i++){
                if(i % n1 != 1){
                    dataFinal.insert(data.extract()); // Se le asigna los elementos "Suerte"
                }else {
                    dataTrash.insert(data.extract()); // Se le asignan los elementos multiplos n1 + 1 (Posiciones)
                }
            }

            // La importancia de las colas es que mediante el recorrido se almacenan los elementos desde la posición 0 hasta n posición de la cola para dividir cuales son los números "De suerte"

            n = data.size();
            while (n1 < 1){
                n1 = r.nextInt(n);
            }
        }
        System.out.println("Esta es la lista con los números de la suerte \n"+ dataFinal);
        System.out.println("Esta es la lista con los números excluidos \n"+ dataTrash); // Print final
    }
}
