package com.company;

public class Cola implements ICola{

    private int minPrioridad;
    private int limiteMaxPrioridad;
    private List lista = new List();

    public Cola(int minPrioridad, int limiteMaxPrioridad){
        this.minPrioridad = minPrioridad;
        this.limiteMaxPrioridad = limiteMaxPrioridad;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
            lista.add(new List());
        }
    }

    @Override
    public void clear() {
        lista.clear();
    }

    @Override
    public boolean isEmpty() {
        return lista.isEmpty();
    }

    @Override
    public Object extract() {
        ListNode inode = lista.head;
        for (int i=limiteMaxPrioridad+1; i>=minPrioridad; i--){
            List l = (List) inode.getObject();
            if (!l.isEmpty()){
                Object object = l.head.getObject();
                l.remove(l.head);
                return object;
            }else{
                inode = inode.next;
            }
        }
        return null;
    }

    @Override
    public boolean insert(Object object, int prioridad) {
        ListNode inode = lista.head;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
            if (i == prioridad){
                List l = (List) inode.getObject();
                l.add(object);
            }else{
                inode = inode.next;
            }
        }
        return true;
    }

    @Override
    public int size() {
        int cont = 0;
        ListNode inode = lista.head;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
            List l = (List) inode.getObject();
            cont += l.getSize();
            inode = inode.next;
        }
        return cont;
    }

    @Override
    public boolean search(Object object) {
        try{
            ListNode inode = lista.head;
            for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
                List l = (List) inode.getObject();
                if (l.search(object) != null){
                    return true;
                }else {
                    inode = inode.next;
                }
            }
            return false;
        }catch (Exception e){
            return false;
        }

    }

    @Override
    public void sort() {
        ListNode inode = lista.head;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
            List l = (List) inode.getObject();
            if (!l.isEmpty()){
                l.sortList();
            }
            inode = inode.next;
        }
    }

    @Override
    public void reverse() {
        List generalReverseList = new List() , l , reverseList;
        ListNode inode = lista.head ,inodel;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
            l = (List) inode.getObject();
            reverseList = new List();
            inodel = new ListNode();
            inodel = l.tail;
            for (int j = l.getSize(); j > 0; j-- ){
                reverseList.add(inodel.getObject());
                inodel = inodel.previous;
            }
            generalReverseList.add(reverseList);
            inode = inode.next;
        }
        lista = generalReverseList;
    }

    @Override
    public String toString(){
        String output = "";
        ListNode inode = lista.head;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--) {
            List l = (List) inode.getObject();
            output += "Prioridad " + i + " -> " + l.recInicioFin() + " / ";
            inode = inode.next;
        }
        return output;
        //return lista.recInicioFin();
    }
}
