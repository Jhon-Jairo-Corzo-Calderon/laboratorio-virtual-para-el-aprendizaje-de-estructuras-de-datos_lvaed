package com.company;

public class Main {

    public static void main(String[] args) {

        Cola dataHospital = new Cola(1, 5);

        //Data del hospital almacenada en la cola de prioridades//
        dataHospital.insert("Pedro Alvares Rincon", 1);
        dataHospital.insert("Angela Lopez Martinez", 4);
        dataHospital.insert("Ivan Sanches Obrador ", 2);
        dataHospital.insert("Jose Perez Labrador", 5);
        dataHospital.insert("Joey Suarez Días", 5);
        dataHospital.insert("Leo Messi Pilar", 2);
        dataHospital.insert("Dopa Jinx Poe", 3);
        dataHospital.insert("Alistar Eduardo perez", 1);
        dataHospital.insert("Ángela Suarez Guevara", 2);
        dataHospital.insert("Lucas Martines Suarez", 1);
        dataHospital.insert("Urgot Eduardo Perez", 3);
        dataHospital.insert("Ahri Juliana Corzo", 3);
        dataHospital.insert("Katarina Loer Slight", 2);
        dataHospital.insert("Luppy Perez", 2);
        dataHospital.insert("Sara Pilar Toro", 5);

        int cont = 0; // Contador para ver el #del paciente
        while (dataHospital.size()!=0){ // Recorre la cola hasta que la longitud de esta sea == 0
            cont++;
            System.out.println( "La paciente #" + cont + " " + "es" + " " +dataHospital.extract()); // Mediante el metodo .extract , se extrae el elemento con mayor prioridad que haya sido ingresado por orden
        }
    }
}
