package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner p = new Scanner(System.in);
        System.out.println("Digite su oracion");
        String data = p.nextLine(); // Texto u oración a analizar

        List alma = new List();

        String palabra = "";
        for(int i = 0 ; i < data.length() ; i++){ // Se recorre el String con la información
            char u = data.charAt(i);
            if (u == ' ' || u == '.' || u ==';' || u == ',' ){
                alma.add(palabra);
                palabra = "";
                //Almacena y separa las palabras cuando se encuentran los anteriores caracteres
            }else{
                palabra += u;
            }
        }
        if (palabra.length() != 0){ // Si al final el usuario no puso un finalizador (".") , esto asegura que esta palabra sea almacenada también
            alma.add(palabra);
        }

        System.out.println("La lista es la siguiente\n" + alma.toString());
        System.out.println("El tamaño de la lista es de \n" + alma.getSize()); // Impresión de la lista

        System.out.println("¿Desea eliminar una palabra de la expresión? || 1 para Aceptar");
        int des = Integer.parseInt(p.nextLine());
        data = "";
        while (des==1){
            System.out.println("¿Cuál es la palabra que quiere eliminar?");
            data = p.nextLine();
            alma.remove(data); // Se utiliza el método .remove de las listas circulares para eliminar un elemento especifico
            System.out.println("¿Desea eliminar otra palabra de la expresión? || 1 para Aceptar");
            des = Integer.parseInt(p.nextLine()); // Se pregunta denuevo si quiere volver eliminar otro elemento
        }
        System.out.println("La lista es la siguiente\n" + alma.toString());
        System.out.println("El tamaño de la lista es de \n" + alma.getSize());
        //impresión final
    }
}