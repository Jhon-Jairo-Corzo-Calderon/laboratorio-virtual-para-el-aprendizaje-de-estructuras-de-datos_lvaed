package com.company;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    //Aplicación mediante el uso de pilas por lista//
    // Conversión de expresiones aritmeticas desde infija (comunmente utilizadas) a otras expresiones como la postfija o prefija//

    public static String prefija(String c){
        // (1*(2-3))+(4+5)
        // Rta : *1-23+45
        ArrayList post = new ArrayList(); // Se crea una pila por listas para almacenar las variables o números
        ArrayStack h = new ArrayStack(c.length()); // Se crea una pila por Array con la longitud del String ingresado en el parámetro para almacenar los parentesis u otros caracteres
        for (int i = c.length () -1 ; i>= 0; i--){ // Se crea un for con el fin de recorrer cada Char del String
            char k = c.charAt(i); // Se asigna en el char k cada caracter del String con el fin de evaluarlo posteriormente
            if (k == 'x' || k == 'y' || k == 'z' || k =='w' || k == '0' || k == '1' || k == '2' || k == '3' || k == '4' || k == '5' || k == '6' || k == '7' || k == '8' || k == '9') {
                // Se almacena en la pila básada por listas las variables o números
                post.add(k);
            } else {
                h.push(k); // Se almacena en la pila básada por arreglos los otros caracteres (Operaciones y parentesis)
                if (k == '(') { // Cuando se identifica el caracter '(' recorre al siguiente camino
                    while (!h.isEmpty()){  // Se empieza a vaciar la pila (Desde el top claramente) , para posteriormente almacenarla en la pila que contiene las variables y números
                        h.pop();
                        post.add(h.pop());
                    }
                }
            }
        }while (!h.isEmpty()) // Se utiliza este while para vaciar los residuos de la pila que almacenada los caracteres para concluir con los ultimos elementos aritmeticos
            post.add(h.pop());

        String result = ""; // Se crea un String
        for (Object asig : post){ // Se recorre la pila por lista con todos sus elementos
            if(asig != null ){
                result += asig; // Se almacena la pila en este String
            }
        }
        String resultc = "";
        for(int m=0 ; m < result.length() ; m++){
            char n = result.charAt(m);
            if(n != '(' && n !=')'){
                resultc += n; //Para eliminar paréntesis (Caracteres no permitidos en la notación prefija) , se recorre el string anterior y solo se almacena los caracteres distintos a esos en este nuevo String
            }
        }

        String iresult = "";
        for (int j=resultc.length()-1;j>=0;j--){
            iresult = iresult + resultc.charAt(j); // Y se voltea la expresión debido a la naturaleza de la expresión
        }

        return iresult;
    }

    public static String postfija(String c){
        // (1*(2-3))+(4+5)
        // Rta : 123-*45++
        ArrayList post = new ArrayList();
        ArrayStack h = new ArrayStack(c.length());
        for(int i=0 ; i < c.length() ; i++) {
            char k = c.charAt(i);
            if (k == 'x' || k == 'y' || k == 'z' || k =='w' || k == '0' || k == '1' || k == '2' || k == '3' || k == '4' || k == '5' || k == '6' || k == '7' || k == '8' || k == '9') {
                post.add(k);
            } else {
                h.push(k);
                if (k == ')') { // La diferencia con la notación prefija con respecto a la postfija es que se empieza a contar desde izquierda a derecha
                    do {
                        h.pop();
                        post.add(h.pop());
                    } while (!h.isEmpty());
                }
            }
        }while (!h.isEmpty())
            post.add(h.pop());
        String result = "";
        for (Object asig : post){
            if(asig != null){
                result += asig;
            }
        }

        return result; // No es necesario voltear la expresión por efectos prácticos de la notación
    }

    //Main el cual se encarga de validar los datos para posteriormente utilizarlos en los parametros de las funciones//
    public static void main(String[] args) {
        Scanner entradaEscaner = new Scanner (System.in);
        String exp = new String();
        System.out.println("Escriba una expresión algebraica (Si va a utilizar variables que estas sean solo x,y,z y/o w)");
        exp = entradaEscaner.nextLine();
        System.out.println("¿Quiere pasar de infija a prefija? / 1 para Si , 0 para No");
        int des1 = entradaEscaner.nextInt();
        if(des1==1){
            String d = prefija(exp);
            System.out.println("La notación prefija de la expresión ingresada es \n" + d);
        }
        System.out.println("¿Quiere pasar de infija a postfija?/ 1 para Si , 0 para No");
        int des2 = entradaEscaner.nextInt();
        if(des2==1){
            String p = postfija(exp);
            System.out.println("La notación postfija de la expresión ingresada es \n" + p);
        }
        System.out.println("Gracias por utilizar el programa");
    }

    // El usuario se encarga de digitar la expresión en infija para posteriormente mediante la consola , preguntarle si quiere convertirla a una de las dos opciones o a las dos//
}