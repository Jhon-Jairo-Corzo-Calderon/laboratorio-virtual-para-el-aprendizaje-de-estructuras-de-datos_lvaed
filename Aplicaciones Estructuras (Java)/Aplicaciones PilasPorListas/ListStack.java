package com.company;

import static java.lang.System.out;

public class ListStack implements StackInterface {

    List ram = new List();

    @Override
    public void clear(){
        ram.clear();
    }
    @Override
    public boolean isEmpty(){
        return ram.isEmpty();
    }
    @Override
    public Object peek(){
        return ram.tail.getObject();
    }
    @Override
    public Object pop(){
        Object data = peek();
        ram.remove(ram.tail);
        out.println("this item has been removed from the stack");
        return data;
    }
    @Override
    public void push(Object object){
        ram.add(object);
    }
    @Override
    public int size(){
        return ram.getSize();
    }
    @Override
    public Object search(Object object){
        return ram.search(object).getObject();
    }
    @Override
    public void sort(){ }
    @Override

    public void reverse(){ }
    @Override

    public String toString(){
        return "xd";
    }
}
