package com.company;

import java.util.Iterator;
import static java.lang.System.*;

public class List implements IList, Iterable<ListNode>{

    private ListNode inode; //Nodo iterable
    private int size;

    public ListNode head; //Punteros para saber donde está el inicio y el fin
    public ListNode tail;

    /**
     * List
     */
    public List() {
        clear();
    }

    /*
    ok
     */
    public List(Object object) {
        add(object);
    }

    /*
    ok
     */
    public boolean isEmpty() {
        return head == null;
    }

    /*
    ok
     */
    @Override
    public int getSize() {
        return size;
    }

    /*
    ok
     */
    @Override
    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    /*
    ok
     */
    @Override
    public Object getHead() {
        return head;
    }

    /*
    ok
     */
    @Override
    public Object getTail() {
        return tail;
    }

    /*
    ok
     */
    @Override
    public ListNode search(Object object) {
        Iterator<ListNode> i = this.iterator();
        ListNode inode;
        while ((inode = i.next()) != null) {
            if (inode.getObject().toString().equals(object.toString())) {
                return inode;
            }
        }
        return null;
    }

    /*
    ok
     */
    @Override
    public boolean add(Object object) {
        return insertTail(object);
    }

    /*
    ok
     */
    @Override
    public boolean insert(ListNode node, Object object) { //REVISAR ESTA PARTE
        try {
            if (node.next == null) {
                add(object);
            } else {
                ListNode newNode = new ListNode(object);
                newNode.previous = node;
                (node.next).previous = newNode; //Se usa los parentesis para así entenderlo un poco mejor :D
                newNode.next = node.next;
                node.next = newNode;
            }
            this.size++;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
    ok
     */
    @Override
    public boolean insert(Object ob, Object object) {
        try {
            if (ob != null) {
                ListNode node = this.search(ob);
                if (node != null) {
                    return insert(node, object);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /*
    ok
     */
    @Override
    public boolean insertHead(Object object) {
        try {
            if (isEmpty()) {
                head = new ListNode(object); //Se crea el nodo
                tail = head;
            } else {
                head.previous = new ListNode(object, head, null);
                head = head.previous;
            }
            this.size++;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
    ok
     */
    @Override
    public boolean insertTail(Object object) {
        try {
            if (isEmpty()) {
                head = new ListNode(object); //Se crea el nodo
                tail = head;
            } else {
                tail.next = new ListNode(object,null,tail); //tail hace referencia al nodo completo, tail.next hace referencia a la cajita del puntero
                tail = tail.next;
            }
            this.size++;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
    ok
     */
    @Override
    public boolean remove(ListNode node) {
        remove(node.getObject());
        return  true;
    }

    /*
    ok
     */
    @Override
    public boolean remove(Object object) {
        if (isEmpty() == false){
            if(head == tail && object == head.getObject()){
                head=null;
                tail=null;
            }
            else if(head.isEquals(object)){ //object == head.getObject()
                head = head.next;
            }
            else{
                ListNode previous = head,temp = head.next;
                while (temp != null && temp.getObject() != object){
                    previous = previous.next;
                    temp = temp.next;
                }
                if (temp != null){
                    previous.next = temp.next;
                    if (temp == tail){
                        tail = previous;
                    }
                }
            }
            //size--;
        }
        return true;

    }

    @Override
    public boolean contains(Object object) {
        inode = head;
        while (inode != null){
            if (inode.getObject() == object){
                return true;
            }
            inode = inode.next;
        }
        return false;
    }


    public Object[] toArray() {
        Object[] array = new Object[size];
        inode = head;
        for (int i=0; i<size; i++){
            array[i] = inode.getObject();
            inode = inode.next;
        }
        return array;
    }

    @Override
    public Object[] toArray(Object[] object) {
        inode = head;
        for (int i=0; i<size; i++){
            object[i] = inode.getObject();
            inode = inode.next;
        }
        return object;
    }

    @Override
    public Object getBeforeTo() {
        return getBeforeTo(tail).getObject(); //Si invocan al método getBeforeTo que se encuentra sin parámetro se asume que me están pidiendo el objeto que se encuentra antes de la cola
    }

    /*
    ok
     */
    @Override
    public ListNode getBeforeTo(ListNode node) {
        if (isEmpty() == false){
            if (head == tail){
                return null;
            }
            else if(node == null){
                return tail;
            }
            else {
                ListNode previous = null;
                inode = head;
                while (inode != null){
                    if (inode.getObject() == node.getObject()){
                        return previous;
                    }
                    else {
                        previous = inode;
                        inode = inode.next;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public Object getNextTo() {
        return getNextTo(head); //Si invocan al método getNextTo que se encuentra sin parámetro se asume que me están pidiendo el objeto que se encuentra después de la cabeza, claro está en caso de haya una cabeza o haya algo después de la cabeza
    }

    @Override
    public Object getNextTo(ListNode node) {
        if (isEmpty() == false){
            if (head == tail){
                return null;
            }
            else {
                ListNode previous = head;
                inode = head.next;
                while (previous != null){
                    if (previous.getObject() == node.getObject()){
                        if (inode != null){
                            return inode.getObject();
                        }
                        else{
                            return null;
                        }
                    }
                    else {
                        previous = inode;
                        inode = inode.next;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public List subList(ListNode from, ListNode to) {
        List out = new List();
        if (isEmpty() == false){
            inode = head;
            boolean itsInRange = false;
            for (int i=0; i<size; i++){
                if ( inode.getObject() == from.getObject() || itsInRange == true){
                    if (inode.getObject() != to.getObject()){
                        out.add(inode.getObject());
                        itsInRange = true;
                    }
                    else {
                        out.add(inode.getObject());
                        break;
                    }
                }
                inode = inode.next;
            }
        }
        return out;
    }

    @Override
    public List sortList() {
        Object previous;
        Object actual;
        int cont = 0;
        do{
            inode = head;
            while(inode.next != null)
            {
                previous = inode.getObject();
                actual = inode.next.getObject();
                if((previous.toString().compareTo(actual.toString()) > 0))
                {
                    this.remove(previous);
                    this.insertTail(previous);
                }
                inode = inode.next;
            }
            cont++;
        }while(cont < size);
        return null;
    }

    @Override
    public Iterator<ListNode> iterator() {
        inode = head;
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return inode.next != null;
            }

            @Override
            public ListNode next() {
                if (inode != null) {
                    ListNode tmp = inode;
                    inode = inode.next;
                    return tmp;
                } else {
                    return null;
                }
            }
        };
    }

    public void recInicioFin() {
        try{
            ListNode node = new ListNode();
            node = head;
            out.println(node.toString());
        }catch (Exception e){ }
    }
    public void recFinInicio() {
        try{
            inode = tail;
            String output = "";
            while (inode != null){
                if (inode.previous!=null){
                    output += inode.toStringReverse();
                }
                else {
                    output += inode.toStringReverse() + "null}";
                }
                inode = inode.previous;
            }
            out.println(output);
        }catch (Exception e){ }
    }
}