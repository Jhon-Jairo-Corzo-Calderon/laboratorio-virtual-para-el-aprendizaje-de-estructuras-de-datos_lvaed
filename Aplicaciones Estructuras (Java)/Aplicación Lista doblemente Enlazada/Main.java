package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        List data = new List(); // Lista doblemente enlazada en donde se almacenara el String
        Scanner p = new Scanner(System.in);
        System.out.println("Digite la cadena de texto");
        String name = p.nextLine(); // String digitado por el usuario
        for(int i = 0 ; i < name.length() ; i++){
            char u = name.charAt(i); //Se recorre el String y posteriormente se le añade a la lista
            data.add(u);
        }
        data.sortList(); // Se utiliza el método .sortList de las listas para organizar los caracteres
        System.out.println("Esta es la siguiente lista con los caracteres organizados \n" );
        System.out.println(data.head); // Se imprime la lista

    }
}
