package com.company;

import java.lang.reflect.Array;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner p = new Scanner(System.in);
        System.out.println("Escriba su palabra");
        String data = p.nextLine();
        Pila one = new Pila(data.length()); //Se crea una pila por array
        List two = new List(); // Se crea una lista
        for(int i = 0 ; i < data.length() ; i++){ // Se recorre la palabra ingresa y se le agrega caracter por caracter a las estructuras
            char u = data.charAt(i);
            one.push(u);
            two.add(u);
        }
        boolean des = true; // Se crea el identificador
        ListNode head = two.head;
        for(int k = 0 ; k <data.length() ; k++){
            Object var = one.pop(); // Mediante el método .pop , la pila retorna y elimina el ultimo elemento en ingresar
            //System.out.println(var);
            Object var2 = head.getObject(); // Se declara la cabeza
            //System.out.println(var2);
            head = head.next; // Se recorre la lista desde la cabeza
            if(var==var2){
                des =true;
            }else {
                des = false; // Si almenos una variable no es igual , el identificador se define como false y cierra el ciclo
                break;
            }
        }
        String s = "";
        //Se define la concatenación donde se muestre si es o no palindroma
        if(des==true){
            s = "Es palindroma";
        }else {
            s = "No es palindroma";
        }
        System.out.println("La palabra" + " " + data + " " + s); // Impresión final
    }
}
