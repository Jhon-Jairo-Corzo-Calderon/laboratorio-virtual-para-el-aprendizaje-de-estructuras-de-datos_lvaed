package com.company;
import java.util.Arrays;

public class Pila implements IPila {

    private int size;
    private Object[] array;
    private int top;

    public Pila(int size) {
        this.size = size;
        this.array = new Object[(size > 0) ? size : 1];
        clear();
    }

    @Override
    public void clear() {
        for (int i = 0; i < array.length; i++) {
            array[i] = null;
        }
        top = -1;
    }

    @Override
    public boolean isEmpty() {
        return array[0] == null;
    }

    @Override
    public Object peek() {
        return (!isEmpty()) ? array[top] : null;
    }

    @Override
    public Object pop() {
        if (!isEmpty()) {
            Object object = array[top];
            array[top--] = null;
            return object;
        } else {
            return null;
        }
    }

    @Override
    public boolean push(Object object) {
        if (top + 1 < size) {
            try {
                array[++top] = object;
                return true;
            } catch (Exception e) {
                System.out.println(e);
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int size() {
        return top + 1;
    }

    @Override
    public boolean search(Object object) {
        Object[] copia = new Object[size];
        boolean correcto = false;
        for (int i=0; i<size; i++){
            copia[i] = pop();
            if (copia[i]==object){
                correcto = true;
            }
        }

        for (int i=0; i<size; i++){
            array[i] = copia[(size-1)-i];
        }

        top = size-1;

        return correcto;
    }

    @Override
    public void sort() {
        Object[] resultado = new Object[size];
        Object dato;
        while (array.length != 0){
            dato = Eliminar(array);
            this.Situar(dato, resultado);
        }
        array = resultado;
        //Arrays.sort(array);
    }

    @Override
    public void reverse() {
        Object[] newArray = new Object[size];
        for (int i=0; i<size; i++){
            newArray[i] = pop();
        }
        array = newArray;
        top = size-1;
    }

    @Override
    public String toString() {
        return "ArrayStack{" +
                "size=" + size +
                ", array=" + Arrays.toString(array) +
                ", top=" + top +
                '}';
    }

    public Object Eliminar(Object[] arrayAEliminar){
        Object[] newArray = new Object[size-1];
        for (int i=0; i<size-1; i++){
            newArray[i] = arrayAEliminar[i];
        }
        array = newArray;
        size = array.length;
        top = size-1;

        return arrayAEliminar[arrayAEliminar.length-1];
    }

    public void AgregarAlFinal(Object[] arrayAInsertar, Object objectoAAgregar){
        Object[] newArray = new Object[size+1];
        for (int i=0; i<size+1; i++){
            if (i != size){
                newArray[i] = arrayAInsertar[i];
            }else {
                newArray[i] = objectoAAgregar;
            }

        }

        top = size;
        array = newArray;
        size = array.length;
    }

    private void Situar(Object dato, Object[] resultado) {
        //Lis
        Object[] auxiliar = new Object[size];
        if (resultado.length == 0){
            AgregarAlFinal(resultado,dato);
            //auxiliar.
        }else {
            if (dato.toString().compareTo(resultado[resultado.length-1].toString()) <= 0){
                AgregarAlFinal(resultado,dato);
            }
        }
    }
}