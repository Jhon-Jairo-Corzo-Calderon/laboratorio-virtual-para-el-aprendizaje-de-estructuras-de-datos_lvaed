package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner l = new Scanner(System.in);
        int cont1 = 0;
        String p;
        String data = "";
        boolean pared = false;
        do {
            p = l.nextLine();
            for (int r = 0; r < p.length(); r++) {
                char o = p.charAt(r);
                if (o == 'e' || o == 'm' || o == '0' || o == '1') {
                    data += o;
                } // Se asignan los valores del mapa al String data para su posterior manipulación
            }
            if (cont1 != 0) {
                for (int j = 0; j < p.length(); j++) {
                    char k = p.charAt(j);
                    if (k == '0' || k == 'm' || k == 'e') {
                        pared = false; // Parametros para completar el mapa
                        break;
                    } else if (k == '1') {
                        pared = true;
                    }
                }
            }
            cont1++;
        } while (pared != true);
        System.out.println("Digite el ancho de la matriz");
        int a = l.nextInt(); // Asignación del ancho
        //Metodo para encontrar las coordenadas e
        int coordxe = 0;
        int coordye = 0;
        int ind1 = 0;
        char gg1 = 'x';
        do {
            ind1++;
            gg1 = data.charAt(ind1);
            coordxe++;
            if (coordxe >= a) { // si el valor de x es mayor que el ancho , esto significa que se salio del mapa , por lo cual se le restablece a 0 para así pasar a la siguiente fila
                coordxe = 0;
                coordye++; // Cada vez que esto pasa se le suma ++1 a ala coordenada en "Y" debido al salto que da
            }
        } while (gg1 != 'e'); // Este proceso nunca para hasta encontrar el Char 'e' en el String data
        System.out.println("Las coordenas de e son \n" + "[" + coordye + "," + coordxe + "]");

        //Metodo para encontrar M (Es la misma lógica del metodo para encontrar e)
        int coordxm = 0;
        int coordym = 0;
        int ind2 = 0;
        char gg2 = 'x';
        do {
            ind2++;
            gg2 = data.charAt(ind2);
            coordxm++;
            if (coordxm >= a) {
                coordxm = 0;
                coordym++;
            }
        } while (gg2 != 'm'); // Este proceso nunca para hasta encontrar el Char 'm' en el String data
        System.out.println("Las coordenas de m son \n" + "[" + coordym + "," + coordxm + "]");
        //System.out.println(data);

        //Camino de m a e
        char rev;
        char cond = data.charAt(coordxm+(a*coordym)); // Posición dentro del String de 'm'
        List structure = new List();
        Object rute;
        while (cond != 'e'){ // Hasta que la posición de 'm' no sea igual a 'e' , se repite este bucle con el fin de igualar las coordenas de estos mismos
            if(coordxm!=coordxe){
                if(coordxm>coordxe){
                    coordxm--;
                    rev = data.charAt(coordxm+(a*coordym));
                    if(rev=='1'){
                        coordxm++;
                    }
                }else if (coordxm < coordxe) {
                    coordxm++;
                    rev = data.charAt(coordxm+(a*coordym));
                    if(rev=='1'){
                        coordxm--;
                    }
                }
                //Las lineas anteriores se encargan de ver si las coordenadas de "m" con respecto a "e" en "x"
                //con mayores o menores para posteriormente sumarle o restarle y verificar si la nueva posición no es un '1' , si este es el caso , deshace esta operación
            }
            rute = "[" + coordym + "," + coordxm + "]"; // Se crea un Object el cual almacena una concatenación con las coordenadas de ese entonces
            structure.add(rute); // Se le agrega a la lista para dejar un registro
            if(coordym!=coordye){
                if(coordym>coordye){
                    coordym--;
                    rev = data.charAt(coordxm+(a*coordym));
                    if(rev=='1'){
                        coordym++;
                    }
                }else if (coordym < coordye) {
                    coordym++;
                    rev = data.charAt(coordxm+(a*coordym));
                    if(rev=='1'){
                        coordym--;
                    }
                }
            }
            //De igual forma es el mismo proceso para las coordenadas en "Y"
            rute = "[" + coordym + "," + coordxm + "]";
            structure.add(rute);
            cond = data.charAt(coordxm+(a*coordym));
        }

        System.out.println("El camino a seguir es");
        for (Object u : structure){
            System.out.println(u);
        }
    }
}