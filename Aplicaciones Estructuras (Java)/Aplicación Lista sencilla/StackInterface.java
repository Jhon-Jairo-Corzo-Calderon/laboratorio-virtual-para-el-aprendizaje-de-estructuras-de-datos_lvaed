package com.company;

public interface StackInterface {

    public void clear();

    public boolean isEmpty();

    public Object peek();

    public Object pop();

    public void push(Object object);

    public int size();

    public Object search(Object object);

    public void sort();

    public void reverse();

    public String toString();

}


