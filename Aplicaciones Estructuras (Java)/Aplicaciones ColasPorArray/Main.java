package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList datai = new ArrayList();
        datai.add("S-Fifa20");
        datai.add("S-Formula1");
        datai.add("A-Call Of Duty");
        datai.add("M-Grand Theft AutoV");
        datai.add("M-World Of Warcraft");
        datai.add("S-Nba2021");
        datai.add("A-Destiny");
        datai.add("A-Halo");
        datai.add("S-Age of empires");
        datai.add("S-Monopoly");
        datai.add("M-Genshing Impact");
        datai.add("A-Warframe");
        datai.add("A-PUBG");
        datai.add("A-Fortnite");
        datai.add("M-Never Winter");
        datai.add("M-Magi");
        datai.add("A-For Honor");
        datai.add("A-Left 4 Dead");
        datai.add("S-Pes2021");

        //Data de los juegos

        Cola data = new Cola(datai.size());

        int TA = 0;
        int TM = 0;
        int TS = 0;
        for(Object u : datai){
            data.insert(u);
            char o = u.toString().charAt(0);
            if(o == 'A'){
                TA++;
            }else if(o=='M'){
                TM++;
            }else if (o=='S'){
                TS++;
            }
        }
        //Como son estructuras estáticas debido al ser implementadas por Arrays , se debe clasificar y denotar el tamaño de las subcolas antes de su implementación

        Cola data_A = new Cola(TA);
        Cola data_M = new Cola(TM);
        Cola data_S = new Cola(TS); // Creación de las colas

        for(Object u : datai){
            data.insert(u);
            char o = u.toString().charAt(0);
            if(o == 'A'){ // Se empieza a recorrer la cola combinada y se clasifica en las subcolas dependiendo el Id
                data_A.insert(data.extract());
            }else if(o=='M'){
                data_M.insert(data.extract());
            }else if (o=='S'){
                data_S.insert(data.extract()); // Se inserta en la subCola y se borra de la cola combinada
            }
        }

        System.out.println(" Esta es la lista de los juegos de acción \n" + data_A+ " " + "y este es su tamaño" + " " + data_A.size());
        System.out.println(" Esta es la lista de los juegos de Simulación \n" + data_S+ " " + "y este es su tamaño" + " " + data_S.size());
        System.out.println(" Esta es la lista de los juegos de Mundo Abierto \n" + data_M + " " + "y este es su tamaño" + " " + data_M.size()); // Impresión final
    }
}
