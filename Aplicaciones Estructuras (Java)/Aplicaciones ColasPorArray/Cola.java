package com.company;
import java.util.Arrays;

public class Cola implements ICola{

    public Object array[];

    public Cola(int size){
        if (size>0){
            array = new Object[size];
        }else{
            array = new Object[1];
        }
    }
    @Override
    public void clear() {
        for (int i = 0; i < array.length; i++) {
            array[i] = null;
        }
    }

    @Override
    public boolean isEmpty() {
        boolean estaVacia = false;
        for (int i=0; i<size(); i++){
            if (array[i] == null){
                estaVacia = true;
            }else{
                estaVacia = false;
                break;
            }
        }
        return estaVacia;
    }

    @Override
    public Object extract() {
        Object object = null;
        for (int i=0; i<size(); i++){
            if (array[i]!=null){
                object = array[i];
                array[i] = null;
                return object;
            }
        }
        return object;
    }

    @Override
    public boolean insert(Object object) {
        for (int i=0; i<array.length; i++){
            if (array[i] == null){
                array[i] = object;
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public boolean search(Object object) {
        Object arraycopia[] = new Object[size()];
        for (int i=0; i<size(); i++){
            if (array[i] == object){
                return true;
            }
        }
        return false;
    }

    @Override
    public void sort() {
        List s = new List();
        for (Object o: array){
            s.add(o);
        }
        s.sortList();
        array = s.toArray();
    }

    @Override
    public void reverse() {
        Object reverseArray[] = new Object[size()];
        for (int i = 0; i < size(); i++){
            reverseArray[i] = array[array.length-(i+1)];
        }
        array = reverseArray;
    }

    @Override
    public String toString(){
        return "ArrayTail{" + "size=" + size() + ", array=" + Arrays.toString(array) + '}';
    }
}
