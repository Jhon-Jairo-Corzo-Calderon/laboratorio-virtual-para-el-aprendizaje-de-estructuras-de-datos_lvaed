from listNode import ListNode
from collections import Iterator

class List(ListNode):

    __inode = ListNode() #Nodo necesario para iterar la lista
    iterBack = False#Como en python no se pueden sobrecargar metodos, esta variable ayuda a
                    #escoger, si se quiere recorrer la lista hacia atras, si esta es True
                    #por default, recorre desde el nodo cabeza a cola.

    __size = 0  #Entero para almacenar el tamaño de la lista

    head = None #Nodo cabeza
    tail = None #Nodo cola

    """
    Constructor de la clase, puede recibir un objeto como parametro, que en cuyo
    caso, seria el primer nodo de la lista.
    """
    def __init__(self, objectt=None): 
        if(objectt != None):
            self.add(objectt)

    """
    Retorna True si la lista esta vacia
    """
    def isEmpty(self): 
        return self.head == None

    """
    Retorna tamaño de la lista
    """
    def getSize(self): 
        return self.__size
    
    """
    Vacia la lista
    """
    def clear(self): 
        self.head = None
        self.tail = None
        self.__size = 0
    
    """
    Retorna nodo cabeza
    """
    def getHead(self):
        return self.head
    
    """
    Retorna nodo cola
    """
    def getTail(self): 
        return self.tail

    """
    Busca un objeto o nodo en la lista, si retorna este nodo
    """
    def search(self,objectt):
        """
        Las 2 siguientes lineas nos permiten recorrer la lista, en la variable
        it, se crea e inicializa el iterador, y la variable inode almacena el nodo
        con el que se hace la iteracion.
        """
        self.iterBack = False 
        it = iter(self)  #Iterador de la lista
        inode = next(it)
        while(inode != None):
            if(inode.isEquals(objectt)):
                return inode
            inode = next(it)
        return None

    
    """
    Busca un objeto o nodo en la lista, recorriendola al reves, es decir, desde la cola a la cabeza
    """
    def searchBackwards(self,objectt):
        self.iterBack = True #Cambia la variable a True, para iterar de forma contraria
        it = iter(self)  #Iterador de la lista
        inode = next(it) 
        while(inode != None):
            if(inode.isEquals(objectt)):
                self.iterBack = False
                return inode
            inode = next(it)
        self.iterBack = False #Retorna la variable a False, para dejar el iterador de forma predeterminada
        return None

    """
    Agrega un nodo, insertandolo por la cola de la lista
    """
    def add(self, objectt):
        self.insertTail(objectt) 

    """
    Inserta un objeto, en la posicion siguiente al nodo u objeto dado, si este se encuentra en la lista. 
    """
    def insert(self ,node, objectt):
        if(type(node)==ListNode):    #Proceso a realizar si se entrega un nodo, en el parametro node
            try:           
                #Para que el se inserte al lado del nodo dado, este debe estar en la lista
                if(node.next == None):
                    self.add(objectt)
                else:
                    newNode = ListNode(objectt,node.next,node)
                    node.next.prev = newNode
                    node.next = newNode
                self.__size
                return True
            except Exception:
                return False
        else: #Proceso a realizar si se entrega un objeto, en el parametro node
            try:
                if(node!= None):
                    node = self.search(node)
                    if(node != None):
                        return self.insert(node,objectt)
                    else:
                        return False
            except Exception:
                return False

    """
    Inserta un objeto, en la cabeza de la lista. 
    """
    def insertHead(self,  objectt): 
        try:
            if(self.isEmpty()):
                self.head = ListNode(objectt)
                self.tail = self.head
            else:
                self.head.prev = ListNode(objectt, self.head)
                self.head = self.head.prev
            self.__size +=1
            return True
        except Exception:
            return False


    """
    Inserta un objeto, en la al final de la lista, es decir, la cola. 
    """
    def insertTail(self,objectt): 
        try:
            if(self.isEmpty()):
                self.head = ListNode(objectt)
                self.tail = self.head
            else:
                self.tail.next = ListNode(objectt, None, self.tail)
                self.tail = self.tail.next
                
            self.__size+=1
            return True
        except Exception:
            return False

    """
    Elimina el objeto dado como parametro, siempre y cuando este este en la lista.
    """
    def remove(self, objectt): 
        
        if(type(objectt)==ListNode):
            nodeB = self.search(objectt.getObject()) 

            if(nodeB != None):
                """
                Para eliminar un nodo hay que tener en cuenta tres situaciones, que este este en la 
                cabeza, en el cuerpo o la cola de la lista. 
                """
                if(nodeB.isEquals(self.head)):#Si objectt esta en la cabeza 
                    self.head = self.head.next
                    self.head.prev = None

                elif(nodeB.isEquals(self.tail)):#Si objectt esta en la cola
                    self.getBeforeTo(nodeB).next = None;

                else:#Si objectt esta en el cuerpo
                    self.getNextTo(nodeB).prev = self.getBeforeTo(nodeB)
                    self.getBeforeTo(nodeB).next = nodeB.next;
                    nodeB.next = None;

                self.__size -= 1
                return True
            else: #Proceso a realizar si se entrega un nodo, en el parametro objectt
                return False
        else:
            lNode = ListNode(objectt)
            return self.remove(lNode)

    """
    Busca el nodo u objeto en la lista, y si este se encuentra retorna True
    """
    def contains(self, objectt):  
        if(self.search(objectt)== None):
            return False
        return True

    """
    Convierte la lista en un arreglo, y si se desea, se puede entregar otro arreglo como parametro 
    para agregar al arreglo a retornar. Debido a que en python no hay arreglos predefinidos, se 
    retorna una lista.
    """
    def toArray(self, objectt = None):  
        if(objectt == None): #En el caso que no se entrega un arreglo adicional
            self.iterBack = False 

            arreglo = [None]*(self.__size) 
            it = iter(self)
            inode = next(it)
            cont = 0

            while(inode != None):
                arreglo[cont] = inode.getObject()
                cont += 1
                inode = next(it)
            return arreglo

        else: #En el caso que se entrega un arreglo adicional
            self.iterBack = False 

            arreglo = [None]*(self.__size + len(objectt))
            arreglo2 = self.toArray()

            cont = 0
            while(cont<len(objectt)+self.__size):
                if(cont==self.__size):
                    for i in objectt:
                        arreglo[cont] = i
                        cont += 1
                    break
                else:
                    arreglo[cont] = arreglo2[cont]
                cont +=1
            return arreglo

    """
    Metodo para convertir un arreglo (lista de python) en una lista enlazada sencilla
    """
    def backToList(self, objectt):  
        head = List()
        for i in objectt:
            head.add(i)
        return head

    """
    Retorna el nodo previo a al nodo entregado como parametro.
    """
    def getBeforeTo(self, node):
        if(type(node)==ListNode):
            onode = self.search(node)
            if(onode != None):
                return onode.prev

            return None
        else:
            return self.getBeforeTo(ListNode(node))

    """
    Retorna el nodo posterior a al nodo entregado como parametro.
    """
    def getNextTo(self, node):
        if(type(node)==ListNode):
            onode = self.search(node)
            if(onode != None):
                return onode.next

            return None
        else:
            return self.getNextTo(ListNode(node))
    

    """
    Crea una sublista desde el primer parametro dado hasta el segundo, solo si estos se encuentran en
    la lista.
    """
    def subList(self,fromm,to): 
        if(type(fromm) == ListNode and type(to) == ListNode):
            if(self.contains(fromm) and self.contains(to)):
                array = self.toArray()
                return self.backToList(array[array.index(fromm.getObject()):array.index(to.getObject())+1])
            return None
        else:
            beg = ListNode(fromm)
            end = ListNode(to)

            return self.subList(beg,end)

    """
    Ordena la lista usando metodos predefinidos de una lista de python
    """
    def sortList(self): 
        array = self.toArray();
        array.sort()
        bk = self.backToList(array)
        self.head = bk.head
        self.tail = bk.tail


    """
    Sobreescribir el metodo __str__ para imprimir la Lista
    """
    def __str__(self):
        return  self.head.__str__() 

    """
    Metodo __iter__ sobreescrito para recorrer la lista.
    """
    def __iter__(self): 
        #Si se desea iterar la lista de manera contraria, se debe cambiar el valor, de la variable iterBack a True, y no ser
        #modificado hasta finalizar el recorrido de la lista, pues este tambien esta involucrado en el metodo __next__.
        if(not self.iterBack):
            self.__inode = self.head
            return self
        else:
            self.__inode = self.tail
            return self

    """
    Metodo __next__ sobreescrito para obtener cada nodo de la lista.
    """
    def __next__(self):  

        if(not self.iterBack):
            if(self.__inode!=None):
                tmp = self.__inode
                self.__inode = self.__inode.next
                return tmp
            else:
                return None
        else:
            if(self.__inode!=None):
                tmp = self.__inode
                self.__inode = self.__inode.prev
                return tmp
            else:
                return None
