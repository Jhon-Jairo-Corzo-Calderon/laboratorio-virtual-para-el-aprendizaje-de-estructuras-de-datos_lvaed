class ListNode():

    __object = None #Variable para almacenar el objeto del nodo
    next = None #Indicador de nodo siguiente
    prev = None #Indicador de nodo previo

    """
    Constructor de la clase, el unico parametro obligatorio es el Objeto 
    """
    def __init__ (self, objectt = None, next = None, previous = None ):
        self.__object = objectt
        self.prev = previous
        self.next = next

        if(type(objectt)== ListNode):
            self.__object = objectt.getObject()
            self.prev = objectt.prev
            self.next = objectt.next

    """
    Los 2 siguientes metodos permiten la encapsulacion de la variable __object
    """
    def getObject(self):
        try:
            return self.__object
        except Exception:
            return None

    def setObject(self,objectt):
        self.__object = objectt

    """
    Evaluar si un nodo o objeto, es equivalente al nodo, esto se hace comparando
    unicamente las __object con el parametro dado
    """
    def isEquals(self, objectt):
        if(type(objectt)==ListNode):
            if(self.__object == objectt.getObject()):#Si el elemento entregado como parametro "objectt" no es un nodo, se genera error y se va al try donde si se puede evaluar si esta en la Lista
                return True
        else:
            if(self.__object == objectt):
                return True
        return False

    """
    Imprimir un nodo de forma recursiva.
    """
    def __str__(self):
    
        try:
            return "ListNode[Object: {0}, Previous: {1}, Next: {2}]".format(self.__object,self.prev.getObject(),self.next)
        except Exception:
            return "ListNode[Object: {0}, Previous: {1}, Next: {2}]".format(self.__object,None,self.next)


    

