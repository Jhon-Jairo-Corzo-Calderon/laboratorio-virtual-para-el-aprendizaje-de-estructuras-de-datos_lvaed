from listDouble import listDouble
from listNode import ListNode


class PrioQueue():

    __queue = listDouble() #None
    __minPrio = 0
    __maxPrio = 0
    
    """
    Constructor de la clase 
    """
    def __init__(self, minPrio, maxPrio):
        self.__queue = listDouble()
        self.__maxPrio = maxPrio
        self.__minPrio = minPrio
        for i in range(maxPrio, minPrio, -1):
            self.__queue.add(listDouble())

    """
    Limpia la cola, no tiene retorno
    """
    def clear(self):
        self.__queue.clear()

    """
    Retorna True si la pila esta vacia, retorna un booleano
    """
    def isEmpty(self):
        return self.__queue.isEmpty()

    """
    Extrae el elemento en la cabeza de la cola, retorna un objeto o none
    """
    def extract(self):
        inode = self.__queue.head
        subList = listDouble()
        obj = object

        for i in range(self.__maxPrio,self.__maxPrio,-1):
            subList = inode.getObject()
            if(not subList.isEmpty()):
                obj = subList.head.getObject()

                subList.remove(subList.head)
                return obj
            else:
                inode = inode.next
        return None

    
    """
    Insertar un nodo, al final de la cola, retorna un booleano
    """
    def insert(self,ob,prio):
        inode = self.__queue.head
        subList = listDouble()

        for i in range(self.__maxPrio,self.__maxPrio,-1):
            if(i ==  prio):
                subList = inode.getObject()
                subList.add(ob)
                return True
            else:
                inode = inode.next
            
        return False

    """
    Retorna la cantidad de elementos, que no sean nulos, en el arreglo, retorna un entero
    """
    def size(self):
        inode = self.__queue.head
        acum  = 0
        subList = listDouble()

        for i in range(self.__maxPrio,self.__maxPrio,-1):
            subList = inode.getObject()
            acum += subList.getSize()
            inode = inode.next
        
        return acum

    """
    Busca un objeto en la cola, returna un booleano
    """
    def search(self, ob):
        inode = self.__queue.head
        subList = listDouble()

        for i in range(self.__maxPrio,self.__maxPrio,-1):
            try:
                subList = inode.getObject()
                if(subList.search(ob)!= None):
                    return True
                else:
                    inode = inode.next
                return False
            except Exception:
                return False
        
    
    """
    Ordena la cola, no tiene retorno
    """
    def sort(self):
        inode = self.__queue.head
        subList = listDouble()

        for i in range(self.__maxPrio,self.__maxPrio,-1):
            subList = inode.getObject()
            if(not subList.isEmpty()):
                subList.sortList()
            inode = inode.next

    """
    Revierte el orden de la cola, no tiene retorno
    """
    def reverse(self):
        inode = self.__queue.head
        inode2 = listDouble

        subList = listDouble()
        reverseList = listDouble()
        generalReverseList = listDouble()

        for i in range(self.__maxPrio,self.__maxPrio,-1):
            subList = inode.getObject()
            inode2 = subList.tail

            for j in range(subList.getSize(), 0, -1):
                reverseList.add(inode2.getObject)
                inode2 = inode2.next
            inode = inode.next
            generalReverseList.add(reverseList)
            
        self.__queue = generalReverseList

    """
    Imprimir la cola
    """
    def __str__(self):
        return self.__queue.__str__()



