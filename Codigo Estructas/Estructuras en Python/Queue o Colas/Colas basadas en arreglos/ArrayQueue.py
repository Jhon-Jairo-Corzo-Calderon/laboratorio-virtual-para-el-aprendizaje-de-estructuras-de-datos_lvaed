class ArrayQueue():
    
    __queue = [] #Como en python no hay arreglos predefinidos, se usa una lista, para evitar usar librerias
    __head = -1 #Indica la posicion del elemento en la cabeza
    __tail = -1 #Indica la posicion del elemento en el final de la cola
    __size = 0  #Variable tipo int para almacenar tamaño de la pila

    """
    Constructor de la clase, el unico parametro obligatorio es el tamaño de la cola 
    """
    def __init__ (self, size):
        self.__size = size
        self.__queue = [None]*(self.__size) 
        

    """
    Limpia la cola, no tiene retorno
    """
    def clear(self): #No recibe parametros ni tiene retorno
        for i in range(0, self.__size):
            self.__queue[i] = None
        self.__head = -1
        self.__tail = self.__head
    
    """
    Retorna True si la pila esta vacia, retorna un booleano
    """
    def isEmpty(self): #Retorna un booleano
        return self.__queue[0] == None 

    """
    Extrae el elemento en la cabeza de la cola, retorna un objeto o none
    """
    def extract(self):
        if(not self.isEmpty()):
            temp = self.__queue[self.__head]
            
            self.__queue[self.__head] = None
            if(self.__head > self.__tail):
                head = 0
            else:
                head+=1
        
            return temp
        else:
            return None

    """
    Insertar un nodo, al final de la cola, retorna un booleano
    """
    def insert(self, ob):
        if(self.__head == -1 or self.__head==0):
            if(self.__tail +1 < self.__size):
                self.__tail += 1
                self.__queue[self.__tail] = ob

                if(self.__head == -1):
                    head = 0
                return True
            else:
                return False
        else:
            if(self.__tail == self.__size -1):
                self.__queue[0] = ob
                self.__tail = 0
                return True
            elif(self.__tail + 1 != self.__head):
                self.__tail += 1 
                self.__queue[self.__tail] = ob
                return True
            else:
                return False

    """
    Retorna la cantidad de elementos, que no sean nulos, en el arreglo, retorna un entero
    """
    def size(self):
        return abs(self.__tail - self.__head) + 1

    """
    Busca un objeto en la cola, returna un booleano
    """
    def search(self, ob):
        for i in self.__queue:
            if(i == ob):
                return True
        
        return False
    
    """
    Ordena la cola, no tiene retorno
    """
    def sort(self):
        if(not self.isEmpty):
            self.__queue.sort()
    
    """
    Revierte el orden de la cola, no tiene retorno
    """
    def reverse(self):
        temp = object
        indOp = 0

        for i in range(0,self.__size/2):
            temp = self.__queue[i]
            indOp = self.__size - i - 1
            self.__queue[i] = self.__queue[indOp]
            self.__queue[indOp] = temp

    
    """
    Imprimir la cola
    """
    def __str__(self):
        return "ArrayStack[ size: {0}, array: {1}, head index: {2}, tail index: {3}]".format(self.size, self.__queue, self.__head, self.__tail)

