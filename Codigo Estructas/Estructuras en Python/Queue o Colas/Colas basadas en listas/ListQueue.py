from listDouble import listDouble
from listNode import ListNode


class ListQueue():

    __queue = None
    
    """
    Constructor de la clase 
    """
    def __init__(self):
        self.__queue = listDouble()

    """
    Limpia la cola, no tiene retorno
    """
    def clear(self):
        self.__queue.clear()

    """
    Retorna True si la pila esta vacia, retorna un booleano
    """
    def isEmpty(self):
        return self.__queue.isEmpty()

    """
    Extrae el elemento en la cabeza de la cola, retorna un objeto o none
    """
    def extract(self):
        temp = self.__queue.head 
        self.__queue.remove(temp)
        return temp.getObject()
    
    """
    Insertar un nodo, al final de la cola, retorna un booleano
    """
    def insert(self,ob):
        return self.__queue.insertTail(ob)

    """
    Retorna la cantidad de elementos, que no sean nulos, en el arreglo, retorna un entero
    """
    def size(self):
        return self.__queue.getSize()

    """
    Busca un objeto en la cola, returna un booleano
    """
    def search(self, ob):
        return self.__queue.contains(ob)
    
    """
    Ordena la cola, no tiene retorno
    """
    def sort(self):
        self.__queue.sortList()

    """
    Revierte el orden de la cola, no tiene retorno
    """
    def reverse(self):
        
        rever = listDouble()
        inode = self.__queue.tail
        
        for i in range(self.__queue.getSize(),0,-1):
            rever.insertTail(inode.getObject())
            inode = inode.prev

        self.__queue = rever

    """
    Imprimir la cola
    """
    def __str__(self):
        return self.__queue.__str__()



