from Node import Node


class BinTree(Node):

    __search = False
    root = None#Node()


    """
    Inicializador de la clase.
    """
    def __init__(self, ob = None):
        if(ob != None):
            self.root = self.SubBinTree(None,ob,None)
        else:
            self.root = None


    """
    Como cada nodo de un arbol binario, puede ser a la misma vez un arbol binario
    con este metodo se crean sub arboles binarios, recibe un objeto, y los nodos
    derechos e izquierdos, y retorna un nodo con estos atributos.
    """
    def SubBinTree(self,left, ob, right):
        temp = Node(ob, left, right)
        # CalcularGradoArbol(temp)
        return temp;
    
    """
    Retorna True si la lista esta vacia
    """
    def IsEmpty(self):
        return self.root == None

    """
    Modifica o crea la raiz del arbol
    """
    def Root(self, ob):
        try:
            self.root = self.SubBinTree(None, ob, None)
            # CalcularGradoArbol(self.root)
            return True
        except Exception:
            return False
    
    """
    Inserta un elemento en la izquierda del arbol
    """
    def InsertLeft(self, ob):
        try:
            if(not self.IsEmpty()):
                self.root.left = self.SubBinTree(None, ob , None)
                return True
            else:
                self.Root(ob)
                return False
        except Exception:
            return False
    
    """
    Inserta un elemento en la derecha del arbol
    """
    def InsertRigth(self, ob):
        try:
            if(not self.IsEmpty()):
                self.root.right = self.SubBinTree(None, ob , None)
                return True
            else:
                self.Root(ob)
                return False
        except Exception:
            return False

    """
    Busca un elemento en el arbol binario, de forma recursiva, este
    metodo es privado.
    """
    def _Search(self, node, ob):
        if(not self.IsEmpty):
            if(node.isEquals(ob)):
                self.__search = True

            if(node.right != None):
                if(node.right.isEquals(ob)):
                    self.__search = True
                else:
                    self._Search(node.right,ob)

            if(node.left != None):
                if(node.left.isEquals(ob)):
                    self.__search = True
                else:
                    self._Search(node.left,ob)

    """
    Busca un elemento en el arbol binario, y retorna un booleano si esta en el arbol.
    """
    def Search(self, ob):   
        self._Search(ob)
        if(self.__search):
            self.__search = False
            return True
        else:
            return False

    """
    Elimina un elemento del arbol, no tiene retorno.
    """
    def Delete(self,root , ndDelete):
        if(not self.IsEmpty):
            if(root.isEquals(ndDelete)):
                if(root != self.root):
                    root.right = None
                    root.left = None
                else:
                    self.Root(None)
            
            if(root.right != None):
                if(root.right.isEquals(ndDelete)):
                    root.right = None
                else:
                    self.Delete(root.right, ndDelete)
            
            if(root.left != None):
                if(root.left.isEquals(ndDelete)):
                    root.left = None
                else:
                    self.Delete(root.left, ndDelete)

    """
    Realiza el recorrido preOrder de forma recursiva, y lo almacena en una variable de tipo string, metodo privado
    """
    def _preOrder(self,root):
        if(root != None):
            print((str(root.GetObject()) + " " if (root != None) else ""), end ="")
            self._preOrder(root.left)
            self._preOrder(root.right)
    
    """
    Imprime el resultado del recorrido _preOrder
    """
    def preOrder(self):
        if(not self.IsEmpty()):
            self._preOrder(self.root)
        else:
            print("El arbol esta vacio.")

    """
    Realiza el recorrido inOrder de forma recursiva, y lo almacena en una variable de tipo string
    """
    def _inOrder(self,root):
        if(root != None):
            self._inOrder(root.left)
            print((str(root.GetObject()) + " " if (root != None) else ""), end ="")
            self._inOrder(root.right)

    """
    Imprime el resultado del recorrido _inOrder
    """
    def inOrder(self):
        if(not self.IsEmpty()):
            self._inOrder(self.root)
        else:
            print("El arbol esta vacio.")
    
    """
    Realiza el recorrido posOrder de forma recursiva, y lo almacena en una variable de tipo string
    """
    def _posOrder(self,root):
        if(root != None):
            self._posOrder(root.left)
            self._posOrder(root.right)
            print((str(root.GetObject()) + " " if (root != None) else ""), end ="")

    """
    Imprime el resultado del recorrido _posOrder
    """
    def posOrder(self):
        if(not self.IsEmpty()):
            self._posOrder(self.root)
        else:
            print("El arbol esta vacio.")




