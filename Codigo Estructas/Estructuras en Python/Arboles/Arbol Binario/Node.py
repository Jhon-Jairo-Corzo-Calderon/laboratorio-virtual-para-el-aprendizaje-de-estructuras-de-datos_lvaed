class Node():

    __object = None #Variable para almacenar el objeto del nodo
    right = None  #Indicador de nodo derecho siguiente
    left = None  #Indicador de nodo izquierdo siguiente

    """
    Inicializador, recibe un objeto, y puede recibir los enlaces de este. 
    """
    def __init__(self, ob, left = None , right = None):
        self.__object = ob
        self.right = right
        self.left = left


    """
    Retorna el objeto del nodo, no recibe parametros. 
    """
    def GetObject(self):
        return self.__object
    
    """
    Evaluar si un nodo u objeto, es equivalente al nodo, esto se hace comparando
    unicamente las __object con el parametro dado
    """
    def isEquals(self, objectt):
        if(type(objectt)==Node):
            if(self.__object == objectt.getObject()):#Si el elemento entregado como parametro "objectt" no es un nodo, se genera error y se va al try donde si se puede evaluar si esta en la Lista
                return True
        else:
            if(self.__object == objectt):
                return True
        return False
    

    """
    Retorna el grado del nodo, no recibe parametros. 
    """
    def Grado(self):
        if(self.right == None and self.left==None):
            return 0
        elif((self.right == None and self.left!=None) or (self.right != None and self.left==None)):
            return 1
        else:
            return 2
    
    """
    Metodo para imprimir un nodo, no recibe parametros.  
    """
    def __str__(self):
        return "Node [left: {0}, right: {1}, object {2}]".format(self.left,self.right,self.GetObject())