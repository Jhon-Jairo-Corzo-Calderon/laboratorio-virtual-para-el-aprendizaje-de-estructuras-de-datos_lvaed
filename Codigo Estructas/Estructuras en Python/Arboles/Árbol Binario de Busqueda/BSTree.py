from Node import Node


class BSTree(Node):

    __search = False
    root = None#Node()


    """
    Inicializador de la clase.
    """
    def __init__(self, ob = None, key = None):
        if(ob != None and key == None):
            raise Exception("No key given for the node")
            self.root = Node(ob,key)
        else:
            self.root = None


    """
    Como cada nodo de un arbol binario, puede ser a la misma vez un arbol binario
    con este metodo se crean sub arboles binarios, recibe un objeto, y los nodos
    derechos e izquierdos, y retorna un nodo con estos atributos.
    """
    def SubBSTree(self,left, ob, right):
        temp = Node(ob, left, right)
        # CalcularGradoArbol(temp)
        return temp;
    
    """
    Retorna True si la lista esta vacia
    """
    def IsEmpty(self):
        return self.root == None


    """
    Inserta un elemento en la lista de acuerdo con la llave entregada como parametro.
    """
    def Insert(self, ob, key):
        if(not self.IsEmpty()):
            inode = self.root

            while(inode!=None):
                if(inode.GetKey()<= key):
                    if(inode.right != None):
                        inode = inode.right
                    else:
                        inode.right = Node(ob,key)
                        return True
                else:
                    if (inode.left!=None):
                        inode = inode.left
                    else:
                        inode.left = Node(ob,key)
                        return True
        else:
            root = Node(ob, key)
        return False

    """
    Busca un elemento en el arbol de forma recursiva, de acuerdo con la llave dada,
    este es un metodo privado.
    """
    def _Search(self,root, key):
        if(root != None):
            if(root.GetKey() == key):
                return root
            elif(root.GetKey() < key):
                return self._Search(root.right,key)
            else:
                return self._Search(root.left,key)
        return None

    """
    Busca un elemento en el arbol de acuerdo a la llave dada como parametro,
    utilizando _Search.
    """
    def Search(self, key):
        return self._Search(self.root,key)

    """
    Remueve un nodo del arbol, de acuerdo al id o llave dada como parametro
    retorna True si fue eliminado.
    """
    def Remove(self,key):
        if(not self.IsEmpty()):
            """
            Recorre el arbol y si la llave existe, obtiene el nodo al que 
            pertenece.
            """
            inode = self.root
            preinode = self.root
            while(inode.GetKey()!= key):
                preinode = inode
                if(inode.GetKey()>key):
                    inode = inode.left
                else:
                    inode = inode.right
                if(inode==None):
                    return False
            """
            Elimina el nodo.
            """
            if(inode.right==None and inode.left==None): #No tiene hijos
                if(inode == self.root):
                    self.root = None
                elif(preinode.left == inode):
                    preinode.left = None
                else:
                    preinode.right = None
            elif(inode.right == None): #Es una rama con un hijo
                if(inode ==self.root):
                    self.root = self.root.left
                elif(preinode.left == inode):
                    preinode.left = inode.left
                else:
                    preinode.right = inode.left
            elif(inode.left == None): #Es una rama con un hijo
                if(inode ==self.root):
                    self.root = self.root.right
                elif(preinode.left == inode):
                    preinode.left = inode.right
                else:
                    preinode.right = inode.right
            else: #Es una rama con 2 hijos
                rePre = inode
                re = inode
                aux = inode.right
                """
                Hallar 
                """
                while(aux != None):
                    rePre = re
                    re = aux
                    aux = aux.left

                if(re != inode.right):
                    rePre.left = re.right
                    re.right = inode.right
                
                if(inode == self.root):
                    self.root = re
                elif(preinode.left == inode):
                    preinode.left = re
                else:
                    preinode.right = re
                re.left = inode.left
                return True
        else:
            return False


    """
    Realiza el recorrido preOrder de forma recursiva, y lo almacena en una variable de tipo string, metodo privado
    """
    def _preOrder(self,root):
        if(root != None):
            print((str(root.GetObject()) + " " if (root != None) else ""), end ="")
            self._preOrder(root.left)
            self._preOrder(root.right)
    
    """
    Imprime el resultado del recorrido _preOrder
    """
    def preOrder(self):
        if(not self.IsEmpty()):
            self._preOrder(self.root)
        else:
            print("El arbol esta vacio.")

    """
    Realiza el recorrido inOrder de forma recursiva, y lo almacena en una variable de tipo string
    """
    def _inOrder(self,root):
        if(root != None):
            self._inOrder(root.left)
            print((str(root.GetObject()) + " " if (root != None) else ""), end ="")
            self._inOrder(root.right)

    """
    Imprime el resultado del recorrido _inOrder
    """
    def inOrder(self):
        if(not self.IsEmpty()):
            self._inOrder(self.root)
        else:
            print("El arbol esta vacio.")
    
    """
    Realiza el recorrido posOrder de forma recursiva, y lo almacena en una variable de tipo string
    """
    def _posOrder(self,root):
        if(root != None):
            self._posOrder(root.left)
            self._posOrder(root.right)
            print((str(root.GetObject()) + " " if (root != None) else ""), end ="")

    """
    Imprime el resultado del recorrido _posOrder
    """
    def posOrder(self):
        if(not self.IsEmpty()):
            self._posOrder(self.root)
        else:
            print("El arbol esta vacio.")




