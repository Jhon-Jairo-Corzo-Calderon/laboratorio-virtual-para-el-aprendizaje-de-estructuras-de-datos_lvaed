class ArrayStack():
    
    __size = 0  #Variable tipo int para almacenar tamaño de la pila
    __array = [] #Como en python no hay arreglos predefinidos, se usa una lista, para evitar usar librerias
    top = 0 #Indica la posicion del elemento en la cabeza

    """
    Constructor de la clase, el unico parametro obligatorio es el tamaño de la pila 
    """
    def __init__ (self, size):
        self.__size = size
        self.__array = [None]*(self.__size) 
        self.top = -1

    """
    Limpia la pila
    """
    def clear(self): #No recibe parametros ni tiene retorno
        for i in range(0, self.__size):
            self.__array[i] = None
        self.top = -1
    
    """
    Retorna True si la pila esta vacia
    """
    def isEmpty(self): #Retorna un booleano
        return self.__array[0] == None

    """
    Retorna el objeto en la cabeza de la pila sin eliminarlo
    """
    def peeK(self): #Retorna un objeto
        return self.__array[self.top] if (not self.isEmpty()) else None
    
    """
    Elimina el objeto en la cabeza de la pila y lo retorna
    """
    def pop(self): #No recibe parametro y retorna un objeto 
        if(not self.isEmpty()):
            ob = self.__array[self.top]
            self.__array[self.top] = None
            self.top -= 1
            return ob
        else:
            return None
    
    """
    Agrega objetos a la pila
    """
    def push(self, ob): #Recibe como parametro un objeto y retorna un booleano
        if(self.top + 1 < self.__size):
            try:
                self.top += 1
                self.__array[self.top] = ob
                return True
            except Exception:
                return False
        else:
            return False

    """
    Retorna la cantidad de elementos en la lista
    """
    def size(self): #Retorna un entero
        return self.top + 1

    """
    Buscar un objeto en la pila
    """
    def search(self, ob): #Recibe como parametro un objeto y retorna un booleano
        tempArr = ArrayStack(self.__size)

        temp = object
        cont = 0
        find = False

        for i in range(0, self.__size):
            temp = self.pop()
            if(temp == ob):
                self.push(temp)
                find = True
            else:
                tempArr.push(temp)
                cont += 1
        
        for x in range(0, cont):
            self.push(tempArr.pop())

        return find

    """
    Ordenar la lista
    """
    def sort(self): #No recibe parametros ni tiene retorno
        self.__array.sort()

    """
    Invierte los elementos de la pila
    """
    def reverse(self): #No recibe parametros ni tiene retorno
        tempArr = self.__array.copy()
        self.clear()

        for i in range(len(tempArr)-1 ,-1,-1):
            self.push(tempArr[i])

    """
    Imprimir la pila
    """
    def __str__(self):
        return str(self.__array) + " top index: {0}".format(self.top)


