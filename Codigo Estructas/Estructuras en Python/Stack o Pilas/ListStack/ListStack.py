from listNode import ListNode
from collections import Iterator


class ListStack(ListNode):

    __top = None #Nodo en la cabeza o cima de la pila
    __size = 0  #Entero para almacenar el tamaño de la lista
    
    __inode = ListNode() #Nodo necesario para iterar la lista


    """
    Constructor de la clase, puede recibir un objeto como parametro, que en cuyo
    caso, seria el primer nodo de la lista.
    """
    def __init__(self, objectt=None):  
        if(objectt != None):
            self.push(objectt)

    """
    Limpia la lista
    """
    def clear(self): #No recibe parametro ni tiene retorno
        self.__size = 0
        self.__top = None

    """
    Retorna True si la lista esta vacia
    """
    def isEmpty(self):  #No recibe parametro y retorna un boleano
        return self.__size == 0

    """
    Retorna el objeto en el nodo top, sin eliminarlo de la pila
    """
    def peek(self):  #No recibe parametro y retorna un objeto 
        return self.__top.getObject() if (not self.isEmpty()) else None


    """
    Elimina el objeto en la cabeza de la pila y lo retorna
    """
    def pop(self): #No recibe parametro y retorna un objeto 
        try:
            if(not self.isEmpty()):
                temp = self.__top.getObject()
                self.__top = self.__top.next
                self.__size -=1
                return temp
            else:
                return None
        except Exception:
            return None
    
    
    """
    Agrega objetos a la pila
    """
    def push(self, ob): #Recibe como parametro un objeto y retorna un booleano
        try:
            if(self.isEmpty()):
                self.__top = ListNode(ob)
            else:
                self.__top = ListNode(ob, self.__top)
            self.__size += 1
        except Exception:
            return None

    """
    Retorna la cantidad de elementos en la lista
    """
    def size(self): #Retorna un entero
        return self.__size

    """
    Buscar un objeto en la pila
    """
    def search(self, ob): #Recibe como parametro un objeto y retorna un booleano
        tempArr = ListStack(self.__size)

        temp = object
        cont = 0
        find = False

        for i in range(0, self.__size):
            temp = self.pop()
            if(temp == ob):
                self.push(temp)
                find = True
            else:
                tempArr.push(temp)
                cont += 1
        
        for x in range(0, cont):
            self.push(tempArr.pop())

        return find

    """
    Ordenar la lista
    """
    def sort(self): #No recibe parametros ni tiene retorno
        it = iter(self)
        inode = next(it)
        arrStack = [None]*(self.__size)
        cont = 0

        while inode != None:
            arrStack[cont] = inode.getObject()
            inode = next(it)
            cont += 1

        self.clear()
        arrStack.sort()

        for i in arrStack:
            self.push(i)

    """
    Invierte los elementos de la pila
    """
    def reverse(self): #No recibe parametros ni tiene retorno
        tempArr = [None]*(self.__size)
        
        for i in  range(0,len(tempArr)):
            tempArr[i] = self.pop()
        
        for x in range(0,len(tempArr)):
            self.push(tempArr[x])



    """
    Sobreescribir el metodo __str__ para imprimir la Lista
    """
    def __str__(self):
        return  "[TOP]" + self.__top.__str__() + "Size: {0}".format(self.__size) 

    """
    Metodo __iter__ sobreescrito para recorrer la Lista
    """
    def __iter__(self): #Metodo para crear iteradores
        self.__inode = self.__top
        return self
    
    """
    Metodo __next__ sobreescrito para obtener cada nodo de la lista
    """
    def __next__(self):  #Metodo para moverse en el iterador de la lista
        if(self.__inode!=None):
            tmp = self.__inode
            self.__inode = self.__inode.next
            return tmp
        else:
            return None
