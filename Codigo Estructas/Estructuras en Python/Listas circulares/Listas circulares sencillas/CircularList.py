from listNode import ListNode
from collections import Iterator


class CircularList(ListNode):

    __inode = ListNode() #Nodo necesario para iterar la lista
    __size = 0  #Entero para almacenar el tamaño de la lista
    
    head = None #Nodo cabeza
    tail = None #Nodo cola

    """
    Constructor de la clase, puede recibir un objeto como parametro, que en cuyo
    caso, seria el primer nodo de la lista.
    """
    def __init__(self, objectt=None):  
        if(objectt != None):
            self.add(objectt)

    """
    Retorna True si la lista esta vacia
    """
    def isEmpty(self):  # No recibe parametros
        return self.head == None

    """
    Retorna tamaño de la lista
    """
    def getSize(self): # No recibe parametros
        return self.__size

    """
    Limpia la lista sin retorno
    """
    def clear(self): # No recibe parametros
        self.head = None
        self.tail = None
        self.__size = 0

    """
    Retorna nodo cabeza
    """
    def getHead(self):  # No recibe parametros
        return self.head

    """
    Retorna nodo cola
    """
    def getTail(self):  # No recibe parametros
        return self.tail

    """
    Busca un objeto o nodo en la lista, si se encuentra en la lista, retorna este nodo
    """
    def search(self, objectt): # Recibe como parametro objectt, que puede ser Objeto o Nodo

        """
        Las 2 siguientes lineas nos permiten recorrer la lista, en la variable
        it, se crea e inicializa el iterador, y la variable inode almacena el nodo
        con el que se hace la iteracion.
        
        Como se esta trabajando con una lista circular es importante controlar las 
        que se hacen, porque de otra forma, se puede entrar en un bucle sin fin.
        Por esto se hace uso de un bucle for
        """
        it = iter(self)  

        for i in range(0,self.__size):
            inode = next(it) 
            if(inode.isEquals(objectt)):
                return inode
        return None


    """
    Agrega un nodo, insertandolo por la cola de la lista, no hay retorno
    """
    def add(self, objectt): # Recibe como parametro objectt, que puede ser Objeto o Nodo
        self.insertTail(objectt)

    """
    Inserta un objeto, en la posicion siguiente al nodo u objeto dado, retorna True solo si el proceso es ejecutado exitosamente.  
    """
    def insert(self, node, objectt): # Recibe como parametro node y objectt, donde, node puede ser Objeto o Nodo y objectt es un objeto

        if(type(node) == ListNode): #Proceso a realizar si se entrega un nodo, en el parametro node
            try:
                #Para que el se inserte al lado del nodo dado, este debe estar en la lista
                if(node.next == None):
                    self.add(objectt)
                else:
                    newNode = ListNode(objectt, node.next)
                    node.next = newNode
                self.__size +=1
                return True
            except Exception:
                return False
        else: #Proceso a realizar si se entrega un objeto, en el parametro node
            try:
                if(node != None):
                    node = self.search(node)
                    if(node != None):
                        return self.insert(node, objectt)
                    else:
                        return False
            except Exception:
                return False

    """
    Inserta un objeto, en la cabeza de la lista, retorna True solo si el proceso es ejecutado exitosamente. 
    """
    def insertHead(self,  objectt):  # Recibe como parametro objectt, que puede ser Objeto o Nodo
        try:
            if(self.isEmpty()):
                self.head = ListNode(objectt)
                self.tail = self.head
            else:
                self.head = ListNode(objectt, self.head)
            self.__size += 1
            return True
        except Exception:
            return False

    """
    Inserta un objeto, en la al final de la lista, es decir, la cola, retorna True solo si el proceso es ejecutado exitosamente. 
    """
    def insertTail(self, objectt):  # Recibe como parametro objectt, que puede ser Objeto o Nodo
        try:
            if(self.isEmpty()):
                self.head = ListNode(objectt)
                self.tail = self.head
            else:
                self.tail.next = ListNode(objectt)
                self.tail = self.tail.next
                self.tail.next = self.head
            self.__size += 1
            return True
        except Exception:
            return False

    """
    Elimina el objeto dado como parametro, siempre y cuando este este en la lista, retorna True
    si el proceso es ejecutado exitosamente.
    """
    def remove(self, objectt):

        if(type(objectt) == ListNode): #Proceso a realizar si se entrega un nodo, en el parametro objectt
            nodeB = self.search(objectt.getObject())

            if(nodeB != None):
                """
                Para eliminar un nodo hay que tener en cuenta tres situaciones, que este este en la 
                cabeza, en el cuerpo o la cola de la lista. 
                """

                if(nodeB.isEquals(self.head)): #Si objectt esta en la cabeza 
                    
                    self.tail.next = self.head.next
                    self.head = self.head.next

                elif(nodeB.isEquals(self.tail)): #Si objectt esta en la cola
                    temp = self.getBeforeTo(nodeB)
                    self.tail = temp
                    self.tail.next = self.head

                else: #Si objectt esta en el cuerpo
                    self.getBeforeTo(nodeB).next = nodeB.next;
                    nodeB.next = None;
                self.__size -= 1
                return True
            else:
                return False
        else: #Proceso a realizar si se entrega un nodo, en el parametro objectt
            lNode = ListNode(objectt)
            return self.remove(lNode)

    """
    Busca el nodo u objeto en la lista, y si este se encuentra retorna True
    """
    def contains(self, objectt):  # Recibe como parametro un objeto o Nodo
        if(self.search(objectt) == None):
            return False
        return True

    """
    Debido a que en python no hay arreglos predefinidos, se retorna una lista con
    los datos de la Lista Circular.
    """
    def toArray(self):  
        arreglo = [None]*(self.__size)  
        it = iter(self)
        cont = 0
            
        for i in range(0,self.__size):
            inode = next(it)
            arreglo[cont] = inode.getObject()
            cont += 1

        return arreglo


    """
    Metodo para convertir un arreglo (lista de python) en una lista enlazada sencilla
    """
    def backToList(self, array): #Retorna una Lista Circular y recibe un arreglo como parametro
        head = CircularList()
        for i in array:
            head.add(i)
        return head
    
    """
    Retorna el nodo previo a al nodo entregado como parametro.
    """
    def getBeforeTo(self, node): #Parametro Node puede ser un nodo u objeto 
        if(type(node)==ListNode):
            it = iter(self)
            inode =ListNode()

            for i in range(0,self.__size):
                inode = next(it)
                if(inode.next!= None):
                    if(inode.next.getObject()==node.getObject()):
                        return inode
                inode = next(it)
            return None
        else:
            return self.getBeforeTo(ListNode(node))

    """
    Retorna el nodo posterior a al nodo entregado como parametro.
    """
    def getNextTo(self, node): #Parametro Node puede ser un nodo u objeto 
        if(type(node)==ListNode):
            node.getObject()
            it = iter(self)
            inode =ListNode()

            for i in range(0,self.__size):
                inode = next(it)
                if(inode.getObject()== node.getObject()):
                    return inode.next
                inode = next(it)
            return None
        else:
            return self.getNextTo(ListNode(node))
    
    """
    Crea una sublista desde el primer parametro dado hasta el segundo, solo si estos se encuentran en
    la lista.
    """
    def subList(self,fromm,to): #Retorna una sublista, desde el parametro fromm hasta el parametro to, si es que estos parametros existen, estos
                                # parametros pueden ser nodos u objetos
        if(type(fromm) == ListNode and type(to) == ListNode):
            if(self.contains(fromm) and self.contains(to)):
                array = self.toArray()
                return self.backToList(array[array.index(fromm.getObject()):array.index(to.getObject())+1])
            return None
        else:
            beg = ListNode(fromm)
            end = ListNode(to)

            return self.subList(beg,end)

    """
    Ordena la lista usando metodos predefinidos de una lista de python
    """
    def sortList(self): # No recibe parametros
        
        array = self.toArray();
        array.sort()
        bk = self.backToList(array)
        self.head = bk.head
        self.tail = bk.tail

    """
    Sobreescribir el metodo __str__ para imprimir la Lista, retorna un string
    """
    def __str__(self):
        output = ""
        it = iter(self)
        inode = ListNode()
        
        for i in range(0,self.__size):
            inode = next(it)
            if(inode.next == self.head and i!= 0):
                output += ("ListNode[ object= {0}, next= {1} (head)".format(inode.getObject(),inode.next.getObject()))
                for x in range(0,self.__size):
                    output += "]"
            else:
                output += ("ListNode[ object= {0}, next=".format(inode.getObject()))
        
        return  output

    """
    Metodo __iter__ sobreescrito para recorrer la Lista
    """
    def __iter__(self): #Metodo para crear iteradores
        self.__inode = self.head
        return self
    
    """
    Metodo __next__ sobreescrito para obtener cada nodo de la lista
    """
    def __next__(self):  #Metodo para moverse en el iterador de la lista
        if(self.__inode!=None):
            tmp = self.__inode
            self.__inode = self.__inode.next
            return tmp
        else:
            return None
