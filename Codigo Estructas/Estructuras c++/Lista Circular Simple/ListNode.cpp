#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;
class ListNode {
	
	    /*

    La lógica de esta estructura cabe en saber cuando la lista ya hizo el recorrido completo con el fin de parar este proceso ; Por lo cual a la hora de implementar los métodos el atributo .Next va a ser fundamental para identificar la cabeza o la cola (Según el recorrido implementado) ,así parando el proceso y eliminando los nulls

    */

	private:
	std::any object;
	ListNode *inode;
	public:
	ListNode *next;
	
	ListNode::ListNode()
	{
		this->object = std::any();
		this->next = nullptr;
	}
	
	ListNode::ListNode(std::any object)
	{
		this->object = object;
		this->next = nullptr;
	}
	
	ListNode::ListNode(std::any object, ListNode *next)
	{
		this->object = object;
		this->next = next;
	}
	
	std::any ListNode::getObject()
	{
		try
		{
			return object;
		}
		catch (const std::runtime_error &e)
		{
			return std::any();
		}
	}
	
	void ListNode::setObject(std::any object)
	{
		this->object = object;
	}
	
	bool ListNode::isEquals(std::any object)
	{
		if (this->getObject().toString().equals(object.toString()))
		{
			return true;
		}
		return false;
	}
	
	bool ListNode::isEquals(ListNode *node)
	{
		if (this->toString() == node->toString())
		{
			return true;
		}
		return false;
	}
	
	std::wstring ListNode::toString()
	{
	
		ListaCircular *ls = new ListaCircular();
	
		ListNode::const_iterator i = this->begin();
		ListNode *inode;
		do
		{
			inode = i->next();
			ls->push_back(inode->getObject());
		} while (inode->next != this);
	
		delete ls;
		return ls->toString();
	}
	
	Iterator<ListNode*> *ListNode::iterator()
	{
		inode = this;
		return new IteratorAnonymousInnerClass(this);
	}
	
	
	bool ListNode::IteratorAnonymousInnerClass::hasNext()
	{
		return !outerInstance->inode->next->empty();
	}
	
	ListNode *ListNode::IteratorAnonymousInnerClass::next()
	{
		if (!outerInstance->inode->empty())
		{
			ListNode *tmp = outerInstance->inode;
			outerInstance->inode = outerInstance->inode->next;
			return tmp;
		}
		else
		{
			return nullptr;
		}
	}
};

