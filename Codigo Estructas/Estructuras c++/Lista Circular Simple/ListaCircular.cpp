#pragma once
#include "ListNode.h"
#include "ListaCircular.h"
#include <vector>
#include <any>
using namespace std

/*

La caracteristica mas llamativa con respecto a las listas ciclicas es a la hora de recorrer la estructura ya que como su nombre lo indica 
esta en constante ciclo , es decir , no tiene final ; Por lo cual el termino "Null" donde significaba anteriormente el final, desaparecio.

Si no hay un "Stop" , la lista nunca para de recorrerse , en consecuencia siendo una estructura inutilizable (Siendo el recorrido una fundamento para los demas métodos) ; Para solucionar este problema , se tiene que tener en cuenta el Nodo cabeza y el Nodo cola para así cuando pase de cola a cabeza para el proceso (Esto en el caso de la Lista Enlazada Circular Sencilla).

*/


class ListaCircular : public ListInterface, public std::vector<ListNode*>{
	
	private:
	ListNode *inode;
	int size = 0;

	public:
	ListNode *head;
	ListNode *tail;
	
	ListaCircular::ListaCircular()
	{
	clear();
	}

	ListaCircular::ListaCircular(std::any object)
	{
		push_back(object);
	}
	
	bool ListaCircular::empty()
	{
		return head->empty();
	}
	
	int ListaCircular::getSize()
	{
		return size;
	}
	
	void ListaCircular::clear()
	{
		head = nullptr;
		tail = nullptr;
		size = 0;
	}
	
	std::any ListaCircular::getHead()
	{
		return head;
	}
	
	std::any ListaCircular::getTail()
	{
		return tail;
	}
	
	ListNode *ListaCircular::search(std::any object)
	{
	
		ListaCircular::const_iterator i = this->begin();
		ListNode *inode;
	
		for (int x = 0; x < size; x++)
		{
			inode = i->next();
			if (inode->getObject().equals(object))
			{
	
				return inode;
			}
		}
		return nullptr;
	}
	
	ListNode *ListaCircular::search(ListNode *object)
	{
	
		return search(object->getObject());
	}
	
	bool ListaCircular::add(std::any object)
	{
		return insertTail(object);
	}
	
	bool ListaCircular::insert(ListNode *node, std::any object)
	{
		try
		{
			if (node->next->empty())
			{
				push_back(object);
			}
			else
			{
	
				ListNode *newNode = new ListNode(object);
				newNode->next = node->next;
				node->next = newNode;
			}
			this->size++;
			return true;
		}
		catch (const std::runtime_error &e)
		{
			return false;
		}
	}
	
	bool ListaCircular::insert(std::any ob, std::any object)
	{
		try
		{
			if (ob.has_value())
			{
				ListNode *node = this->search(ob);
				if (!node->empty())
				{
					return insert(node, object);
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		catch (const std::runtime_error &e)
		{
			return false;
		}
	}
	
	bool ListaCircular::insertHead(std::any object)
	{
		try
		{
			if (empty())
			{
				head = new ListNode(object);
				tail = head;
			}
			else
			{
				head = new ListNode(object, head);
				tail->next = head;
			}
			this->size++;
			return true;
		}
		catch (const std::runtime_error &e)
		{
			return false;
		}
	}
	
	bool ListaCircular::insertTail(std::any object)
	{
		try
		{
			if (empty())
			{
				head = new ListNode(object);
				tail = head;
			}
			else
			{
				tail->next = new ListNode(object);
				tail = tail->next;
				tail->next = head; // El Nodo posterior a la cola es la cabeza por lo cual al momento de insertar una nueva cola este objeto quedaría entre la cola anterior y la cabeza (Depende de como el programador lo implemente , esto puede variar)
			}
			this->size++;
			return true;
		}
		catch (const std::runtime_error &e)
		{
			return false;
		}
	}
	
	bool ListaCircular::remove(ListNode *node)
	{
	
		ListNode *nodeB = this->search(node->getObject());
	
		if (!nodeB->empty())
		{
			if (nodeB->getObject() == head->getObject())
			{
				ListNode *temp = this->head->next;
				this->tail->next = temp;
				this->head = temp;
				// En este caso como el Nodo a eliminar es la cabeza , el Nodo anterior es la cola , por lo cual esta se le asigna como la nueva cabeza 
			}
			else if (nodeB->getObject() == tail->getObject())
			{
				this->getBeforeTo(nodeB)->next = head; // En este caso como el Nodo a eliminar es la cola , se establece como la nueva cola el nodo anterior 
			}
			else
			{
				this->getBeforeTo(nodeB)->next = nodeB->next;
	
				nodeB->next = nullptr;
			}
	
			this->size--;
			return true;
		}
		return false;
	}
	
	bool ListaCircular::remove(std::any object)
	{
		ListNode *lNode = new ListNode(object);
		delete lNode;
		return remove(lNode);
	}
	
	bool ListaCircular::contains(std::any object)
	{

		if (search(object) == nullptr)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	std::vector<std::any> ListaCircular::toArray()
	{
		std::vector<std::any> arreglo(size);

		ListaCircular::const_iterator i = this->begin();
		ListNode *inode;
		int cont = 0;

		for (int x = 0; x < size; x++)
		{
			inode = i->next();
			arreglo[cont] = inode->getObject();
			cont++;
		}
		return arreglo;
	}

	std::vector<std::any> ListaCircular::toArray(std::vector<std::any> &object)
	{

		std::vector<std::any> arreglo(size + object.size());
		std::vector<std::any> arreglo2 = toArray();
		int cont = 0;
		
		while (cont < object.size() + size)
		{
			if (cont == size)
			{
				for (auto object2 : object)
				{
					arreglo[cont] = object2;
					cont++;
				}

				break;
			}
			else
			{
				arreglo[cont] = arreglo2[cont];
			}
			cont++;
		}
		return arreglo;
	}

	ListaCircular *ListaCircular::backToList(std::vector<std::any> &objects)// Un nuevo método el cual se encarga de recorrer un arreglo y almacenar estos valores en la lista
	{
		ListaCircular *lista = new ListaCircular();
		for (auto object : objects)
		{
			lista->add(object);
		}
		return lista;
	}

	std::any ListaCircular::getBeforeTo()
	{

		ListaCircular::const_iterator i = this->begin();
		ListNode *inode;
		while ((inode = i->next()) != nullptr)
		{

			if (inode->next.getObject() == this->tail.getObject())
			{
				return inode->getObject();
			}
		}

		return std::any();
	}

	ListNode *ListaCircular::getBeforeTo(ListNode *node)
	{
		ListaCircular::const_iterator i = this->begin();
		ListNode *inode;
		while ((inode = i->next()) != nullptr)
		{

			if (inode->next.getObject() == node->getObject())
			{
				return inode;
			}


		}

		return nullptr;
	}

	std::any ListaCircular::getNextTo()
	{
		return head->next;
	}

	std::any ListaCircular::getNextTo(ListNode *node)
	{
		ListaCircular::const_iterator i = this->begin();
		ListNode *inode;
		while ((inode = i->next()) != nullptr)
		{
			if (inode->getObject() == node->getObject())
			{
				return inode->next.getObject();
			}
		}

		return std::any();
	}

	ListNode *ListaCircular::getnextTo(ListNode *node)
	{
		ListaCircular::const_iterator i = this->begin();
		ListNode *inode;
		while ((inode = i->next()) != nullptr)
		{

			if (inode->getObject() == node->getObject())
			{
				return inode->next;
			}
		}
		return nullptr;
	}

	ListaCircular *ListaCircular::subList(ListNode *from, ListNode *to)
	{
		try
		{
			ListaCircular *sublist = new ListaCircular();
			if (from != nullptr && to != nullptr)
			{
				ListaCircular::const_iterator i = this->begin();
				ListNode *inode;
				bool frExist = false;
				while ((inode = i->next()) != nullptr)
				{
					if (inode->getObject().equals(to->getObject()))
					{
						sublist->add(inode->getObject());
						return sublist;
					}
					else if (inode->getObject().equals(from->getObject()) || frExist)
					{
						sublist->add(inode->getObject());
						frExist = true;
					}


				}
			}
			return nullptr;
		}
		catch (const std::runtime_error &e)
		{
			return nullptr;
		}
	}

	ListaCircular *ListaCircular::subList(std::any from, std::any to)
	{
		ListNode *nfrom = new ListNode(from);
		ListNode *nto = new ListNode(to);

		delete nto;
		delete nfrom;
		return this->subList(nfrom, nto);
	}

	ListaCircular *ListaCircular::sortList()
	{
		try
		{
			std::vector<std::any> array = toArray(); //Almacena la lista en un arreglo
			Arrays::sort(array);//Utiliza el método .sort tipicos de los arreglos para organizar la estructura 

			return backToList(array); // Convierte el arreglo a una lista
		}
		catch (const std::runtime_error &e)
		{
			return nullptr;
		}
	}
	
		std::wstring ListaCircular::toString()
	{
		std::wstring output = L"";
		ListaCircular::const_iterator i = this->begin();
		ListNode *inode;
		for (int x = 0; x < size; x++)
		{
			inode = i->next();

			if (inode->next == this->head && x != 0)
			{
				output += L"ListNode{" + L"object=" + inode->getObject() + L", next=" + inode->next.getObject() + L"(head)";
				for (int z = 0; z < size; z++)
				{
					output += L'}';
				}

			}
			else
			{
				output += L"ListNode{" + L"object=" + inode->getObject() + L", next=";
			}
		}

		return output;
	}

	Iterator<ListNode*> *ListaCircular::iterator()
	{
		inode = head;
		return new IteratorAnonymousInnerClass(this);
	}

	ListaCircular::IteratorAnonymousInnerClass::IteratorAnonymousInnerClass(ListaCircular *outerInstance) : outerInstance(outerInstance)
	{
	}

	bool ListaCircular::IteratorAnonymousInnerClass::hasNext()
	{
		return inode->next != nullptr;
	}

	ListNode *ListaCircular::IteratorAnonymousInnerClass::next()
	{
		if (inode != nullptr)
		{
			ListNode *tmp = inode;
			inode = inode->next;
			return tmp;
		}
		else
		{
			return nullptr;
		}
	}

	void ListaCircular::rec(ListNode *node)
	{
		if (node->next != nullptr)
		{
			rec(node->next);
			// <- ;) ->
		}
		out::println(node->toString());
	}
};



