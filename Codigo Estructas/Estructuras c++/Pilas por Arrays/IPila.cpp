#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;

//Interfaz de la pila basada en arrays

	class IPila
	{
	public:
		virtual void clear() = 0;

		virtual bool isEmpty() = 0;

		virtual std::any peek() = 0;

		virtual std::any pop() = 0;

		virtual bool push(std::any object) = 0;

		virtual int size() = 0;

		virtual bool search(std::any object) = 0;

		virtual void sort() = 0;

		virtual void reverse() = 0;

		virtual std::wstring toString() = 0;
	};
