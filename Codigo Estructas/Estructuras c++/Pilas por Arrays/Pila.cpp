#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;

class Pila : public IPila{

	    /*
    Nota : Es una pila básada en los arreglos del propio lenguaje , volviendola así una estructura estática 
    */
	
	private:
		int size_Conflict = 0;
		std::vector<std::any> array;
		int top = 0; // Es necesario saber la posición del top ya que es un arreglo
	
	Pila::Pila(int size) 
	{
		this->size_Conflict = size;
		this->array = std::vector<std::any>((size > 0) ? size : 1);
		clear();
	}

	void Pila::clear() //Recorre todas las posiciones del arreglo volviendo en cada posición su elemento correspondiente en null
	{
		for (int i = 0; i < array.size(); i++)
		{
			array[i] = std::any();
		}
		top = -1; // Se establece el top "-1" debido a que la estructura esta vacia
	}

	bool Pila::isEmpty()
	{
		return !array[0].has_value(); // Si en la posición 0 , el objeto es igual a null , significa que la estructura esta vacia
	}

	std::any Pila::peek()
	{
		return (!isEmpty()) ? array[top] : std::any();  // Extrae el top que es el ultimo elemento que ha sido ingresado en la pila (Siempre y cuando que el método isEmpty verifique que almenos haya un elemento la estructura)
	}

	std::any Pila::pop()
	{
		if (!isEmpty())
		{
			std::any object = array[top]; // Se almacena el top de la pila en la variable object 
			array[top--] = std::any(); // Se "Elimina" el top declarandolo como null y le resta -1 a top
			return object; // Retorna el la variable con el antiguo top
		}
		else
		{
			return std::any();
		}
	}

	bool Pila::push(std::any object)
	{
		if (top + 1 < size_Conflict)
		{
			try
			{
				array[++top] = object; // Se le agrega un nuevo espacio al array y en ese sitio se almacena la variable ingresada en el parametro
				return true;
			}
			catch (const std::runtime_error &e)
			{
				std::wcout << e << std::endl;
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	int Pila::size()
	{
		return top + 1; // Retorna ++top (El tamaño de la estructura) debido a que un array empieza desde 0 
	}

	bool Pila::search(std::any object)
	{
		std::vector<std::any> copia(size_Conflict);
		bool correcto = false;
		for (int i = 0; i < size_Conflict; i++) // Se recorre el arreglo
		{
			copia[i] = pop(); // Se extrae elemento por elemento hasta que encuentre el objeto ingresado en el parametro , si no es así retorna el valor orignal de booleano (False)
			if (copia[i] == object)
			{
				correcto = true;
			}
		}

		for (int i = 0; i < size_Conflict; i++)
		{
			array[i] = copia[(size_Conflict - 1) - i];
		}

		top = size_Conflict - 1;

		return correcto;
	}

	void Pila::sort() //Organiza los elementos de la pila 
	{
		std::vector<std::any> resultado(size_Conflict);
		std::any dato;
		while (array.size() != 0)
		{
			dato = Eliminar(array);
			this->Situar(dato, resultado);
		}
		array = resultado;
		//Arrays.sort(array);
	}

	void Pila::reverse() // Invierte los elementos de la pila
	{
		std::vector<std::any> newArray(size_Conflict);
		for (int i = 0; i < size_Conflict; i++)
		{
			newArray[i] = pop();
		}
		array = newArray;
		top = size_Conflict - 1;
	}

	std::wstring Pila::toString() // Crea una concatenación de Strings con todos los elementos del array (Pila)
        return "ArrayStack{" +

	{
		return L"ArrayStack{" + L"size=" + std::to_wstring(size_Conflict) + L", array=" + Arrays->toString(array) + L", top=" + std::to_wstring(top) + StringHelper::toString(L'}');
	}

	std::any Pila::Eliminar(std::vector<std::any> &arrayAEliminar)
	{
		std::vector<std::any> newArray(size_Conflict - 1);
		for (int i = 0; i < size_Conflict - 1; i++)
		{
			newArray[i] = arrayAEliminar[i];
		}
		array = newArray;
		size_Conflict = array.size();
		top = size_Conflict - 1;

		return arrayAEliminar[arrayAEliminar.size() - 1];
	}

	void Pila::AgregarAlFinal(std::vector<std::any> &arrayAInsertar, std::any objectoAAgregar)
	{
		std::vector<std::any> newArray(size_Conflict + 1);//Aumenta la capacidad de almacenamiento
		for (int i = 0; i < size_Conflict + 1; i++)
		{
			if (i != size_Conflict)
			{
				newArray[i] = arrayAInsertar[i]; //Agrega los elementos en el nuevo array
			}
			else
			{
				newArray[i] = objectoAAgregar;// Y al final como su "Top" el ultimo objeto ingresado
			}

		}

		top = size_Conflict; // El tamaño aumenta en +1
		array = newArray;
		size_Conflict = array.size();
	}

	void Pila::Situar(std::any dato, std::vector<std::any> &resultado)
	{
		std::vector<std::any> auxiliar(size_Conflict);
		if (resultado.empty())
		{
			AgregarAlFinal(resultado,dato);
			//auxiliar.
		}
		else
		{
			if (dato.toString().compareTo(resultado[resultado.size() - 1].toString()) <= 0)
			{
				AgregarAlFinal(resultado,dato);
			}
		}
	}	
};
