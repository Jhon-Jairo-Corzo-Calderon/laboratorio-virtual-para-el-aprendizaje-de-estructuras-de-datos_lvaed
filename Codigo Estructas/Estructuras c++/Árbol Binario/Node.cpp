#include "Node.h"
#pragma once
#include <string>
#include <any>
Using namespace std
	
	class Node
	{
		private:
			std::any object;
		public:
			Node *right;
			Node *left;
		
		Node::Node(std::any o)
		{
			this->object = o;
			this->right = nullptr;
			this->left = nullptr;
		}
	
		Node::Node(Node *left, std::any object, Node *right)
		{
			this->object = object;
			this->left = left;
			this->right = right;
		}
	
		std::any Node::GetObject()
		{
			return object;
		}
	
		int Node::Grado()
		{
			if (right == nullptr & left == nullptr)
			{
				return 0;
			}
			else if ((right == nullptr & left != nullptr) | (right != nullptr & left == nullptr))
			{
				return 1;
			}
			else
			{
				return 2;
			}
		}
	
		std::wstring Node::toString()
		{
			return L"Node{ left=" + left + L", right=" + right + L", object=" + object + L'}';
		}

	}
	
