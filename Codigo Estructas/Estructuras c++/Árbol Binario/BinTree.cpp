#include "BinTree.h"
#pragma once
#include "Node.h"
#include <string>
#include <stdexcept>
Using namespace std

	class BinTree
	{
		private:
			int gradoArbol = 0, alturaArbol = 0, cont = 1;
			std::wstring preorder = L"", inorder = L"", posorder = L"";
			bool search = false;
		public:
			Node *root;
			
		//Constructores
		BinTree::BinTree()
		{
			gradoArbol = alturaArbol = 0;
			this->root = nullptr;
		}

		//Método SubBinTree
		BinTree::BinTree(std::any o)
		{
			gradoArbol = alturaArbol = 0;
			this->root = SubBinTree(nullptr, o, nullptr);
		}
	
		Node *BinTree::SubBinTree(Node *left, std::any o, Node *right)
		{
			Node *nodoARetornar = new Node(left, o, right);
			CalcularGradoArbol(nodoARetornar);
			return nodoARetornar;
		}
		//Método IsEmpty
		bool BinTree::IsEmpty()
		{
			if (!root->GetObject().has_value() & root->right == nullptr & 	root->left == nullptr)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		//Método Root
		bool BinTree::Root(std::any o)
		{
			try
			{
				this->root = SubBinTree(nullptr,o,nullptr);
				CalcularGradoArbol(this->root);
				return true;
			}
			catch (const std::runtime_error &e)
			{
				e.printStackTrace();
				return false;
			}
		}
		//Método InsertLeft
		bool BinTree::InsertLeft(std::any o)
		{
			try
			{
				if (!IsEmpty())
				{
					root->left = SubBinTree(nullptr, o, nullptr);
					return true;
				}
				else
				{
					Root(o);
					return false;
				}
			}
			catch (const std::runtime_error &e)
			{
				e.printStackTrace();
				return false;
			}
		}
		//Método InsertRight
		bool BinTree::InsertRight(std::any o)
		{
			try
			{
				if (!IsEmpty())
				{
					root->right = SubBinTree(nullptr, o, nullptr);
					return true;
				}
				else
				{
					Root(o);
					return false;
				}
			}
			catch (const std::runtime_error &e)
			{
				e.printStackTrace();
				return false;
			}
		}
		//Métodos necesarios para realizar el search en el arbol binario
		void BinTree::Search(Node *n, std::any o)
		{
			if (!IsEmpty())
			{
				if (n->GetObject().toString().compareTo(o.toString()) == 0)
				{
					search = true;
				}
				if (n->right != nullptr)
				{
					if (n->right->GetObject().toString().compareTo(o.toString()) == 0)
					{
						search = true;
					}
					else
					{
						Search(n->right, o);
					}
				}
				if (n->left != nullptr)
				{
					if (n->left->GetObject().toString().compareTo(o.toString()) == 0)
					{
						search = true;
					}
					else
					{
						Search(n->left, o);
					}
				}
			}
		}
	
		bool BinTree::SearchObject(std::any o)
		{
			Search(root,o);
			if (search == true)
			{
				search = false; //Se vuelve a dejar la variable booleana en false para en un futuro poder volverla a usar
				return true;
			}
			else
			{
				return false;
			}
		}

		//Método Eliminar
		void BinTree::Eliminar(Node *ROOT, Node *nodoAEliminar)
		{
			if (!IsEmpty())
			{
				if (ROOT->GetObject().toString().compareTo(nodoAEliminar->GetObject().toString()) == 0)
				{
					if (ROOT != root)
					{
						ROOT->right = nullptr;
						ROOT->left = nullptr;
					}
					else
					{
						Root(std::any());
					}
					return;
				}
				if (ROOT->right != nullptr)
				{
					if (ROOT->right->GetObject().toString().compareTo(nodoAEliminar->GetObject().toString()) == 0)
					{
						ROOT->right = nullptr;
					}
					else
					{
					   Eliminar(ROOT->right, nodoAEliminar);
					}
				}
				if (ROOT->left != nullptr)
				{
					if (ROOT->left->GetObject().toString().compareTo(nodoAEliminar->GetObject().toString()) == 0)
					{
						ROOT->left = nullptr;
					}
					else
					{
						Eliminar(ROOT->left, nodoAEliminar);
					}
				}
			}
		}

		//Métodos necesarios para realizar el preorder
		void BinTree::PreOrder(Node *n)
		{
			if (!IsEmpty())
			{
				preorder += std::any_cast<std::wstring>(n->GetObject());
				if (n->left != nullptr)
				{
					PreOrder(n->left);
				}
				if (n->right != nullptr)
				{
					PreOrder(n->right);
				}
			}
		}
		
		std::wstring BinTree::GetPreOrder()
		{
		PreOrder(root);
		return preorder;
		}

		//Métodos necesarios para realizar el inorder
		void BinTree::InOrder(Node *n)
		{
			if (!IsEmpty())
			{
				if (n->left != nullptr)
				{
					InOrder(n->left);
				}
				inorder += static_cast<std::wstring>(n->GetObject());
				if (n->right != nullptr)
				{
					InOrder(n->right);
				}
			}
		}
	
		std::wstring BinTree::GetInOrder()
		{
			InOrder(root);
			return inorder;
		}
		//Métodos necesarios para realizar el postorder
		void BinTree::PosOrder(Node *n)
		{
			if (!IsEmpty())
			{
				if (n->left != nullptr)
				{
					PosOrder(n->left);
				}
				if (n->right != nullptr)
				{
					PosOrder(n->right);
				}
				posorder += static_cast<std::wstring>(n->GetObject());
			}
		}
	
		std::wstring BinTree::GetPosOrder()
		{
			PosOrder(root);
			return posorder;
		}

		//Métodos necesarios para cálcular el grado del arbol binario
		void BinTree::CalcularGradoArbol(Node *n)
		{
			if (!IsEmpty())
			{
				if (n->Grado() == 1)
				{
					gradoArbol = 1;
					if (n->right != nullptr)
					{
						CalcularGradoArbol(n->right);
					}
					if (n->left != nullptr)
					{
						CalcularGradoArbol(n->left);
					}
				}
				else if (n->Grado() == 2)
				{
					gradoArbol = 2;
				}
			}
			else
			{
				gradoArbol = 0;
			}
	
		}
	
		int BinTree::GetGrado()
		{
			CalcularGradoArbol(root);
			return gradoArbol;
		}
		//Métodos necesarios para cálcular la altura del arbol binario
		void BinTree::CalcularAlturaArbol(Node *n)
		{
			if (!IsEmpty())
			{
				if (n->right != nullptr)
				{
					CalcularAlturaArbol(n->right);
				}
				if (n->left != nullptr)
				{
					CalcularAlturaArbol(n->left);
				}
				cont++;
			}
			else
			{
				alturaArbol = 1;
				return;
			}
	
			if (cont % 2 != 0)
			{
				alturaArbol = (cont / 2) + 1;
			}
			else
			{
				alturaArbol = cont / 2;
			}
		}
	
		int BinTree::GetAltura()
		{
			CalcularAlturaArbol(root);
			return alturaArbol;
		}
	}


