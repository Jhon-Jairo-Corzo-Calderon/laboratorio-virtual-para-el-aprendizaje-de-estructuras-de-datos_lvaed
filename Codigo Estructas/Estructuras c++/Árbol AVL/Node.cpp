#include "Node.h"
#include <string>
#include <any>

Using namespace std

	//ATRIBUTOS
	class Node{
		
		private:
			std::any object;
		public:
			Node *right;
			Node *left;
		private:
			int id = 0;
			int balance = 0;

		//MÉTODOS Y CONSTRUCTORES
		Node::Node(std::any o, int id)
		{
			this->object = o;
			this->right = nullptr;
			this->left = nullptr;
			this->id = id;
			this->balance = 0;
		}
	
		Node::Node(Node *left, std::any object, Node *right, int id)
		{
			this->object = object;
			this->left = left;
			this->right = right;
			this->id = id;
			this->balance = 0;
		}
	
		int Node::GetID()
		{
			return id;
		}
	
		int Node::GetBalance()
		{
			return balance;
		}
	
		void Node::SetBalance(int balance)
		{
			this->balance = balance;
		}
	
		std::any Node::GetObject()
		{
			return object;
		}
		
		//Método del cálculo del grado de un nodo
		int Node::Grado()
		{
			if (right == nullptr & left == nullptr)
			{
				return 0;
			}
			else if ((right == nullptr & left != nullptr) | (right != nullptr & left == nullptr))
			{
				return 1;
			}
			else
			{
				return 2;
			}
		}
	
		std::wstring Node::toString()
		{
			return L"Node{ left=" + left + L", right=" + right + L", object=" + object + L", id=" + std::to_wstring(id) + StringHelper::toString(L'}');
		}
	};
	

	

