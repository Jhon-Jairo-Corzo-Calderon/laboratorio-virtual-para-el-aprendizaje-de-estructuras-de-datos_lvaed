#include "AVLTree.h"
#pragma once
#include "Node.h"
#include <string>
#include <any>

Using namespace std


class AVLTree{

	//ATRIBUTOS
	private:
		std::wstring preorder = L"", inorder = L"", posorder = L"";
		bool search = false;
	public:
		Node *root;
	
	//MÉTODOS Y CONSTRUCTORES

	AVLTree::AVLTree()
	{
		this->root = nullptr;
	}

	AVLTree::AVLTree(std::any o, int id)
	{
		this->root = new Node(nullptr, o, nullptr, id);
	}

	bool AVLTree::IsEmpty()
	{
		if (root == nullptr)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//Método InsertBalanced, propio de los arboles balanceados como el árbol AVL
	Node *AVLTree::InsertBalanced(Node *insert, Node *main)
	{
		Node *newMain = main;
		if (insert->GetID() < main->GetID())
		{
			if (main->left == nullptr)
			{
				main->left = insert;
			}
			else
			{
				main->left = InsertBalanced(insert, main->left);
				if (Getfe(main->left) - Getfe(main->right) == 2)
				{
					if (insert->GetID() < main->left->GetID())
					{
						newMain = Rsi(main);
					}
					else
					{
						newMain = Rdi(main);
					}
				}
			}
		}
		else if (insert->GetID() > main->GetID())
		{
			if (main->right == nullptr)
			{
				main->right = insert;
			}
			else
			{
				main->right = InsertBalanced(insert, main->right);
				if (Getfe(main->right) - Getfe(main->left) == 2)
				{
					if (insert->GetID() > main->right->GetID())
					{
						newMain = Rsd(main);
					}
					else
					{
						newMain = Rdd(main);
					}
				}
			}
		}
		else
		{
			return nullptr;
		}

		if (main->left == nullptr & main->right != nullptr)
		{
			main->SetBalance(main->right->GetBalance() + 1);
		}
		else if (main->right == nullptr & main->left != nullptr)
		{
			main->SetBalance(main->left->GetBalance() + 1);
		}
		else
		{
			main->SetBalance(std::max(Getfe(main->left),Getfe(main->right)) + 1); //Se le suma 1 ya que el factor de equilibrio hace referencia al nivel ms 1
		}
		return newMain;
	}

	//Método Insert
	bool AVLTree::Insert(std::any o, int id)
	{
		Node *n = new Node(nullptr,o,nullptr,id);
		if (!IsEmpty())
		{
			Node *newMain = InsertBalanced(n, root);
			if (newMain != nullptr)
			{
				root = newMain;
			}
			else
			{
				return false;
			}
		}
		else
		{
			root = n;
		}
		return true;
	}
	//Método de obtención del factor de equilibrio
	int AVLTree::Getfe(Node *n)
	{
		if (n != nullptr)
		{
			return n->GetBalance();
		}
		else
		{
			return -1;
		}
	}

	//Métodos de las rotaciones

	//Rotación simple a la izquierda
	Node *AVLTree::Rsi(Node *n)
	{
		Node *nl = n->left;
		n->left = nl->right;
		nl->right = n;
		n->SetBalance(std::max(Getfe(n->left), Getfe(n->right)) + 1); //Se le suma 1 ya que el factor de equilibrio hace referencia al nivel ms 1
		nl->SetBalance(std::max(Getfe(nl->left), Getfe(nl->right)) + 1);
		return nl;
	}

	//Rotación simple a la derecha
	Node *AVLTree::Rsd(Node *n)
	{
		Node *nl = n->right;
		n->right = nl->left;
		nl->left = n;
		n->SetBalance(std::max(Getfe(n->left), Getfe(n->right)) + 1); //Se le suma 1 ya que el factor de equilibrio hace referencia al nivel ms 1
		nl->SetBalance(std::max(Getfe(nl->left), Getfe(nl->right)) + 1);
		return nl;
	}

	//Rotación doble a la izquierda
	Node *AVLTree::Rdi(Node *n)
	{
		Node *nl;
		n->left = Rsd(n->left);
		nl = Rsi(n);
		return nl;
	}
	//Rotación doble a la derecha
	Node *AVLTree::Rdd(Node *n)
	{
		Node *nl;
		n->right = Rsi(n->right);
		nl = Rsd(n);
		return nl;
	}
	//Métodos necesarios para realizar el search en el árbol AVL
	Node search(Node root, int value)
	{
		if (root != nullptr)
		{
			if (root::GetID() == value)
			{
				return root;
			}
			else if (root::GetID() < value)
			{
				return search(root::right, value);
			}
			else
			{
				return search(root::left, value);
			}
		}
		return nullptr;
	}	

	Node search(int value)
	{
		return search(this->root, value);
	}

	//Método remove
	bool AVLTree::Remove(int id)
	{
		if (!IsEmpty())
		{
			Node *inode = root;
			Node *preInode = root;
			while (inode->GetID() != id)
			{
				preInode = inode;
				if (inode->GetID() > id)
				{
					inode = inode->left;
				}
				else
				{
					inode = inode->right;
				}
				if (inode == nullptr)
				{
					return false;
				}
			}
			if (inode->right == nullptr & inode->left == nullptr)
			{
				if (inode == root)
				{
					root = nullptr;
				}
				else if (preInode->left == inode)
				{
					preInode->left = nullptr;
				}
				else
				{
					preInode->right = nullptr;
				}
			}
			else if (inode->right == nullptr)
			{
				if (inode == root)
				{
					root = root::left;
				}
				else if (preInode->left == inode)
				{
					preInode->left = inode->left;
				}
				else
				{
					preInode->right = inode->left;
				}
			}
			else if (inode->left == nullptr)
			{
				if (inode == root)
				{
					root = root::right;
				}
				else if (preInode->left == inode)
				{
					preInode->left = inode->right;
				}
				else
				{
					preInode->right = inode->right;
				}
			}
			else
			{
				Node *rePre = inode, *re = inode, *aux = inode->right;
				while (aux != nullptr)
				{
					rePre = re;
					re = aux;
					aux = aux->left;
				}
				if (re != inode->right)
				{
					rePre->left = re->right;
					re->right = inode->right;
				}

				if (inode == root)
				{
					root = re;
				}
				else if (preInode->left == inode)
				{
					preInode->left = re;
				}
				else
				{
					preInode->right = re;
				}
				re->left = inode->left;
			}
		}
		else
		{
			return false;
		}
		return true;
	}

	//Métodos necesarios para realizar el preorder
	void AVLTree::PreOrder(Node *n)
	{
		if (!IsEmpty())
		{
			preorder += static_cast<std::wstring>(n->GetObject());
			if (n->left != nullptr)
			{
				PreOrder(n->left);
			}
			if (n->right != nullptr)
			{
				PreOrder(n->right);
			}
		}
	}

	std::wstring AVLTree::GetPreOrder()
	{
		PreOrder(root);
		return preorder;
	}

	//Métodos necesarios para realizar el inorder
	void AVLTree::InOrder(Node *n)
	{
		if (!IsEmpty())
		{
			if (n->left != nullptr)
			{
				InOrder(n->left);
			}
			inorder += static_cast<std::wstring>(n->GetObject());
			if (n->right != nullptr)
			{
				InOrder(n->right);
			}
		}
	}

	std::wstring AVLTree::GetInOrder()
	{
		InOrder(root);
		return inorder;
	}
	//Métodos necesarios para realizar el posorder
	void AVLTree::PosOrder(Node *n)
	{
		if (!IsEmpty())
		{
			if (n->left != nullptr)
			{
				PosOrder(n->left);
			}
			if (n->right != nullptr)
			{
				PosOrder(n->right);
			}
			posorder += static_cast<std::wstring>(n->GetObject());
		}
	}

	std::wstring AVLTree::GetPosOrder()
	{
		PosOrder(root);
		return posorder;
	}
};

