#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;
	
	   /*

    Anteriormente mencionado en la lista circular sencilla , las caracteristica principal de este tipo de listas "ciclicas" , es que su comienzo es su final y su final es su comienzo , haciendo que al momento de recorrerlas nunca se encuentren con el parámetro null ; La diferencia frente a la sencilla es que esta se pude recorrer tanto por derecha como izquierda , dando así una mejor versatilidad al momento de navegar por la estructura

    */

	
	class List : public IList, public std::vector<ListNode*>{
		
		private:
		ListNode *inode;  //Nodo iterable
		int size = 0;

		public:
		ListNode *head;  //Punteros para saber donde está el inicio y el fin
		ListNode *tail;
	
		List::List()
		{
			clear();
		}

		List::List(std::any object)
		{
			push_back(object);
		}
	
		bool List::empty()
		{
			return head == nullptr;
		}
	
		int List::getSize()
		{
			return size;
		}
	
		void List::clear()
		{
			head = nullptr;
			tail = nullptr;
			size = 0;
		}
	
		std::any List::getHead()
		{
			return head;
		}
	
		std::any List::getTail()
		{
			return tail;
		}
	
		ListNode *List::search(std::any object)
		{
			inode = head;
			for (int i = 0; i < this->size; i++)
			{
				if (inode->getObject() == object)
				{
					return inode;
				}
				else
				{
					inode = inode->next;
				}
			}
			return nullptr;
		}
	
		bool List::add(std::any object)
		{
			return insertTail(object);
		}
	
		bool List::insert(ListNode *node, std::any object)
		{
			try
			{
				ListNode *newNode = new ListNode(object);
				newNode->previous = node;
				(node->next)->previous = newNode; //Se usa los parentesis para así entenderlo un poco mejor :D
				newNode->next = node->next;
				node->next = newNode;
				this->size++;
				return true;
			}
			catch (const std::runtime_error &e)
			{
				return false;
			}
		}
	
		bool List::insert(std::any ob, std::any object)
		{
			try
			{
				if (ob.has_value())
				{
					ListNode *node = this->search(ob);
					if (node != nullptr)
					{
						return insert(node, object);
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			catch (const std::runtime_error &e)
			{
				return false;
			}
		}
	
		bool List::insertHead(std::any object)
		{
			try
			{
				if (empty())
				{
					head = new ListNode(object); //Se crea el nodo
					tail = head;
				}
				else
				{
					head->previous = new ListNode(object, head, tail);
					head = head->previous;
				}
				tail->next = head;
				this->size++;
				return true;
			}
			catch (const std::runtime_error &e)
			{
				return false;
			}
		}
	
		bool List::insertTail(std::any object)
		{
			try
			{
				if (empty())
				{
					head = new ListNode(object); //Se crea el nodo
					tail = head;
				}
				else
				{
					tail->next = new ListNode(object,head,tail); //tail hace referencia al nodo completo, tail.next hace referencia a la cajita del puntero
					tail = tail->next;
				}
				head->previous = tail;
				this->size++;
				return true;
			}
			catch (const std::runtime_error &e)
			{
				return false;
			}
		}
	
		bool List::remove(ListNode *node)
		{
			remove(node->getObject());
			return true;
		}
	
		bool List::remove(std::any object)
		{
			if (empty() == false)
			{
				if (head == tail && object == head->getObject())
				{
					head = nullptr;
					tail = nullptr;
				}
				else if (head->isEquals(object)) //object == head.getObject()
				{ 
					head = head->next;
					tail->next = head;
				}
				else
				{
					ListNode *previous = head, *temp = head->next;
					while (temp != head && temp->getObject() != object)
					{
						previous = previous->next;
						temp = temp->next;
					}
					if (temp != head)
					{
						previous->next = temp->next;
						if (temp == tail)
						{
							tail = previous;
						}
					}
				}
				size--;
			}
			return true;
		}
		bool List::contains(std::any object)
		{
		inode = head;
		do
		{
			if (inode::getObject() == object)
			{
				return true;
			}
			inode = inode->next;
		} while (inode != head); // Condición para parar el bucle (Empieza desde la cabeza y si se la encuentra otra vez significa que ya recorrio la estructura)
		return false;
		}
	
		std::vector<std::any> List::toArray()
		{
			std::vector<std::any> array(size);
			inode = head;
			for (int i = 0; i < size; i++)
			{
				array[i] = inode::getObject();
				inode = inode->next;
			}
			return array;
		}
		
		std::vector<std::any> List::toArray(std::vector<std::any> &object)
		{
			inode = head;
			for (int i = 0; i < size; i++)
			{
				object[i] = inode::getObject();
				inode = inode->next;
			}
			return object;
		}
		
		std::any List::getBeforeTo()
		{
			return getBeforeTo(tail)->getObject(); //Si invocan al método getBeforeTo que se encuentra sin parámetro se asume que me están pidiendo el objeto que se encuentra antes de la cola
		}
		
		ListNode *List::getBeforeTo(ListNode *node)
		{
			if (isEmpty() == false)
			{
				if (head == tail)
				{
					return head;
				}
				else if (node == head)
				{
					return tail;
				}
				else
				{
					ListNode *previous = nullptr;
					inode = head;
					do
					{
						if (inode::getObject() == node->getObject())
						{
							return previous;
						}
						else
						{
							previous = inode;
							inode = inode->next;
						}
					} while (inode != head);
				}
			}
			return nullptr;
		}
		
		std::any List::getNextTo()
		{
			return getNextTo(head); 
		}
		
		std::any List::getNextTo(ListNode *node)
		{
			if (isEmpty() == false)
			{
				if (head == tail)
				{
					return head;
				}
				else
				{
					ListNode *previous = head;
					inode = head->next;
					do
					{
						if (previous->getObject() == node->getObject())
						{
							if (inode != nullptr)
							{
								return inode::getObject();
							}
							else
							{
								return std::any();
							}
						}
						else
						{
							previous = inode;
							inode = inode->next;
						}
					} while (previous != head);
				}
			}
			return std::any();
		}
		
		List *List::subList(ListNode *from, ListNode *to)
		{
			List *out = new List();
			if (isEmpty() == false)
			{
				inode = head;
				bool itsInRange = false;
				for (int i = 0; i < size; i++)
				{
					if (inode::getObject() == from->getObject() || itsInRange == true)
					{
						if (inode::getObject() != to->getObject())
						{
							out->add(inode::getObject());
							itsInRange = true;
						}
						else
						{
							out->add(inode::getObject());
							break;
						}
					}
					inode = inode->next;
				}
			}
			return out;
		}
		
		List *List::sortList()
		{
			std::any previous;
			std::any actual;
			int cont = 0;
			do
			{
				inode = head;
				while (inode->next != head)
				{
					previous = inode::getObject();
					actual = inode->next.getObject();
					if ((previous.toString().compareTo(actual.toString()) > 0))
					{
						this->remove(previous);
						this->insertTail(previous);
					}
					inode = inode->next;
				}
				cont++;
			}while (cont < size); // Se utiliza esta expresión pora evitar almacenar los mismos elementos de la lista dos veces (Que haga un solo recorrido)
			return nullptr;
		}
		Iterator<ListNode*> *List::iterator()
		{
			inode = head;
			return new IteratorAnonymousInnerClass(this);
		}
		
		List::IteratorAnonymousInnerClass::IteratorAnonymousInnerClass(List *outerInstance) : outerInstance(outerInstance)
		{
		}
		
		bool List::IteratorAnonymousInnerClass::hasNext()
		{
			return inode->next != nullptr;
		}
		
		ListNode *List::IteratorAnonymousInnerClass::next()
		{
			if (inode != tail)
			{
				ListNode *tmp = inode;
				inode = inode->next;
				return tmp;
			}
			else
			{
				return nullptr;
			}
		}
		
		std::wstring List::toString()
		{
			inode = head;
			std::wstring string = L"";
			for (int i = 0; i < size; i++)
			{
				string += L"ListNode{" + L"object=" + inode::getObject() + L"," + L"next=";
				if (i + 1 == size)
				{
					string += inode->next.getObject() + L"}";
				}
				inode = inode->next;
			}
			return string;
		}
	};
