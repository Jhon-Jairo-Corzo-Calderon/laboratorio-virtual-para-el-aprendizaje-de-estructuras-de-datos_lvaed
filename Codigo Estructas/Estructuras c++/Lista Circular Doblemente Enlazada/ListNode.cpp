#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;

class ListNode{
	
	private:
		std::any object;  //Objeto a almacenar
	public:
		ListNode *previous;  //Puntero o dirección para ir al nodo anterior
		ListNode *next; //Puntero o dirección para ir al siguiente nodo
		
	ListNode::ListNode()
	{
		this->object = std::any(); //Solo creo un nodo pero no le paso nada
		this->previous = nullptr;
		this->next = nullptr;
	}

	ListNode::ListNode(std::any object)
	{
		this->object = object;  //Solo creo un nodo y le paso el objeto más no el nodo
		this->previous = nullptr;
		this->next = nullptr;
	}

	ListNode::ListNode(std::any object, ListNode *next, ListNode *previous)
	{
		this->object = object; //Solo creo un nodo y le paso ambos valores
		this->next = next;
		this->previous = previous;
	}

	std::any ListNode::getObject()
	{
		return object;
	}

	void ListNode::setObject(std::any object)
	{
		this->object = object;
	}

	bool ListNode::isEquals(std::any object)
	{
		if (this->getObject().toString().equals(object.toString()))
		{
			return true;
		}
		return false;
	}

	bool ListNode::isEquals(ListNode *node)
	{
		if (this->toString().equals(node->toString()))
		{
			return true;
		}
		return false;
	}
};


