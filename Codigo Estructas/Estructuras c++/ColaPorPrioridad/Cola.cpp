#pragma once
#include "ICola.h"
#include "List.h"
#include <string>
#include <stdexcept>
#include <any>

Using namespace std

	class Cola : public ICola{
		
		private:
			int minPrioridad = 0;
			int limiteMaxPrioridad = 0;
			List *lista = new List();

		public:
		
		Cola::Cola(int minPrioridad, int limiteMaxPrioridad) // Constructor
		{
			this->minPrioridad = minPrioridad;
			this->limiteMaxPrioridad = limiteMaxPrioridad;
			for (int i = limiteMaxPrioridad; i >= minPrioridad; i--)
			{
				List tempVar();
				lista->push_back(&tempVar);
			}
		}
	
		void Cola::clear()
		{
			lista->clear();
		}
	
		bool Cola::isEmpty()
		{
			return lista->empty();
		}
	
		std::any Cola::extract()
		{
			ListNode *inode = lista->head; // Se declara el Nodo cabeza (Primer elemento que ingreso)
			for (int i = limiteMaxPrioridad + 1; i >= minPrioridad; i--)
			{
				List *l = std::any_cast<List*>(inode->getObject());
				if (!l->empty())
				{
					std::any object = l->head->getObject();
					l->remove(l->head);
					return object; // Si la lista ,por ejemplo , de prioridad 5 tiene almenos un elemento , se extrae este
				}
				else
				{
					inode = inode->next;// Recorre el otro nodo el cual es una lista con los elementos de cierta prioridad
				}
			}
			return std::any();
		}
	
		bool Cola::insert(std::any object, int prioridad)
		{
			ListNode *inode = lista->head;
			for (int i = limiteMaxPrioridad; i >= minPrioridad; i--)
			{
				if (i == prioridad)
				{
					List *l = std::any_cast<List*>(inode->getObject());
					l->push_back(object);// Si el Nodo contiene la prioridad que es , se le ingresa a la sublista
				}
				else
				{
					inode = inode->next;
				}
			}
			return true;
		}
	
		int Cola::size()
		{
			int cont = 0;
			ListNode *inode = lista->head;
			for (int i = limiteMaxPrioridad; i >= minPrioridad; i--)
			{
				List *l = std::any_cast<List*>(inode->getObject());
				cont += l->getSize();
				inode = inode->next;
			}
			return cont; // Recorre los elementos de las sublistas y los cuenta
		}
	
		bool Cola::search(std::any object)
		{
			try
			{
				ListNode *inode = lista->head;
				for (int i = limiteMaxPrioridad; i >= minPrioridad; i--)
				{
					List *l = std::any_cast<List*>(inode->getObject());
					if (l->search(object) != nullptr) // Recorre las sublistas y si encuentra el elemento en alguna de esas , retorna true
					{
						return true;
					}
					else
					{
						inode = inode->next;
					}
				}
				return false;
			}
			catch (const std::runtime_error &e)
			{
				return false;
			}
	
		}
	
		void Cola::sort()
		{
			ListNode *inode = lista->head;
			for (int i = limiteMaxPrioridad; i >= minPrioridad; i--)
			{
				List *l = std::any_cast<List*>(inode->getObject());
				if (!l->empty())
				{
					l->sortList(); // Utiliza el método .sort de las listas para organizar el resultado
				}
				inode = inode->next;
			}
		}
	
		void Cola::reverse()
		{
			List *generalReverseList = new List(), *l, *reverseList;
			ListNode *inode = lista->head, *inodel;
			for (int i = limiteMaxPrioridad; i >= minPrioridad; i--)
			{
				l = std::any_cast<List*>(inode->getObject());
				reverseList = new List();
				inodel = new ListNode();
				inodel = l->tail;
				for (int j = l->getSize(); j > 0; j--)
				{
					reverseList->push_back(inodel->getObject());
					inodel = inodel->previous;
				}
				generalReverseList->push_back(reverseList);
				inode = inode->next;
			}
			//Invierte cada sublista 
			lista = generalReverseList;
			delete inodel;
		}
	
		std::wstring Cola::toString() // Recorre las sublistas y crea una concatenación con sus elementos
		{
			std::wstring output = L"";
			ListNode *inode = lista->head;
			for (int i = limiteMaxPrioridad; i >= minPrioridad; i--)
			{
				List *l = std::any_cast<List*>(inode->getObject());
				output += L"Prioridad " + std::to_wstring(i) + L" -> " + l->recInicioFin() + L" / ";
				inode = inode->next;
			}
			return output;
			//return lista.recInicioFin();
		}
	}	
