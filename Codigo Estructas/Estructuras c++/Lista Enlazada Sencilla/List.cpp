#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;

class List : public IList, public std::vector<ListNode*>
{
	private:
		ListNode *inode; //Nodo iterable
		int size = 0;
	public:
		ListNode *head; //Punteros para saber donde está el inicio y el fin
		ListNode *tail;
	
	List::List() //Constructor para borrar el contenido
	{
		clear();
	}
	
	List::List(std::any object)
	{
		push_back(object); // Constructor para agregar contenido
	}
	
	bool List::empty() // Un metodo boleano que va a ser muy utilizado a lo largo de la estructura para verificar si esta vacia o no
	{
		return head == nullptr; // Si la cabeza es null , retorna false , esto significa que no existe ningún elemento que tome esta posición y por consecuente , la estructura esta vacia
	}
	
	int List::getSize()
	{
		return size; // Retornamos la variable la cual va a ser modificada alrededor de los demas metodos
	}
	
	void List::clear() // Se elimina tanto la cola como la cabeza , eliminando así todo el contenido de la estructura , además de igualar el tamaño del contenido a "0" por naturalidad del caso
	{
		head = nullptr;
		tail = nullptr;
		size = 0;
	}
	
	std::any List::getHead() 
	{
		return head; // Retorna el contenido de la cabeza
	}
	
	std::any List::getTail()
	{
		return tail; // Retorna el contenido de la cola
	}
	
	// Si la cola y la cabeza son la misma , retornara el mismo contenido
	
	ListNode *List::search(std::any object)
	{
		List::const_iterator i = this->begin();
		ListNode *inode;
		while ((inode = i->next()) != nullptr) //Itera hasta encontrar al contenido de un nodo que sea igual al objeto ingresado con el fin de retornarlo
		{
			if (inode->getObject()->toString().equals(object.toString()))
			{
				return inode;
			}
		}
		return nullptr; // Y en caso que no lo encuentre , retornara Null
	}
	
	bool List::add(std::any object)
	{
		return insertTail(object); //El metodo .add funciona con la lógica de la inserción de la cola
	}
	
	bool List::insert(ListNode *node, std::any object) //Un método boolean el cual busca un nodo y en esa posición inserta el objeto del parámetro (El contenido del nodo anterior simplemente se desplaza)
	{
		try
		{
			if (node->next == nullptr)
			{
				push_back(object); // Primera dirección donde simplemente se agrega a lo ultimo
			}
			else
			{
				ListNode *newNode = new ListNode(object); // Segunda direción donde se tiene que mover el nodo para almacenar el valor del parametro y así no perder el anterior elemento que estaba en esa posición
				newNode->next = node->next;
				node->next = newNode;
				this->size++; // El tamaño aumenta debido a la integración del nuevo nodo
			}
			return true;
		}
		catch (const std::runtime_error &e)
		{
			return false; // Retorna falso si detecta algún error
		}
	}
	
	bool List::insert(std::any ob, std::any object) // Un método boolean el cual busca el objeto b dentro de la lista y lo reemplaza por el objecto object
	{
		try
		{
			if (ob.has_value())
			{
				ListNode *node = this->search(ob); // Mediante el metodo .search se asegura que el objeto esta dentro de la lista y retorna su valor para su posterior uso
				if (node != nullptr)
				{
					return insert(node, object); 
				}
				else
				{
					return false; // Retorna falso si el objeto ingresado es igual a null
				}
			}
			else
			{
				return false;
			}
		}
		catch (const std::runtime_error &e)
		{
			return false; // Retorna falso si detecta algún error
		}
	} // Como sustituyo al contentido de cierto nodo , la lista no se le suma ni resta ya que aunque cambio internamente , estructuralmente sigue igual
	
	bool List::insertHead(std::any object) // Se añade y asigna al objeto ingresado como la cabeza de la estructura
	{
		try
		{
			if (empty())
			{
				head = new ListNode(object);  // Se crea el nodo
				tail = head; // como esta vacia , al insertar por este metodo , la cabeza se convierte en el nodo que contiene al objeto del parametro y como solo existe un nodo , la cabeza es igual a la cola
			}
			else
			{
				head = new ListNode(object, head); // Se le asigna como cabeza de la estructura al nodo ingresado
			}
			this->size++; // Aumenta ya que siempre se va a ingresar un nuevo nodo 
			return true;
		}
		catch (const std::runtime_error &e)
		{
			return false;
		}
	}
	
	bool List::insertTail(std::any object) // Se añade y asigna al objeto ingresado como la cola de la estructura
	{
		try
		{
			if (empty())
			{
				head = new ListNode(object);  // Se crea el nodo
				tail = head; //como esta vacia , al insertar por este metodo , la cola se convierte en el nodo que contiene al objeto del parametro y como solo existe un nodo , la cabeza es igual a la cola
			}
			else
			{
				tail->next = new ListNode(object); 
				tail = tail->next;
			}
			this->size++; // Aumenta ya que siempre se va a ingresar un nuevo nodo 
			return true;
		}
		catch (const std::runtime_error &e)
		{
			return false;
		}
	}
	
	bool List::remove(ListNode *node) // convierte y elimina un nodo mediante la lógica del siguiente metodo
	{
		remove(node->getObject());
		return true;
	}
	
	bool List::remove(std::any object) // Remueve un elemento de la lista
	{
		if (!empty()) //Verifica que la lista tenga elementos
		{
			if (head == tail && object == head->getObject()) // En caso de que la cabeza sea igual que la cola y el objeto ingresado sea igual a la cabeza , se vacia la estructura
			{
				head = nullptr;
				tail = nullptr;
			}
			else if (object == head->getObject()) // En caso de que el objeto sea igual a la cabeza , la cabeza actual desaparece dejando este cargo al siguiente nodo (Se sobreescribe)
			{
				head = head->next;
			}
			else 
			{
				ListNode *previous = head, *temp = head->next;
				while (temp != nullptr && temp->getObject() != object)
				{
					previous = previous->next;
					temp = temp->next;
				}
				if (temp != nullptr)
				{
					previous->next = temp->next; // Se asigna el contenido del nodo anterior al nodo que acaba de perder su elemento
					if (temp == tail)
					{
						tail = previous; // En caso de que sea la cola , simplemente se toma el nodo anterior a la misma para asginarle este rol
					}
				}
			}
			this->size--; 
			return true; // Retorna verdadero para avisar que elimino efectivamente un elemento de la lista
		}
		return false; // Retorna falso para avisar que no se hizo ningún procedimiento a causa de que la lista estaba vacia
	}
	
	bool List::contains(std::any object) // Metodo booleano el cual se encarga de verficar unicamente si existe el objeto ingresado en el parametro en la lista
	{
		inode = head;
		while (inode != nullptr)
		{
			if (inode::getObject() == object) 
			{
				return true; // En caso de que el contenido de un nodo sea igual al parametro ingresado , se rompe el ciclo y retorna verdadero , así avisando que este si existe dentro de la lista
			}
			inode = inode->next; // El nodo se convierte en el nodo posterior para así recorrer la estructura y alcanzar un momento el null para romper el ciclo
		}
		return false; // Si nunca detecta un parametro igual y rompe el ciclo gracias a que el nodo se "convirtio" en null , se retorna false así avisando que el objeto no existe dentro de la lista
	}
	
	std::vector<std::any> List::toArray() // Metodo para convertir la lista en un array
	{
		std::vector<std::any> object(size); // Crea un Array con el tamaño de la lista , gracias al atributo "size"
		return toArray(object); // Utiliza el siguiente metodo (To.array con parametros) para así almacenar la lista en el Array anteriormente creado
	}
	
	std::vector<std::any> List::toArray(std::vector<std::any> &object) // Metodo para convertir la lista en un array
	{
		try
		{
			inode = head;
			for (int i = 0; i < size; i++) 
			{
				object[i] = inode::getObject(); // Almacena y convierte en objeto el nodo cabeza mediante el anterior ciclo
				inode = inode->next; // Se recorre los nodos así cambiando el contenido según la lista en cada iteración del nodo
			}
			return object; 
		}
		catch (const std::runtime_error &e)
		{
			return std::vector<std::any>();
		}
	}
	
	std::any List::getBeforeTo()
	{
		return getBeforeTo(tail)->getObject(); // invocan al método getBeforeTo que se encuentra sin parámetro se asume que me están pidiendo el objeto que se encuentra antes de la cola
	}
	
	ListNode *List::getBeforeTo(ListNode *node)
	{
		if (!isEmpty())
		{
			if (head == tail)
			{
				return nullptr; // Retorna null ya que significa que la lista solo posee un elemento
			}
			else if (node == nullptr)
			{
				return tail; // Retorna la cola debido que si el nodo es null , el anterior será la cola
			}
			else
			{
				ListNode *previous = nullptr;
				inode = head;
				while (inode != nullptr)
				{
					if (inode::getObject() == node->getObject())
					{
						return previous;
					}
					else
					{
						previous = inode;
						inode = inode->next;
					}
				} // Identifica el nodo y retorna el anterior 
			}
		}
		return nullptr;
	}
	
	std::any List::getNextTo()
	{
		return getNextTo(head); //invocan al método getNextTo que se encuentra sin parámetro se asume que me están pidiendo el objeto que se encuentra después de la cabeza, claro está en caso de haya una cabeza o haya algo después de la cabeza
	}
	
	std::any List::getNextTo(ListNode *node)
	{
		if (!isEmpty())
		{
			if (head == tail)
			{
				return nullptr; // Retorna null ya que significa que la lista solo posee un elemento
			}
			else
			{
				ListNode *previous = head;
				inode = head->next;
				while (previous != nullptr)
				{
					if (previous->getObject() == node->getObject())
					{
						return inode::getObject();
					}
					else
					{
						previous = inode;
						inode = inode->next;
					}
				}
				// Identifica el nodo y retorna el posterior  
			}
		}
		return nullptr;
	}
	
	List *List::subList(ListNode *from, ListNode *to) // Metodo para crear una Sublista mediante dos nodos (El inicial y el final), digitado por el parametro
	{
		List *out = new List(); // Se crea la lista que posteriormente se va a retornar 
		if (!isEmpty())
		{
			inode = head;
			bool itsInRange = false;
			for (int i = 0; i < size; i++)
			{
				if (inode::getObject() == from->getObject() || itsInRange)
				{
					if (inode::getObject() != to->getObject()) // Se empieza a recorrer desde el nodo ingresado , almacenando todos los nodos hasta que se tope con el segundo nodo del parametro que indica la cola
					{
						out->add(inode::getObject());
						itsInRange = true;
					}
					else
					{
						out->add(inode::getObject()); 
						break; // Cuando añade la cola , se rompe el ciclo
					}
					inode = inode->next;
				}
			}
		}
		return out;
	}
	
	List *List::sortList() // Metodo para organizar la lista
	{
		try
		{
			std::any previous;
			std::any actual;
			int cont = 0;
			do
			{
				inode = head;
				while (inode->next != nullptr)
				{
					previous = inode::getObject();
					actual = inode->next.getObject();
					if ((previous.toString().compareTo(actual.toString()) > 0)) // Mediante el metodo .compare y la transformaciones de estos a Strings , se organiza la lista iniciando con la cabeza como el Nodo principal y siguiendo con los Nodos de manera descendente hasta llegar a la cola siendo este el ultimo según la organización del metodo
					{
						this->remove(previous);
						this->insertTail(previous);
						inode = inode->next;
					}
					else
					{
						inode = inode->next; //Si el nodo por .compare resulta ser "menor" al nodo actual , se le asigne el siguiente
					}
				}
				cont++; // El count sirve para evitar que exceda la cantidad de elementos de la lista original
			}while (cont < size);
			return nullptr;
		}
		catch (const std::runtime_error &e)
		{
			return nullptr;
		}
	}

		
	Iterator<ListNode*> *List::iterator()
	{
		inode = head;
		return new IteratorAnonymousInnerClass(this);
	}
	
	List::IteratorAnonymousInnerClass::IteratorAnonymousInnerClass(List *outerInstance) : outerInstance(outerInstance)
	{
	}
	
	bool List::IteratorAnonymousInnerClass::hasNext()
	{
		return inode->next != nullptr;
	}
	
	ListNode *List::IteratorAnonymousInnerClass::next()
	{
		if (inode != nullptr)
		{
			ListNode *tmp = inode;
			inode = inode->next;
			return tmp;
		}
		else
		{
			return nullptr;
		}
	}
	
	void List::rec(ListNode *node) // Metodo para recorrer la lista 
	{
		try
		{
			if (node->next != nullptr)
			{
				rec(node->next);
				// <- ;) ->
			}
			out::println(node->toString());
		}
		catch (const std::runtime_error &e)
		{
			out::println(L"¡Ha ocurrido un error!");
		}
	}	
}
