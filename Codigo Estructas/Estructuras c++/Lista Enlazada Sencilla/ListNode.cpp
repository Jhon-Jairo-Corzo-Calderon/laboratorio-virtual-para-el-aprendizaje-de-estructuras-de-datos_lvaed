#include "ListNode.h"
#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;

class ListNode{
	
	private:
		std::any object; //Objeto a almacenar
	public:
		ListNode *next; //Puntero o direccin para ir al siguiente nodo
	
	ListNode::ListNode()
	{
		this->object = std::any(); //Solo creo un nodo pero no le paso nada
		this->next = nullptr;
	}

	ListNode::ListNode(std::any object)
	{
		this->object = object; //Solo creo un nodo y le paso el objeto ms no el nodo
		this->next = nullptr;
	}

	ListNode::ListNode(std::any object, ListNode *next)
	{
		this->object = object; 
		this->next = next; // Crea el espacio para el siguiente nodo
	}

	std::any ListNode::getObject()
	{
		return object; // Retorna el objeto del nodo
	}

	void ListNode::setObject(std::any object)
	{
		this->object = object;
	}

	bool ListNode::isEquals(std::any object)
	{
		if (this->getObject().toString().equals(object.toString())) // Compara en nodo con un objeto a ingresar
		{
			return true;
		}
		return false;
	}

	bool ListNode::isEquals(ListNode *node)
	{
		if (this->toString() == node->toString()) // Compara en nodo con un nodo a ingresar
		{
			return true;
		}
		return false;
	}

	std::wstring ListNode::toString() // Genera una expresin "String" donde convierte se almacenan todos los nodos de la estructura
	{
		return L"ListNode{" + L"object=" + object + L", next=" + next + L'}';
	}
}
