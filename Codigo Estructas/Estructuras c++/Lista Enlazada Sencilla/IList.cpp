#pragma once
#include "ListNode.h"
#include "List.h"
#include <vector>
#include <any>
#include <iostream>
using namespace std;

	class IList
	{
		public:
		virtual bool isEmpty() = 0;

		virtual int getSize() = 0;

		virtual void clear() = 0;

		virtual std::any getHead() = 0;

		virtual std::any getTail() = 0;

		virtual ListNode *search(std::any object) = 0;

		virtual bool add(std::any object) = 0;

		virtual bool insert(ListNode *node, std::any object) = 0;

		virtual bool insert(std::any ob, std::any object) = 0;

		virtual bool insertHead(std::any object) = 0;

		virtual bool insertTail(std::any object) = 0;

		virtual bool remove(ListNode *node) = 0;

		virtual bool remove(std::any object) = 0;

		virtual bool contains(std::any object) = 0;

		virtual Iterator<ListNode*> *iterator() = 0;

		virtual std::vector<std::any> toArray() = 0;

		virtual std::vector<std::any> toArray(std::vector<std::any> &object) = 0;

		virtual std::any getBeforeTo() = 0;

		virtual ListNode *getBeforeTo(ListNode *node) = 0;

		virtual std::any getNextTo() = 0;

		virtual std::any getNextTo(ListNode *node) = 0;

		virtual List *subList(ListNode *from, ListNode *to) = 0;

		virtual List *sortList() = 0;
	};
