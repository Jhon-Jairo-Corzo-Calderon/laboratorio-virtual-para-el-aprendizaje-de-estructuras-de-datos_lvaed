#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;

class Pila : public IPila{
	
	    /*

    Nota : la pila basada en listas , es especifico se constituye por las Listas Doblemente Enlazadas (Explicadas anteriormente) , así volviendo dinamica esta estructura

    */

	private:
	List *lista;
	
	Pila::Pila() // Constructor para crear una nueva pila
	{
		lista = new List();
	}

	Pila::Pila(std::any o) // Constructor para crear una nueva pila con el parametro o
	{
		lista = new List(o);
	}

	void Pila::clear()
	{
		if (isEmpty() != true)
		{
			lista->clear(); // Se utiliza el método de las listas .clear para vaciar el contenido de la estructura
		}
	}

	bool Pila::isEmpty()
	{
		if (lista->empty()) // Mediante el método isEmpty de las listas , se verifica que la pila tenga o no contenido
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	std::any Pila::peek() // El método retorna el "Top" de las pilas que básicamente en este caso es la cola de la lista ya que fue el ultimo elemento en ingresar a la estructura
	{
		if (lista->empty() != true) // Se verifica que la pila tenga almenos un elemento
		{
			return lista->tail->getObject(); // Retorna la cola de la lista
		}
		else
		{
			return std::any(); // Retorna null ya que no tiene top al estar vacia
		}
	}

	std::any Pila::pop() //El método retorna el "Top" y lo elimina de la estructura
	{
		if (lista->empty() != true)
		{
			std::any object = this->peek(); // Se le asigna a la variable object el top de la pila
			lista->remove(lista->tail); // elimina el "top" de la pila (La cola de la lista)
			return object;// Retorna la variable que contenia el antiguo top de la pila
		}
		else
		{
			return std::any();
		}
	}

	bool Pila::push(std::any object) // El método de agregación de elementos a la pila mediante el .add de las listas
	{
		lista->insertTail(object);
		return true;
	}

	int Pila::size() // Retorna el tamaño de la pila
	{
		return lista->getSize();
	}

	bool Pila::search(std::any object)
	{
		if (lista->search(object)->getObject() == object) //Busca el objeto del parametro y si esta dentro de la pila (Mediante el .search de las listas) , retorna true
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void Pila::sort()
	{
		//Se modifica el metodo sort de la lista
		List *sort = lista;
		sort->sortList();
		lista->clear();
		ListNode *node = sort->head;
		for (int i = 0;i < sort->getSize();i++)
		{
			lista->push_back(node->getObject());
			node = node->next;
		}
	}

	void Pila::reverse() // Crea una lista , almacena el contenido desde el top (Hace que los ultimos que ingresaron sean los ultimos que salgan y viceversa) y posteriormente lo vuelve a almacenar en la lista original que es la pila
	{
		List *reverse = new List();
		ListNode *node = lista->head;
		for (int i = 0; i < lista->getSize(); i++)
		{
			reverse->insertHead(pop());
			node = node->next;
		}
		lista = reverse;
	}
};
