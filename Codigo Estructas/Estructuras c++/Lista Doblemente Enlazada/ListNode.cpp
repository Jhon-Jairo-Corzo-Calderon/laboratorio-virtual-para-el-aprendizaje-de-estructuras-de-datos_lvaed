#include "ListNode.h"
#include <iostream>
#pragma once
#include <vector>
#include <any>

using namespace std;

class ListNode{
	
    /*La mayor diferencia referente a las listas sencillas enlazadas , es que su navegación
	o recorrido solo se puede hacer de un sentido , de izquierda a derecha O derecha a izquierda , 
	así solo conteniendo un final "Null" , por lo cual , al ser doblemente enlazada , se tiene que 
	crear con la lógica tanto que antes de la cabeza como despus de la cola , hay un null as pudiendo realizar los metodos */
	
	private:
		std::any object; //Objeto a almacenar
	public:
		ListNode *previous; //Puntero o direccin para ir al nodo anterior
		ListNode *next; //Puntero o direccin para ir al siguiente nodo
	
	ListNode::ListNode()
	{
		this->object = nullptr; //Solo creo un nodo pero no le paso nada
		this->previous = nullptr;
		this->next = nullptr;
	}

	ListNode::ListNode(std::any object)
	{
		this->object = object; //Solo creo un nodo y le paso el objeto ms no el nodo
		this->previous = nullptr;
		this->next = nullptr;
	}

	ListNode::ListNode(std::any object, ListNode *next, ListNode *previous)
	{
		this->object = object;  //Solo creo un nodo y le paso ambos valores
		this->next = next;
		this->previous = previous;
	}

	std::any ListNode::getObject()
	{
		return object;
	}

	void ListNode::setObject(std::any object)
	{
		this->object = object;
	}

	bool ListNode::isEquals(std::any object)
	{
		if (this->getObject().toString().equals(object.toString()))
		{
			return true;
		}
		return false;
	}

	bool ListNode::isEquals(ListNode *node)
	{
		if (this->toString() == node->toString())
		{
			return true;
		}
		return false;
	}

	std::wstring ListNode::toString()
	{
		return L"ListNode{" + L"object=" + object + L"," + L"next=" + next + L'}';
	}

	std::wstring ListNode::toStringReverse()
	{
		return L"ListNode{" + L"object=" + object + L"," + L"previous=";
	}
};

