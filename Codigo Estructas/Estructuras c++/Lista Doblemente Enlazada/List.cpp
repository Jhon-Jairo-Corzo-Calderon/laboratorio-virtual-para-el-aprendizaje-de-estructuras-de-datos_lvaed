#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;
	
	class List : public IList, public std::vector<ListNode*>{
		
		/*
		Nota : La propia naturaleza de las listas doblemente enlazadas permite viajar por esta estructura
		tanto de izquierda a derecha como de derecha a izquierda as logrando una mayor versatilidad al 
		momento de usarlas , sin embargo , no deja de ser una lista con los mismos mtodos que la simplemente
		enlazada , claramente tomando en cuenta esta caracteristica.
		
		Para ver el funcionamiento de la lista sencilla , por favor dirigirse a la seccin correspondiente
		*/
		
		private:
		ListNode *inode; //Nodo iterable
		int size = 0;

		public:
		ListNode *head; //Punteros para saber donde est el inicio y el fin
		ListNode *tail;
		
		List::List() 
		{
			clear();
		}
	
		List::List(std::any object)
		{
			push_back(object); 
		}
	
		bool List::empty() 
		{
			return head == nullptr; 
		}
	
		int List::getSize() 
		{
			return size;
		}
	
		void List::clear() 
		{
			head = nullptr;
			tail = nullptr;
			size = 0;
		}
	
		std::any List::getHead()
		{
			return head; 
		}
	
		std::any List::getTail()
		{
			return tail; 
		}
	
		ListNode *List::search(std::any object)
		{
			List::const_iterator i = this->begin();
			ListNode *inode;
			while ((inode = i->next()) != nullptr) 
			{
				if (inode->getObject().toString().equals(object.toString()))
				{
					return inode;
				}
			} 
			return nullptr; 
		}
	
		bool List::add(std::any object)
		{
			return insertTail(object); 
		}
	
		bool List::insert(ListNode *node, std::any object) 
		{ 
			try
			{
				if (node->next == nullptr)
				{
					push_back(object); 
				}
				else
				{
					ListNode *newNode = new ListNode(object);
					newNode->previous = node;
					(node->next)->previous = newNode; 
					newNode->next = node->next;
					node->next = newNode;
					//Se recorre la lista tanto por derecha como por izquierda para encontrar al nodo y as reemplazarlo por el nodo (Junto a su contenido) , adems de mover el nodo anterior para no perderlo
				}
				this->size++; 
				return true;
			}
			catch (const std::runtime_error &e)
			{
				return false; 
			}
		}
	
		bool List::insert(std::any ob, std::any object)
		{
			try
			{
				if (ob.has_value())
				{
					ListNode *node = this->search(ob); 
					if (node != nullptr)
					{
						return insert(node, object);
					}
					else
					{
						return false; 
					}
				}
				else
				{
					return false;
				}
			}
			catch (const std::runtime_error &e)
			{
				return false;
			}
		} 
	
		bool List::insertHead(std::any object) 
		{
			try
			{
				if (empty())
				{
					head = new ListNode(object); 
					tail = head; 
				}
				else
				{
					head->previous = new ListNode(object, head, nullptr); // Aprovechando que se puede recorrer de derecha a izquierda , se aade el nuevo Nodo a la lista en la posicin anterior de la actual cabeza
					head = head->previous; // Se le asigna como cabeza al nuevo Nodo 
				}
				this->size++; 
				return true;
			}
			catch (const std::runtime_error &e)
			{
				return false;
			}
		}
	
		bool List::insertTail(std::any object) 
		{
			try
			{
				if (empty())
				{
					head = new ListNode(object); 
					tail = head;
				}
				else
				{
					tail->next = new ListNode(object,nullptr,tail); 
					tail = tail->next; 
				}
				this->size++;  
				return true;
			}
			catch (const std::runtime_error &e)
			{
				return false;
			}
		}
	
		bool List::remove(ListNode *node) 
		{
			remove(node->getObject());
			return true;
		}
	
		bool List::remove(std::any object) 
		{
			if (empty() == false) 
			{
				if (head == tail && object == head->getObject()) 
				{
					head = nullptr;
					tail = nullptr;
				}
				else if (head->isEquals(object)) 
				{ 
					head = head->next;
				}
				else
				{
					ListNode *previous = head, *temp = head->next;
					while (temp != nullptr && temp->getObject() != object)
					{
						previous = previous->next;
						temp = temp->next;
					}
					if (temp != nullptr)
					{
						previous->next = temp->next;
						if (temp == tail)
						{
							tail = previous; 
						}
					}
				}
				size--; 
			}
			return true;
		}	
		
		bool List::contains(std::any object) 
		{
			inode = head;
			while (inode != nullptr)
			{
				if (inode::getObject() == object)
				{
					return true;
				}
				inode = inode->next;
			}
			return false;
		}
	
		std::vector<std::any> List::toArray()
		{
			std::vector<std::any> array(size);
			inode = head;
			for (int i = 0; i < size; i++)
			{
				array[i] = inode::getObject();
				inode = inode->next;
			}
			return array;
		}
	
		std::vector<std::any> List::toArray(std::vector<std::any> &object)
		{
			inode = head;
			for (int i = 0; i < size; i++)
			{
				object[i] = inode::getObject();
				inode = inode->next;
			}
			return object;
		}
	
		std::any List::getBeforeTo()
		{
			return getBeforeTo(tail)->getObject();
		}
	
		ListNode *List::getBeforeTo(ListNode *node)
		{
			if (empty() == false)
			{
				if (head == tail)
				{
					return nullptr;
				}
				else if (node == nullptr)
				{
					return tail;
				}
				else
				{
					ListNode *previous = nullptr;
					inode = head;
					while (inode != nullptr)
					{
						if (inode::getObject() == node->getObject())
						{
							return previous; // En caso de que algn nodo coincida con el nodo ingresado en el parametro , se retornara el anterior gracias al recorrido inverso que puede hacer esta estructura
						}
						else
						{
							previous = inode;
							inode = inode->next;
						}
					}
				}
			}
			return nullptr;
		}
	
		std::any List::getNextTo()
		{
			return getNextTo(head); } //Si invocan al método getNextTo que se encuentra sin parámetro se asume que me están pidiendo el objeto que se encuentra después de la cabeza, claro está en caso de haya una cabeza o haya algo después de la cabeza
	
		std::any List::getNextTo(ListNode *node)
		{
			if (isEmpty() == false)
			{
				if (head == tail)
				{
					return std::any();
				}
				else
				{
					ListNode *previous = head;
					inode = head->next;
					while (previous != nullptr)
					{
						if (previous->getObject() == node->getObject())
						{
							if (inode != nullptr)
							{
								return inode::getObject();
							}
							else
							{
								return std::any();
							}
						}
						else
						{
							previous = inode;
							inode = inode->next;
						}
					}
				}
			}
			return nullptr;
		}
	
		List *List::subList(ListNode *from, ListNode *to)
		{
			List *out = new List();
			if (empty() == false)
			{
				inode = head;
				bool itsInRange = false;
				for (int i = 0; i < size; i++)
				{
					if (inode::getObject() == from->getObject() || itsInRange == true)
					{
						if (inode::getObject() != to->getObject())
						{
							out->add(inode::getObject());
							itsInRange = true;
						}
						else
						{
							out->add(inode::getObject());
							break;
						}
					}
					inode = inode->next;
				}
			}
			return out;
		}
	
		List *List::sortList()
		{
			std::any previous;
			std::any actual;
			int cont = 0;
			do
			{
				inode = head;
				while (inode->next != nullptr)
				{
					previous = inode::getObject();
					actual = inode->next.getObject();
					if ((previous.toString().compareTo(actual.toString()) > 0))
					{
						this->remove(previous);
						this->insertTail(previous);
					}
					inode = inode->next;
				}
				cont++;
			}while (cont < size);
			return nullptr;
		}
		Iterator<ListNode*> *List::iterator()
		{
		inode = head;
		return new IteratorAnonymousInnerClass(this);
		}

		List::IteratorAnonymousInnerClass::IteratorAnonymousInnerClass(List *outerInstance) : outerInstance(outerInstance)
		{
		}
	
		bool List::IteratorAnonymousInnerClass::hasNext()
		{
			return inode->next != nullptr;
		}
	
		ListNode *List::IteratorAnonymousInnerClass::next()
		{
			if (inode != nullptr)
			{
				ListNode *tmp = inode;
				inode = inode->next;
				return tmp;
			}
			else
			{
				return nullptr;
			}
		}
	
		void List::recInicioFin() //Recorre la lista de izquierda a derecha 
		{
			try
			{
				ListNode *node = new ListNode();
				node = head;
				out::println(node->toString());
	
				delete node;
			}
			catch (const std::runtime_error &e)
			{
			}
		}
	
		void List::recFinInicio() // Recorre la lista de derecha a izquierda
		{
			try
			{
				inode = tail;
				std::wstring output = L"";
				while (inode != nullptr)
				{
					if (inode::previous != nullptr)
					{
						output += inode::toStringReverse();
					}
					else
					{
						output += inode::toStringReverse() + L"null}";
					}
					inode = inode::previous;
				}
				out::println(output);
			}
			catch (const std::runtime_error &e)
			{
			}
		}
	};
