#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;

class Cola : public ICola{

	    /*

    Nota : Al estar basada en arrays , convierte la cola en una estructura estática  

    */

	private:
	std::vector<std::any> array;
	
	Cola::Cola(int size)
	{
		if (size > 0)
		{
			array = std::vector<std::any>(size);
		}
		else
		{
			array = std::vector<std::any>(1);
		}
	}

	void Cola::clear() // Recorre la cola , asignando a cada posición su contenido null
	{
		for (int i = 0; i < array.size(); i++)
		{
			array[i] = std::any();
		}
	}

	bool Cola::isEmpty()
	{
		bool estaVacia = false;
		for (int i = 0; i < size(); i++)
		{
			if (!array[i].has_value()) // Si el primer el primer elemento es null , significa que la cola esta vacia
			{
				estaVacia = true;
			}
			else
			{
				estaVacia = false;
				break; // Rompe el ciclo cuando se de esta condición
			}
		}
		return estaVacia;
	}

	std::any Cola::extract()
	{
		std::any object; // Se declara el Objeto donde se va almacenar el primer elemento de la cola
		for (int i = 0; i < size(); i++)
		{
			if (array[i].has_value())
			{
				object = array[i]; // Asigna el objeto el valor del primer elemento de la cola
				array[i] = std::any(); // Elimina el elemento
				return object;
			}
		}
		return object; // Se hace este return debido a que si la cola esta vacia , retorna el valor original asignado (Null)
	}

	bool Cola::insert(std::any object)
	{
		for (int i = 0; i < array.size(); i++)
		{
			if (!array[i].has_value())// Cuando la posición de un array de null y en el caso de las colas , signfica que será puesto al final de la misma
			{
				array[i] = object; // Se le asigna el objeto a ese espacio
				return true;
			}
		}
		return false;
	}

	int Cola::size() // Retorna el tamaño de la estructura 
	{
		return array.size();
	}

	bool Cola::search(std::any object)
	{
		std::vector<std::any> arraycopia(size());
		for (int i = 0; i < size(); i++)
		{
			if (array[i] == object) // Recorre el array para verificar si el objeto ingresaod en el parametro esta dentro de la estructura
			{
				return true;
			}
		}
		return false;
	}

	void Cola::sort() // Organiza la cola
	{
		List *s = new List();
		for (auto o : array)
		{
			s->push_back(o);
		}
		s->sortList();
		array = s->toArray();

		delete s;
	}

	void Cola::reverse()
	{
		std::vector<std::any> reverseArray(size());// Crea un array donde se van almacenar los elementos "Inversos"
		for (int i = 0; i < size(); i++)
		{
			reverseArray[i] = array[array.size() - (i + 1)];// Recorre el array original inversamente
		}
		array = reverseArray;//Asigna los valores del Array "Inverso" al original
	}

	std::wstring Cola::toString()// Crea una concatenación de Strings con los elementos del array (Cola)
	{
		return L"ArrayTail{" + L"size=" + std::to_wstring(size()) + L", array=" + Arrays->toString(array) + L'}'; //Aparte del tamaño
	}
};


