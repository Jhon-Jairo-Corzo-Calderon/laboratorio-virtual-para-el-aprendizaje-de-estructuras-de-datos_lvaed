#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;

	class ICola //Interfaz de la cola por Array
	{

	public:
		virtual void clear() = 0;

		virtual bool isEmpty() = 0;

		virtual std::any extract() = 0;

		virtual bool insert(std::any object) = 0;

		virtual int size() = 0;

		virtual bool search(std::any object) = 0;

		virtual void sort() = 0;

		virtual void reverse() = 0;

		virtual std::wstring toString() = 0;
	};

