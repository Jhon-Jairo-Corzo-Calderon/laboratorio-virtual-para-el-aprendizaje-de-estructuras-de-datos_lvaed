#include "BinSearchTree.h"
#pragma once
#include "Node.h"
#include <string>
#include <any>

using namespace std

class BinSearchTree{
	
	 //ATRIBUTOS
	private:
		std::wstring preorder = L"", inorder = L"", posorder = L"";
		bool search = false;
	public:
		Node *root;
	
	//MÉTODOS Y CONSTRUCTORES

	BinSearchTree::BinSearchTree()
	{
		this->root = nullptr;
	}

	BinSearchTree::BinSearchTree(std::any o, int id)
	{
		this->root = new Node(nullptr, o, nullptr, id);
	}
	//Método IsEmpty
	bool BinSearchTree::IsEmpty()
	{
		if (root == nullptr)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//Método Insert
	bool BinSearchTree::Insert(std::any o, int id)
	{
		if (!IsEmpty())
		{
			Node *inode = root;
			while (inode != nullptr)
			{
				if (inode->GetID() <= id)
				{
					if (inode->right != nullptr)
					{
						inode = inode->right;
					}
					else
					{
						inode->right = new Node(nullptr,o,nullptr,id);
						break;
					}
				}
				else
				{
					if (inode->left != nullptr)
					{
						inode = inode->left;
					}
					else
					{
						inode->left = new Node(nullptr,o,nullptr,id);
						break;
					}
				}
			}
		}
		else
		{
			root = new Node(nullptr,o,nullptr,id);
		}
		return true;
	}
	//Método search
	void BinSearchTree::Search(Node *n, std::any o)
	{
		if (!IsEmpty())
		{
			if (n->GetObject().toString().compareTo(o.toString()) == 0)
			{
				search = true;
			}
			if (n->right != nullptr)
			{
				if (n->right->GetObject().toString().compareTo(o.toString()) == 0)
				{
					search = true;
				}
				else
				{
					Search(n->right, o);
				}
			}
			if (n->left != nullptr)
			{
				if (n->left->GetObject().toString().compareTo(o.toString()) == 0)
				{
					search = true;
				}
				else
				{
					Search(n->left, o);
				}
			}
		}
	}
	//Método remove
	bool BinSearchTree::SearchObject(std::any o)
	{
		Search(root,o);
		if (search == true)
		{
			search = false; //Se vuelve a dejar la variable booleana en false para en un futuro poder volverla a usar
			return true;
		}
		else
		{
			return false;
		}
	}
	
	bool BinSearchTree::Remove(int id)
	{
		if (!IsEmpty())
		{
			Node *inode = root;
			Node *preInode = root;
			while (inode->GetID() != id)
			{
				preInode = inode;
				if (inode->GetID() > id)
				{
					inode = inode->left;
				}
				else
				{
					inode = inode->right;
				}
				if (inode == nullptr)
				{
					return false;
				}
			}
			if (inode->right == nullptr & inode->left == nullptr)
			{
				if (inode == root)
				{
					root = nullptr;
				}
				else if (preInode->left == inode)
				{
					preInode->left = nullptr;
				}
				else
				{
					preInode->right = nullptr;
				}
			}
			else if (inode->right == nullptr)
			{
				if (inode == root)
				{
					root = root::left;
				}
				else if (preInode->left == inode)
				{
					preInode->left = inode->left;
				}
				else
				{
					preInode->right = inode->left;
				}
			}
			else if (inode->left == nullptr)
			{
				if (inode == root)
				{
					root = root::right;
				}
				else if (preInode->left == inode)
				{
					preInode->left = inode->right;
				}
				else
				{
					preInode->right = inode->right;
				}
			}
			else
			{
				Node *rePre = inode, *re = inode, *aux = inode->right;
				while (aux != nullptr)
				{
					rePre = re;
					re = aux;
					aux = aux->left;
				}
				if (re != inode->right)
				{
					rePre->left = re->right;
					re->right = inode->right;
				}

				if (inode == root)
				{
					root = re;
				}
				else if (preInode->left == inode)
				{
					preInode->left = re;
				}
				else
				{
					preInode->right = re;
				}
				re->left = inode->left;
			}
		}
		else
		{
			return false;
		}
		return true;
	}
	//Métodos necesarios para realizar el preorder
	void BinSearchTree::PreOrder(Node *n)
	{
		if (!IsEmpty())
		{
			preorder += static_cast<std::wstring>(n->GetObject());
			if (n->left != nullptr)
			{
				PreOrder(n->left);
			}
			if (n->right != nullptr)
			{
				PreOrder(n->right);
			}
		}
	}

	std::wstring BinSearchTree::GetPreOrder()
	{
		PreOrder(root);
		return preorder;
	}

	//Métodos necesarios para realizar el inorder
	void BinSearchTree::InOrder(Node *n)
	{
		if (!IsEmpty())
		{
			if (n->left != nullptr)
			{
				InOrder(n->left);
			}
			inorder += static_cast<std::wstring>(n->GetObject());
			if (n->right != nullptr)
			{
				InOrder(n->right);
			}
		}
	}

	std::wstring BinSearchTree::GetInOrder()
	{
		InOrder(root);
		return inorder;
	}
	//Métodos necesarios para realizar el posorder
	void BinSearchTree::PosOrder(Node *n)
	{
		if (!IsEmpty())
		{
			if (n->left != nullptr)
			{
				PosOrder(n->left);
			}
			if (n->right != nullptr)
			{
				PosOrder(n->right);
			}
			posorder += static_cast<std::wstring>(n->GetObject());
		}
	}

	std::wstring BinSearchTree::GetPosOrder()
	{
		PosOrder(root);
		return posorder;
	}
};
