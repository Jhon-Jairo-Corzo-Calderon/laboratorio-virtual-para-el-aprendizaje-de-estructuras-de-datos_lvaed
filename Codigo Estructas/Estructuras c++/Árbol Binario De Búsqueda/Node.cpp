#include "BinSearchTree.h"
#pragma once
#include "Node.h"
#include <string>
#include <any>

using namespace std

class Node{
	
	//ATRIBUTOS
	private:
		std::any object;
	public:
		Node *right;
		Node *left;
	private:
		int id = 0;
	
	//MÉTODOS Y CONSTRUCTORES
	//Constructores
	Node::Node(std::any o, int id)
	{
		this->object = o;
		this->right = nullptr;
		this->left = nullptr;
		this->id = id;
	}

	Node::Node(Node *left, std::any object, Node *right, int id)
	{
		this->object = object;
		this->left = left;
		this->right = right;
		this->id = id;
	}

	int Node::GetID()
	{
		return id;
	}

	std::any Node::GetObject()
	{
		return object;
	}

	int Node::Grado()
	{
		if (right == nullptr & left == nullptr)
		{
			return 0;
		}
		else if ((right == nullptr & left != nullptr) | (right != nullptr & left == nullptr))
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}
	
	//Método toString
	std::wstring Node::toString()
	{
		return L"Node{ left=" + left + L", right=" + right + L", object=" + object + L", id=" + std::to_wstring(id) + StringHelper::toString(L'}');
	}
};

