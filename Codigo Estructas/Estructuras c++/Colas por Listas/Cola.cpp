#include <iostream>
#pragma once
#include <vector>
#include <any>
using namespace std;

    /*

    Nota : Las colas por listas estan basadas especificamente en las Listas Doblemente Enlazadas , por lo cual hace que este tipo de estructura se dinámica

    */


class Cola : public ICola{
	
	private:
	List *lista = new List();
	
	void Cola::clear()
	{
		lista->clear(); // Elimina los elementos de la cola mediante el .clear de la lista
	}

	bool Cola::isEmpty()
	{
		return lista->empty(); // Utiliza el método .clear de las listas para eliminar todo tipo de contenido de la estructura
	}

	std::any Cola::extract()
	{
		std::any object = lista->head->getObject(); // Almacena la cabeza de la lista en la variable object
		lista->remove(lista->head); // Remueve la cabeza de la lista (El primer elemento que ingreso)
		return object; // Retorna el elemento object
	}

	bool Cola::insert(std::any object)
	{
		lista->push_back(object); // Agrega el elemento ingresado en el parametro al final de la cola
		return true;
	}

	int Cola::size() // Retorna el tamaño de la estructura
	{
		return lista->getSize();
	}

	bool Cola::search(std::any object)
	{
		try
		{
			if (lista->search(object)->getObject().toString().equals(object.toString())) //Mediante el método .search de las listas , recorre la cola para verificar que el elemento este en la estructura
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch (const std::runtime_error &e)
		{
			return false;
		}

	}

	void Cola::sort() // Organiza la cola 
	{
		lista->sortList();
	}

	void Cola::reverse()
	{
		List *reverseList = new List(); // Crea la lista "Inversa"
		ListNode *inode = new ListNode(); // Crea el nodo que va a recorrer la estructura
		inode = lista->tail; // Declara el nodo con el último valor ingresado a la estructura (La cola)
		for (int i = lista->getSize(); i > 0; i--)
		{
			reverseList->push_back(inode->getObject());
			inode = inode->previous; // Recorre la estructura desde la cola aprovechando las cualidades de la lista doblemente enlazada para ir almacenando los elementos en la lista "Inversa"
		}
		lista = reverseList; // Asigan los valores de la lista "Inversa"  a la lista original
	}

	std::wstring Cola::toString()
	{
		return lista->recInicioFin(); // Utiliza el método .recInicioFin para crear una concatenación de string con los elementos de la estructura
	}
};


