package com.company;

public class ListNode {

    /*La mayor diferencia referente a las listas sencillas enlazadas , es que su navegación
	o recorrido solo se puede hacer de un sentido , de izquierda a derecha O derecha a izquierda , 
	así solo conteniendo un final "Null" , por lo cual , al ser doblemente enlazada , se tiene que 
	crear con la lógica tanto que antes de la cabeza como despus de la cola , hay un null as pudiendo realizar los metodos */


    private Object object; //Objeto a almacenar
    public ListNode previous; //Puntero o dirección para ir al nodo anterior
    public ListNode next; //Puntero o dirección para ir al siguiente nodo

    public ListNode() {
        this.object = null; //Solo creo un nodo pero no le paso nada
        this.previous = null;
        this.next = null;
    }

    public ListNode(Object object) {
        this.object = object; //Solo creo un nodo y le paso el objeto más no el nodo
        this.previous = null;
        this.next = null;
    }

    public ListNode(Object object, ListNode next, ListNode previous) {
        this.object = object; //Solo creo un nodo y le paso ambos valores
        this.next = next;
        this.previous = previous;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public boolean isEquals(Object object) {
        if (this.getObject().toString().equals(object.toString())) {
            return true;
        }
        return false;
    }

    public boolean isEquals(ListNode node) {
        if (this.toString().equals(node.toString())) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "ListNode{" + "object=" + object + "," + "next=" + next + '}';
    }

    public String toStringReverse() {
        return "ListNode{" + "object=" + object + "," + "previous=";
    }
}
