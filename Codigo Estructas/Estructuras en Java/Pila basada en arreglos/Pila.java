package com.company;
import java.util.Arrays;

public class Pila implements IPila {


    /*
    Nota : Es una pila básada en los arreglos del propio lenguaje , volviendola así una estructura estática 
    */

    private int size;
    private Object[] array;
    private int top; // Es necesario saber la posición del top ya que es un arreglo

    public Pila(int size) { 
        this.size = size;
        this.array = new Object[(size > 0) ? size : 1];
        clear();
    }

    @Override
    public void clear() { //Recorre todas las posiciones del arreglo volviendo en cada posición su elemento correspondiente en null
        for (int i = 0; i < array.length; i++) {
            array[i] = null;
        }
        top = -1; // Se establece el top "-1" debido a que la estructura esta vacia
    }

    @Override
    public boolean isEmpty() {
        return array[0] == null; // Si en la posición 0 , el objeto es igual a null , significa que la estructura esta vacia
    }

    @Override
    public Object peek() {
        return (!isEmpty()) ? array[top] : null; // Extrae el top que es el ultimo elemento que ha sido ingresado en la pila (Siempre y cuando que el método isEmpty verifique que almenos haya un elemento la estructura)
    }

    @Override
    public Object pop() {
        if (!isEmpty()) {
            Object object = array[top]; // Se almacena el top de la pila en la variable object 
            array[top--] = null; // Se "Elimina" el top declarandolo como null y le resta -1 a top
            return object; // Retorna el la variable con el antiguo top
        } else {
            return null;
        }
    }

    @Override
    public boolean push(Object object) {
        if (top + 1 < size) {
            try {
                array[++top] = object; // Se le agrega un nuevo espacio al array y en ese sitio se almacena la variable ingresada en el parametro
                return true;
            } catch (Exception e) {
                System.out.println(e);
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int size() {
        return top + 1; // Retorna ++top (El tamaño de la estructura) debido a que un array empieza desde 0 
    }

    @Override
    public boolean search(Object object) {
        Object[] copia = new Object[size];
        boolean correcto = false; 
        for (int i=0; i<size; i++){ // Se recorre el arreglo
            copia[i] = pop(); // Se extrae elemento por elemento hasta que encuentre el objeto ingresado en el parametro , si no es así retorna el valor orignal de booleano (False)
            if (copia[i]==object){
                correcto = true;
            }
        }

        for (int i=0; i<size; i++){
            array[i] = copia[(size-1)-i];
        }

        top = size-1;

        return correcto;
    }

    @Override
    public void sort() { //Organiza los elementos de la pila 
        Object[] resultado = new Object[size];
        Object dato;
        while (array.length != 0){
            dato = Eliminar(array);
            this.Situar(dato, resultado);
        }
        array = resultado;
        //Arrays.sort(array);
    }

    @Override
    public void reverse() { // Invierte los elementos de la pila
        Object[] newArray = new Object[size];
        for (int i=0; i<size; i++){
            newArray[i] = pop();
        }
        array = newArray;
        top = size-1;
    }

    @Override
    public String toString() { // Crea una concatenación de Strings con todos los elementos del array (Pila)
        return "ArrayStack{" +
                "size=" + size +
                ", array=" + Arrays.toString(array) +
                ", top=" + top +
                '}';
    }

    public Object Eliminar(Object[] arrayAEliminar){
        Object[] newArray = new Object[size-1];
        for (int i=0; i<size-1; i++){
            newArray[i] = arrayAEliminar[i];
        }
        array = newArray;
        size = array.length;
        top = size-1;

        return arrayAEliminar[arrayAEliminar.length-1];
    }

    public void AgregarAlFinal(Object[] arrayAInsertar, Object objectoAAgregar){
        Object[] newArray = new Object[size+1]; //Aumenta la capacidad de almacenamiento
        for (int i=0; i<size+1; i++){
            if (i != size){
                newArray[i] = arrayAInsertar[i]; //Agrega los elementos en el nuevo array
            }else {
                newArray[i] = objectoAAgregar; // Y al final como su "Top" el ultimo objeto ingresado
            }

        }

        top = size; // El tamaño aumenta en +1
        array = newArray;
        size = array.length;
    }

    private void Situar(Object dato, Object[] resultado) { 
        Object[] auxiliar = new Object[size];
        if (resultado.length == 0){
            AgregarAlFinal(resultado,dato);
            //auxiliar.
        }else {
            if (dato.toString().compareTo(resultado[resultado.length-1].toString()) <= 0){
                AgregarAlFinal(resultado,dato);
            }
        }
    }
}
