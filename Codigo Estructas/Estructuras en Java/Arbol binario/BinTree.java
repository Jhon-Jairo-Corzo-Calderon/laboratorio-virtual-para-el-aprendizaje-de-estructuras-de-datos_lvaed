package com.company;

public class BinTree {

    //ATRIBUTOS

    private int gradoArbol, alturaArbol, cont = 1;;
    private String preorder = "", inorder = "", posorder = "";
    private boolean search = false;
    public Node root;

    //MÉTODOS Y CONSTRUCTORES

    //Constructores
    public BinTree(){
        gradoArbol = alturaArbol = 0;
        this.root = null;
    }

    public BinTree(Object o){
        gradoArbol = alturaArbol = 0;
        this.root = SubBinTree(null, o, null);
    }

    //Método SubBinTree
    public Node SubBinTree(Node left, Object o, Node right){
        Node nodoARetornar = new Node(left, o, right);
        CalcularGradoArbol(nodoARetornar);
        return nodoARetornar;
    }

    //Método IsEmpty
    public boolean IsEmpty(){
        if (root.GetObject()==null & root.right == null & root.left == null){
            return true;
        }else{
            return false;
        }
    }

    //Método Root
    public boolean Root(Object o){
        try {
            this.root = SubBinTree(null,o,null);
            CalcularGradoArbol(this.root);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Método InsertLeft
    public boolean InsertLeft(Object o){
        try{
            if (!IsEmpty()){
                root.left = SubBinTree(null, o, null);
                return true;
            }else{
                Root(o);
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Método InsertRight
    public boolean InsertRight(Object o){
        try{
            if (!IsEmpty()){
                root.right = SubBinTree(null, o, null);
                return true;
            }else{
                Root(o);
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Métodos necesarios para realizar el search en el arbol binario
    private void Search (Node n, Object o){
        if (!IsEmpty()){
            if (n.GetObject().toString().compareTo(o.toString()) == 0){
                search = true;
            }
            if (n.right != null){
                if (n.right.GetObject().toString().compareTo(o.toString()) == 0){
                    search = true;
                }else{
                    Search(n.right, o);
                }
            }
            if (n.left != null){
                if (n.left.GetObject().toString().compareTo(o.toString()) == 0){
                    search = true;
                }else{
                    Search(n.left, o);
                }
            }
        }
    }

    public boolean SearchObject(Object o){
        Search(root,o);
        if (search == true){
            search = false; //Se vuelve a dejar la variable booleana en false para en un futuro poder volverla a usar
            return true;
        }else{
            return false;
        }
    }

    //Método Eliminar
    public void Eliminar(Node ROOT, Node nodoAEliminar){
        if (!IsEmpty()){
            if (ROOT.GetObject().toString().compareTo(nodoAEliminar.GetObject().toString()) == 0){
                if (ROOT != root){
                    ROOT.right = null;
                    ROOT.left = null;
                }else {
                    Root(null);
                }
                return;
            }
            if (ROOT.right != null){
                if (ROOT.right.GetObject().toString().compareTo(nodoAEliminar.GetObject().toString()) == 0){
                    ROOT.right = null;
                }else{
                   Eliminar(ROOT.right, nodoAEliminar);
                }
            }
            if (ROOT.left != null){
                if (ROOT.left.GetObject().toString().compareTo(nodoAEliminar.GetObject().toString()) == 0){
                    ROOT.left = null;
                }else{
                    Eliminar(ROOT.left, nodoAEliminar);
                }
            }
        }
    }

    //Métodos necesarios para realizar el preorder
    private void PreOrder(Node n){
        if (!IsEmpty()){
            preorder += (String) n.GetObject();
            if (n.left!=null){
                PreOrder(n.left);
            }
            if (n.right!=null){
                PreOrder(n.right);
            }
        }
    }

    public String GetPreOrder(){
        PreOrder(root);
        return preorder;
    }

    //Métodos necesarios para realizar el inorder
    private void InOrder(Node n){
        if (!IsEmpty()){
            if (n.left!=null){
                InOrder(n.left);
            }
            inorder += (String) n.GetObject();
            if (n.right!=null){
                InOrder(n.right);
            }
        }
    }

    public String GetInOrder(){
        InOrder(root);
        return inorder;
    }

    //Métodos necesarios para realizar el posorder
    private void PosOrder(Node n){
        if (!IsEmpty()){
            if (n.left!=null){
                PosOrder(n.left);
            }
            if (n.right!=null){
                PosOrder(n.right);
            }
            posorder += (String) n.GetObject();
        }
    }

    public String GetPosOrder(){
        PosOrder(root);
        return posorder;
    }

    //Métodos necesarios para cálcular el grado del arbol binario
    private void CalcularGradoArbol(Node n){
        if (!IsEmpty()){
            if(n.Grado() == 1){
                gradoArbol = 1;
                if (n.right!=null){
                    CalcularGradoArbol(n.right);
                }
                if (n.left!=null){
                    CalcularGradoArbol(n.left);
                }
            }
            else if (n.Grado() == 2){
                gradoArbol = 2;
            }
        }else{
            gradoArbol = 0;
        }

    }

    public int GetGrado(){
        CalcularGradoArbol(root);
        return gradoArbol;
    }

    //Métodos necesarios para cálcular la altura del arbol binario
    private void CalcularAlturaArbol(Node n){
        if (!IsEmpty()){
            if (n.right!=null){
                CalcularAlturaArbol(n.right);
            }
            if (n.left!=null){
                CalcularAlturaArbol(n.left);
            }
            cont++;
        }
        else{
            alturaArbol = 1;
            return;
        }

        if (cont%2!=0){
            alturaArbol = (cont/2)+1;
        }else{
            alturaArbol = cont/2;
        }
    }

    public int GetAltura(){
        CalcularAlturaArbol(root);
        return alturaArbol;
    }
}