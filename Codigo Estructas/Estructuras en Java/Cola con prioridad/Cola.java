package com.company;

public class Cola implements ICola{

    private int minPrioridad;
    private int limiteMaxPrioridad;
    private List lista = new List();

    public Cola(int minPrioridad, int limiteMaxPrioridad){ // Constructor
        this.minPrioridad = minPrioridad;
        this.limiteMaxPrioridad = limiteMaxPrioridad;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
            lista.add(new List()); // Funciona como sublistas dependiendo la categoria asignada
        }
    }

    @Override
    public void clear() { 
        lista.clear(); 
    }

    @Override
    public boolean isEmpty() {
        return lista.isEmpty();
    }

    @Override
    public Object extract() {
        ListNode inode = lista.head; // Se declara el Nodo cabeza (Primer elemento que ingreso)
        for (int i=limiteMaxPrioridad+1; i>=minPrioridad; i--){
            List l = (List) inode.getObject();
            if (!l.isEmpty()){
                Object object = l.head.getObject();
                l.remove(l.head);
                return object; // Si la lista ,por ejemplo , de prioridad 5 tiene almenos un elemento , se extrae este
            }else{
                inode = inode.next; // Recorre el otro nodo el cual es una lista con los elementos de cierta prioridad
            }
        }
        return null;
    }

    @Override
    public boolean insert(Object object, int prioridad) {
        ListNode inode = lista.head;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
            if (i == prioridad){
                List l = (List) inode.getObject();
                l.add(object); // Si el Nodo contiene la prioridad que es , se le ingresa a la sublista
            }else{
                inode = inode.next;
            }
        }
        return true;
    }

    @Override
    public int size() {
        int cont = 0;
        ListNode inode = lista.head;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
            List l = (List) inode.getObject();
            cont += l.getSize();
            inode = inode.next;
        }
        return cont; // Recorre los elementos de las sublistas y los cuenta
    }

    @Override
    public boolean search(Object object) {
        try{
            ListNode inode = lista.head;
            for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
                List l = (List) inode.getObject();
                if (l.search(object) != null){ // Recorre las sublistas y si encuentra el elemento en alguna de esas , retorna true
                    return true;
                }else {
                    inode = inode.next;
                }
            }
            return false;
        }catch (Exception e){
            return false;
        }

    }

    @Override
    public void sort() {
        ListNode inode = lista.head;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
            List l = (List) inode.getObject();
            if (!l.isEmpty()){
                l.sortList(); // Utiliza el método .sort de las listas para organizar el resultado
            }
            inode = inode.next;
        }
    }

    @Override
    public void reverse() {
        List generalReverseList = new List(), l, reverseList;
        ListNode inode = lista.head, inodel;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--){
            l = (List) inode.getObject();
            reverseList = new List();
            inodel = l.tail;
            for (int j = l.getSize(); j > 0; j-- ){
                reverseList.add(inodel.getObject());
                inodel = inodel.previous;
            }
            generalReverseList.add(reverseList);
            inode = inode.next;
        }
        //Invierte cada sublista 
        lista = generalReverseList;
    }

    @Override
    public String toString(){ // Recorre las sublistas y crea una concatenación con sus elementos
        String output = "";
        ListNode inode = lista.head;
        for (int i=limiteMaxPrioridad; i>=minPrioridad; i--) {
            List l = (List) inode.getObject();
            output += "Prioridad " + i + " -> " + l.recInicioFin() + " / ";
            inode = inode.next;
        }
        return output;
        //return lista.recInicioFin();
    }
}
