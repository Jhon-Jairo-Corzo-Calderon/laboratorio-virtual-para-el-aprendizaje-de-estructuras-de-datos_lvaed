package com.company;

public class BinSearchTree {

    //ATRIBUTOS

    private String preorder = "", inorder = "", posorder = "";
    private boolean search = false;
    public Node root;

    //MÉTODOS Y CONSTRUCTORES

    //Constructores

    public BinSearchTree(){
        this.root = null;
    }

    public BinSearchTree(Object o, int id){
        this.root = new Node(null, o, null, id);
    }

    //Método IsEmpty

    public boolean IsEmpty(){
        if (root == null){
            return true;
        }else{
            return false;
        }
    }

    //Método Insert
    public boolean Insert(Object o, int id){
        if (!IsEmpty()){
            Node inode = root;
            while (inode!=null){
                if (inode.GetID()<=id){
                    if(inode.right != null){
                        inode = inode.right;
                    }else{
                        inode.right = new Node(null,o,null,id);
                        break;
                    }
                }else{
                    if(inode.left != null){
                        inode = inode.left;
                    }else{
                        inode.left = new Node(null,o,null,id);
                        break;
                    }
                }
            }
        }else{
            root = new Node(null,o,null,id);
        }
        return true;
    }

    //Método search

    private Node search(Node root, int value) {
        if (root != null) {
            if (root.GetID() == value) {
                return root;
            } else if (root.GetID() < value) {
                return search(root.right, value);
            } else {
                return search(root.left, value);
            }
        }

        return null;
    }

    public Node search(int value) {
        return search(this.root, value);
    }

    //Método remove
    public boolean Remove(int id){
        if (!IsEmpty()){
            Node inode = root;
            Node preInode = root;
            while(inode.GetID() != id){
               preInode = inode;
               if (inode.GetID()>id){
                   inode = inode.left;
               }else{
                   inode = inode.right;
               }
               if (inode == null){
                   return false;
               }
            }
            if (inode.right == null & inode.left== null){
                if (inode == root){
                    root = null;
                }else if(preInode.left == inode){
                    preInode.left = null;
                }else{
                    preInode.right = null;
                }
            }else if(inode.right == null){
                if (inode == root){
                    root = root.left;
                }else if (preInode.left == inode){
                    preInode.left = inode.left;
                }
                else{
                    preInode.right = inode.left;
                }
            }else if(inode.left == null){
                if (inode == root){
                    root = root.right;
                }else if (preInode.left == inode){
                    preInode.left = inode.right;
                }
                else{
                    preInode.right = inode.right;
                }
            }else{
                Node rePre = inode, re = inode, aux = inode.right;
                while (aux != null){
                    rePre = re;
                    re = aux;
                    aux = aux.left;
                }
                if (re != inode.right){
                    rePre.left = re.right;
                    re.right = inode.right;
                }

                if (inode==root){
                    root = re;
                }else if(preInode.left == inode){
                    preInode.left = re;
                }else{
                    preInode.right = re;
                }
                re.left = inode.left;
            }
        }else{
            return false;
        }
        return true;
    }

    //Métodos necesarios para realizar el preorder
    private void PreOrder(Node n){
        if (!IsEmpty()){
            preorder += (String) n.GetObject();
            if (n.left!=null){
                PreOrder(n.left);
            }
            if (n.right!=null){
                PreOrder(n.right);
            }
        }
    }

    public String GetPreOrder(){
        PreOrder(root);
        return preorder;
    }

    //Métodos necesarios para realizar el inorder
    private void InOrder(Node n){
        if (!IsEmpty()){
            if (n.left!=null){
                InOrder(n.left);
            }
            inorder += (String) n.GetObject();
            if (n.right!=null){
                InOrder(n.right);
            }
        }
    }

    public String GetInOrder(){
        InOrder(root);
        return inorder;
    }

    //Métodos necesarios para realizar el posorder
    private void PosOrder(Node n){
        if (!IsEmpty()){
            if (n.left!=null){
                PosOrder(n.left);
            }
            if (n.right!=null){
                PosOrder(n.right);
            }
            posorder += (String) n.GetObject();
        }
    }

    public String GetPosOrder(){
        PosOrder(root);
        return posorder;
    }
}
