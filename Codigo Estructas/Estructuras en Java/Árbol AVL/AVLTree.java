package com.company;

public class AVLTree {

    //ATRIBUTOS

    private String preorder = "", inorder = "", posorder = "";
    private boolean search = false;
    public Node root;

    //MÉTODOS Y CONSTRUCTORES

    //Constructores
    public AVLTree(){
        this.root = null;
    }

    public AVLTree(Object o, int id){
        this.root = new Node(null, o, null, id);
    }

    //Método IsEmpty
    public boolean IsEmpty(){
        if (root == null){
            return true;
        }else{
            return false;
        }
    }

    //Método InsertBalanced, propio de los arboles balanceados como el árbol AVL

    private Node InsertBalanced(Node insert, Node main){
        Node newMain = main;
        if (insert.GetID()<main.GetID()){
            if (main.left ==null){
                main.left = insert;
            }else{
                main.left = InsertBalanced(insert, main.left);
                if (Getfe(main.left) - Getfe(main.right) == 2){
                    if (insert.GetID()<main.left.GetID()){
                        newMain = Rsi(main);
                    }else{
                        newMain = Rdi(main);
                    }
                }
            }
        }else if(insert.GetID()> main.GetID()){
            if (main.right == null){
                main.right = insert;
            }else{
                main.right = InsertBalanced(insert, main.right);
                if (Getfe(main.right) - Getfe(main.left) == 2) {
                    if (insert.GetID() > main.right.GetID()) {
                        newMain = Rsd(main);
                    } else {
                        newMain = Rdd(main);
                    }
                }
            }
        }else{
            return null;
        }

        if (main.left == null & main.right != null){
            main.SetBalance(main.right.GetBalance()+1);
        }else if(main.right == null & main.left != null){
            main.SetBalance(main.left.GetBalance()+1);
        }else{
            main.SetBalance(Math.max(Getfe(main.left),Getfe(main.right))+1); //Se le suma 1 ya que el factor de equilibrio hace referencia al nivel más 1
        }
        return newMain;
    }

    //Método Insert

    public boolean Insert(Object o, int id){
        Node n = new Node(null,o,null,id);
        if (!IsEmpty()){
            Node newMain = InsertBalanced(n, root);
            if (newMain != null){
                root = newMain;
            }else{
                return false;
            }
        }else{
            root = n;
        }
        return true;
    }

    //Método de obtención del factor de equilibrio

    public int Getfe(Node n){
        if (n!=null){
            return n.GetBalance();
        }else{
            return -1;
        }
    }

    //Métodos de las rotaciones

    //Rotación simple a la izquierda
    public Node Rsi(Node n){
        Node nl = n.left;
        n.left = nl.right;
        nl.right = n;
        n = nl;
        n.SetBalance(0);
        nl.SetBalance(0);
        return nl;
    }

    //Rotación simple a la derecha
    public Node Rsd(Node n){
        Node nl = n.right;
        n.right= nl.left;
        nl.left = n;
        n = nl;
        n.SetBalance(0);
        nl.SetBalance(0);
        return nl;
    }

    //Rotación doble a la izquierda
    public Node Rdi(Node n){
        Node nl = n.right;
        Node nt = nl.left;
        nl.left = nt.right;
        nt.right = nl;
        n.right = nt.left;
        nt.left = n;
        n = nt;

        if (nt.GetBalance()==-1){
            n.SetBalance(0);
            nl.SetBalance(1);
            nt.SetBalance(0);
        }else if (nt.GetBalance()==0){
            n.SetBalance(0);
            nl.SetBalance(0);
            nt.SetBalance(0);
        }else if(nt.GetBalance()==1){
            n.SetBalance(-1);
            nl.SetBalance(0);
            nt.SetBalance(0);
        }
        return nt;
    }

    //Rotación doble a la derecha
    public Node Rdd(Node n){
        Node nl = n.left;
        Node nt = nl.right;
        nl.right = nt.left;
        nt.left = nl;
        n.left = nt.right;
        nt.right = n;
        n = nt;

        if (nt.GetBalance()==-1){
            n.SetBalance(1);
            nl.SetBalance(0);
            nt.SetBalance(0);
        }else if (nt.GetBalance()==0){
            n.SetBalance(0);
            nl.SetBalance(0);
            nt.SetBalance(0);
        }else if(nt.GetBalance()==1){
            n.SetBalance(0);
            nl.SetBalance(-1);
            nt.SetBalance(0);
        }
        return nt;
    }

    //Métodos necesarios para realizar el search en el árbol AVL

    private Node search(Node root, int value) {
        if (root != null) {
            if (root.GetID() == value) {
                return root;
            } else if (root.GetID() < value) {
                return search(root.right, value);
            } else {
                return search(root.left, value);
            }
        }

        return null;
    }

    public Node search(int value) {
        return search(this.root, value);
    }

    public boolean SearchObject(Object o){
        Search(root,o);
        if (search == true){
            search = false; //Se vuelve a dejar la variable booleana en false para en un futuro poder volverla a usar
            return true;
        }else{
            return false;
        }
    }

    //Método remove

    public boolean Remove(int id){
        if (!IsEmpty()){
            Node inode = root;
            Node preInode = root;
            while(inode.GetID() != id){
               preInode = inode;
               if (inode.GetID()>id){
                   inode = inode.left;
               }else{
                   inode = inode.right;
               }
               if (inode == null){
                   return false;
               }
            }
            if (inode.right == null & inode.left== null){
                if (inode == root){
                    root = null;
                }else if(preInode.left == inode){
                    preInode.left = null;
                }else{
                    preInode.right = null;
                }
            }else if(inode.right == null){
                if (inode == root){
                    root = root.left;
                }else if (preInode.left == inode){
                    preInode.left = inode.left;
                }
                else{
                    preInode.right = inode.left;
                }
            }else if(inode.left == null){
                if (inode == root){
                    root = root.right;
                }else if (preInode.left == inode){
                    preInode.left = inode.right;
                }
                else{
                    preInode.right = inode.right;
                }
            }else{
                Node rePre = inode, re = inode, aux = inode.right;
                while (aux != null){
                    rePre = re;
                    re = aux;
                    aux = aux.left;
                }
                if (re != inode.right){
                    rePre.left = re.right;
                    re.right = inode.right;
                }

                if (inode==root){
                    root = re;
                }else if(preInode.left == inode){
                    preInode.left = re;
                }else{
                    preInode.right = re;
                }
                re.left = inode.left;
            }
        }else{
            return false;
        }
        return true;
    }

    //Métodos necesarios para realizar el preorder

    private void PreOrder(Node n){
        if (!IsEmpty()){
            preorder += (String) n.GetObject();
            if (n.left!=null){
                PreOrder(n.left);
            }
            if (n.right!=null){
                PreOrder(n.right);
            }
        }
    }

    public String GetPreOrder(){
        PreOrder(root);
        return preorder;
    }

    //Métodos necesarios para realizar el inorder

    private void InOrder(Node n){
        if (!IsEmpty()){
            if (n.left!=null){
                InOrder(n.left);
            }
            inorder += (String) n.GetObject();
            if (n.right!=null){
                InOrder(n.right);
            }
        }
    }

    public String GetInOrder(){
        InOrder(root);
        return inorder;
    }

    //Métodos necesarios para realizar el posorder

    private void PosOrder(Node n){
        if (!IsEmpty()){
            if (n.left!=null){
                PosOrder(n.left);
            }
            if (n.right!=null){
                PosOrder(n.right);
            }
            posorder += (String) n.GetObject();
        }
    }

    public String GetPosOrder(){
        PosOrder(root);
        return posorder;
    }
}
