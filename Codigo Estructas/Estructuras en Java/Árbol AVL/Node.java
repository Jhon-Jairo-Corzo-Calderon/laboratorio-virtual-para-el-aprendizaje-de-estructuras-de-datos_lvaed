package com.company;

public class Node {

    //ATRIBUTOS

    private Object object;
    public Node right;
    public Node left;
    private int id;
    private int balance;

    //MÉTODOS Y CONSTRUCTORES

    //Constructores
    public Node(Object o, int id){
        this.object = o;
        this.right = null;
        this.left = null;
        this.id = id;
        this.balance = 0;
    }

    public Node(Node left,Object object, Node right, int id) {
        this.object = object;
        this.left = left;
        this.right = right;
        this.id = id;
        this.balance = 0;
    }

    public int GetID(){
        return id;
    }

    public int GetBalance(){
        return balance;
    }
    public void SetBalance(int balance){
        this.balance = balance;
    }

    //Getter deL atributo object
    public Object GetObject() {
        return object;
    }

    //Método del cálculo del grado de un nodo
    public int Grado(){
        if (right==null & left==null){
            return 0;
        }else if ((right == null & left!=null) | (right != null & left==null)){
            return 1;
        }else{
            return 2;
        }
    }

    //Método toString
    @Override
    public String toString() {
        return "Node{ left=" + left + ", right=" + right + ", object=" + object + ", id=" + id + '}';
    }
}