package com.company;
import java.util.Arrays;

public class Cola implements ICola{


    /*

    Nota : Al estar basada en arrays , convierte la cola en una estructura estática  

    */

    private Object array[];

    public Cola(int size){
        if (size>0){
            array = new Object[size];
        }else{
            array = new Object[1];
        }
    }
    @Override
    public void clear() { // Recorre la cola , asignando a cada posición su contenido null
        for (int i = 0; i < array.length; i++) {
            array[i] = null;
        }
    }

    @Override
    public boolean isEmpty() {
        boolean estaVacia = false; 
        for (int i=0; i<size(); i++){
            if (array[i] == null){ // Si el primer el primer elemento es null , significa que la cola esta vacia
                estaVacia = true;
            }else{
                estaVacia = false;
                break; // Rompe el ciclo cuando se de esta condición
            }
        }
        return estaVacia;
    }

    @Override
    public Object extract() {
        Object object = null; // Se declara el Objeto donde se va almacenar el primer elemento de la cola
        for (int i=0; i<size(); i++){
            if (array[i]!=null){
                object = array[i]; // Asigna el objeto el valor del primer elemento de la cola
                array[i] = null; // Elimina el elemento
                return object;
            }
        }
        return object; // Se hace este return debido a que si la cola esta vacia , retorna el valor original asignado (Null)
    }

    @Override
    public boolean insert(Object object) {
        for (int i=0; i<array.length; i++){
            if (array[i] == null){ // Cuando la posición de un array de null y en el caso de las colas , signfica que será puesto al final de la misma
                array[i] = object; // Se le asigna el objeto a ese espacio
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() { // Retorna el tamaño de la estructura 
        return array.length;
    }

    @Override
    public boolean search(Object object) {
        Object arraycopia[] = new Object[size()];
        for (int i=0; i<size(); i++){
            if (array[i] == object){ // Recorre el array para verificar si el objeto ingresaod en el parametro esta dentro de la estructura
                return true;
            }
        }
        return false;
    }

    @Override
    public void sort() { // Organiza la cola
        List s = new List();
        for (Object o: array){
            s.add(o);
        }
        s.sortList();
        array = s.toArray();
    }

    @Override
    public void reverse() { 
        Object reverseArray[] = new Object[size()]; // Crea un array donde se van almacenar los elementos "Inversos"
        for (int i = 0; i < size(); i++){
            reverseArray[i] = array[array.length-(i+1)]; // Recorre el array original inversamente
        }
        array = reverseArray; //Asigna los valores del Array "Inverso" al original
    }

    @Override
    public String toString(){ // Crea una concatenación de Strings con los elementos del array (Cola)
        return "ArrayTail{" + "size=" + size() + ", array=" + Arrays.toString(array) + '}'; //Aparte del tamaño
    }
}
