package com.company;

public class Pila implements IPila{

    /*

    Nota : la pila basada en listas , es especifico se constituye por las Listas Doblemente Enlazadas (Explicadas anteriormente) , así volviendo dinamica esta estructura

    */

    private List lista;

    public Pila(){ // Constructor para crear una nueva pila
        lista = new List();
    }

    public Pila(Object o){ // Constructor para crear una nueva pila con el parametro o
        lista = new List(o);
    }

    @Override
    public void clear() { 
        if (isEmpty()!=true) {
            lista.clear(); // Se utiliza el método de las listas .clear para vaciar el contenido de la estructura
        }
    }

    @Override
    public boolean isEmpty() {
        if (lista.isEmpty()){ // Mediante el método isEmpty de las listas , se verifica que la pila tenga o no contenido
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public Object peek() { // El método retorna el "Top" de las pilas que básicamente en este caso es la cola de la lista ya que fue el ultimo elemento en ingresar a la estructura
        if (lista.isEmpty() != true){ // Se verifica que la pila tenga almenos un elemento
            return lista.tail.getObject(); // Retorna la cola de la lista
        }
        else {
            return null; // Retorna null ya que no tiene top al estar vacia
        }
    }

    @Override
    public Object pop() { //El método retorna el "Top" y lo elimina de la estructura
        if (lista.isEmpty() != true){ 
            Object object = this.peek(); // Se le asigna a la variable object el top de la pila
            lista.remove(lista.tail); // elimina el "top" de la pila (La cola de la lista)
            return object; // Retorna la variable que contenia el antiguo top de la pila
        }
        else {
            return null; 
        }
    }

    @Override
    public boolean push(Object object) { // El método de agregación de elementos a la pila mediante el .add de las listas
        lista.insertTail(object);
        return true;
    }

    @Override
    public int size() { // Retorna el tamaño de la pila
        return lista.getSize();
    }

    @Override
    public boolean search(Object object) { 
        if (lista.search(object).getObject() == object){ //Busca el objeto del parametro y si esta dentro de la pila (Mediante el .search de las listas) , retorna true
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void sort() {
        //Se modifica el metodo sort de la lista
        List sort = lista;
        sort.sortList();
        lista.clear();
        ListNode node = sort.head;
        for (int i=0;i<sort.getSize();i++){
            lista.add(node.getObject());
            node = node.next;
        }
    }

    @Override
    public void reverse() { // Crea una lista , almacena el contenido desde el top (Hace que los ultimos que ingresaron sean los ultimos que salgan y viceversa) y posteriormente lo vuelve a almacenar en la lista original que es la pila
        List reverse = new List(); 
        ListNode node = lista.head;
        for (int i=0; i<lista.getSize(); i++){ 
            reverse.insertHead(pop());
            node = node.next;
        }
        lista = reverse;
    }
}
