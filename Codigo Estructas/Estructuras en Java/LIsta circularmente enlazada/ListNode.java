import java.util.Iterator;

public class ListNode implements  Iterable<ListNode> {

    /*

    La lógica de esta estructura cabe en saber cuando la lista ya hizo el recorrido completo con el fin de parar este proceso ; Por lo cual a la hora de implementar los métodos el atributo .Next va a ser fundamental para identificar la cabeza o la cola (Según el recorrido implementado) ,así parando el proceso y eliminando los nulls

    */

    private Object object;
    public ListNode next; 

    private ListNode inode;

    public ListNode() {
        this.object = null;
        this.next = null;
    }

    public ListNode(Object object) {
        this.object = object;
        this.next = null;
    }

    public ListNode(Object object, ListNode next) {
        this.object = object;
        this.next = next;
    }

    public Object getObject() {
        try{
            return object;
        }
        catch(Exception e){
            return null;
        }
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public boolean isEquals(Object object) {
        if (this.getObject().toString().equals(object.toString())) {
            return true;
        }
        return false;
    }

    public boolean isEquals(ListNode node) {
        if (this.toString().equals(node.toString())) {
            return true;
        }
        return false;
    }
    


    @Override
    public String toString() {

        ListaCircular ls = new ListaCircular();

        Iterator<ListNode> i = this.iterator();
        ListNode inode;
        do {
            inode=i.next();
            ls.add(inode.getObject());
        } while (inode.next != this);

        return ls.toString();
    }

    @Override
    public Iterator<ListNode> iterator() {
        inode = this;
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return inode.next != null;
            }

            @Override
            public ListNode next() {
                if (inode != null) {
                    ListNode tmp = inode;
                    inode = inode.next;
                    return tmp;
                } else {
                    return null;
                }
            }
        };
    }

}
