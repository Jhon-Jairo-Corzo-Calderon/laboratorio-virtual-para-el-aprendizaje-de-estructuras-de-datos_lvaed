import java.util.Iterator;
import java.util.Arrays;
import static java.lang.System.*;

/*

La caracteristica mas llamativa con respecto a las listas ciclicas es a la hora de recorrer la estructura ya que como su nombre lo indica 
esta en constante ciclo , es decir , no tiene final ; Por lo cual el termino "Null" donde significaba anteriormente el final, desaparecio.

Si no hay un "Stop" , la lista nunca para de recorrerse , en consecuencia siendo una estructura inutilizable (Siendo el recorrido una fundamento para los demas métodos) ; Para solucionar este problema , se tiene que tener en cuenta el Nodo cabeza y el Nodo cola para así cuando pase de cola a cabeza para el proceso (Esto en el caso de la Lista Enlazada Circular Sencilla).

*/

public class ListaCircular implements ListInterface, Iterable<ListNode>{

    private ListNode inode;
    private int size;

    public ListNode head;
    public ListNode tail;

    /**
     * List
     */
    public ListaCircular() {
        clear();
    }

    /*
    ok
     */
    public ListaCircular(Object object) {
        add(object);
    }

    /*
    ok
     */
    public boolean isEmpty() {
        return head == null;
    }

    /*
    ok
     */
    @Override
    public int getSize() {
        return size;
    }

    /*
    ok
     */
    @Override
    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    /*
    ok
     */
    @Override
    public Object getHead() {
        return head;
    }

    /*
    ok
     */
    @Override
    public Object getTail() {
        return tail;
    }

    /*
    ok
     */
    @Override
    public ListNode search(Object object) {
        
        Iterator<ListNode> i = this.iterator();
        ListNode inode;

        for(int x = 0; x<size; x++){
            inode = i.next();
            if (inode.getObject().equals(object)) {
            
                return inode;
            }
        }
        return null;
    }

    
    public ListNode search(ListNode object) {
        
        return search(object.getObject());
    }

    /*
    ok
     */
    @Override
    public boolean add(Object object) {
        return insertTail(object);
    }

    /*
    ok 
     */
    @Override
    public boolean insert(ListNode node, Object object) {
        try {
            if (node.next == null) {
                add(object);
            } else {
                
                ListNode newNode = new ListNode(object);
                newNode.next = node.next;
                node.next = newNode;

            }
            this.size ++;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
    ok
     */
    @Override
    public boolean insert(Object ob, Object object) {
        try {
            if (ob != null) {
                ListNode node = this.search(ob);
                if (node != null) {
                    return insert(node, object);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /*
    ok
     */
    @Override
    public boolean insertHead(Object object) {
        try {
            if (isEmpty()) {
                head = new ListNode(object);
                tail = head;
            } else {
                head = new ListNode(object, head);
                tail.next = head;
            }
            this.size++;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
    ok
     */
    @Override
    public boolean insertTail(Object object) {
        try {
            if (isEmpty()) {
                head = new ListNode(object);
                tail = head;
            } else {
                tail.next = new ListNode(object);
                tail = tail.next;
                tail.next = head; // El Nodo posterior a la cola es la cabeza por lo cual al momento de insertar una nueva cola este objeto quedaría entre la cola anterior y la cabeza (Depende de como el programador lo implemente , esto puede variar)
            }
            this.size++;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean remove(ListNode node) {

        ListNode nodeB = this.search(node.getObject());
        
        if(nodeB != null){
            if(nodeB.getObject()==head.getObject()){
                ListNode temp = this.head.next;
                this.tail.next = temp;
                this.head = temp;    
                // En este caso como el Nodo a eliminar es la cabeza , el Nodo anterior es la cola , por lo cual esta se le asigna como la nueva cabeza 
            }
            else if(nodeB.getObject()==tail.getObject()){
                this.getBeforeTo(nodeB).next = head; // En este caso como el Nodo a eliminar es la cola , se establece como la nueva cola el nodo anterior 
            }
            else{
                this.getBeforeTo(nodeB).next = nodeB.next;
                nodeB.next = null;
                
            } 

            this.size--;
            return true;       
        }
        return false;
    }


    @Override
    public boolean remove(Object object) {
        ListNode lNode = new ListNode(object);
        return remove(lNode);
    }
    
    @Override
    public boolean contains(Object object) {

        if(search(object)==null)
            return false;
        
        return true;

    }

    @Override
    public Object[] toArray() {
        Object[] arreglo = new Object[size];

        Iterator<ListNode> i = this.iterator();
        ListNode inode;
        int cont = 0;

        for(int x = 0; x<size; x++){
            inode = i.next();
            arreglo[cont] = inode.getObject();            
            cont++;
        }


        return arreglo;
    }

    @Override
    public Object[] toArray(Object[] object) {

        Object[] arreglo = new Object[size + object.length];
        Object[] arreglo2 = toArray();

        int cont = 0;
        while(cont< object.length+size){
            if(cont == size){
                for (Object object2 : object) {
                    arreglo[cont] = object2;
                    cont++;
                }

                break;
            }
            else{
                arreglo[cont] = arreglo2[cont];
            }
            cont++;
        }
        return arreglo;
    }

    public ListaCircular backToList(Object[] objects){ // Un nuevo método el cual se encarga de recorrer un arreglo y almacenar estos valores en la lista
        ListaCircular lista = new ListaCircular();
        for (Object object : objects) {
            lista.add(object);
        }
        return lista;
    }

    @Override
    public Object getBeforeTo() {
        
        Iterator<ListNode> i = this.iterator();
        ListNode inode;

        while ((inode = i.next()) != null) {
            
            if (inode.next.getObject()==this.tail.getObject()) {
                return inode.getObject();
            }
        }

        return null;
    }

    @Override
    public ListNode getBeforeTo(ListNode node) {
        Iterator<ListNode> i = this.iterator();
        ListNode inode;

        while ((inode = i.next()) != null) {

            if (inode.next.getObject()==node.getObject()) {
                return inode;
            }

            
        }

        return null;
    }

    @Override
    public Object getNextTo() {
        return head.next;
    }

    @Override
    public Object getNextTo(ListNode node) {
        Iterator<ListNode> i = this.iterator();
        ListNode inode;

        while ((inode = i.next()) != null) {
            
            if (inode.getObject()==node.getObject()) {
                return inode.next.getObject();
            }
        }

        return null;
    }

    public ListNode getnextTo(ListNode node) {
        Iterator<ListNode> i = this.iterator();
        ListNode inode;

        while ((inode = i.next()) != null) {
            
            if (inode.getObject()==node.getObject()) {
                return inode.next;
            }
        }

        return null;
    }
    

    @Override
    public ListaCircular subList(ListNode from, ListNode to) {
        try {
            ListaCircular sublist = new ListaCircular();
            if(from != null && to != null){
                Iterator<ListNode> i = this.iterator();
                ListNode inode;
                boolean frExist = false;
                while ((inode = i.next()) != null) {
                    if(inode.getObject().equals(to.getObject())){
                        sublist.add(inode.getObject());
                        return sublist;
                    }
                    else if(inode.getObject().equals(from.getObject())|| frExist){
                        sublist.add(inode.getObject());
                        frExist = true;
                    }
                    

                }                
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
    
    public ListaCircular subList(Object from, Object to) {
        ListNode nfrom = new ListNode(from);
        ListNode nto = new ListNode(to);
        return this.subList(nfrom, nto);
    } 

    @Override
    public ListaCircular sortList() {
        try{
            Object[] array = toArray(); //Almacena la lista en un arreglo

            Arrays.sort(array); //Utiliza el método .sort tipicos de los arreglos para organizar la estructura 

            return backToList(array); // Convierte el arreglo a una lista
        }
        catch(Exception e){
            return null;
        }
    }

    @Override
    public String toString() {
        String output = ""; 

        Iterator<ListNode> i = this.iterator();
        ListNode inode;
        for(int x = 0; x<size; x++){
            inode = i.next();

            if(inode.next == this.head && x !=0 ){
                output += "ListNode{" +
                "object=" + inode.getObject() +
                ", next=" + inode.next.getObject() + "(head)";
                for(int z = 0; z<size; z++){
                    output += '}';
                }

            }
            else{
                output += "ListNode{" +
                "object=" + inode.getObject() +
                ", next=";
            }

            
        }

        return output;
    }

    @Override
    public Iterator<ListNode> iterator() {
        inode = head;
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return inode.next != null;
            }

            @Override
            public ListNode next() {
                if (inode != null) {
                    ListNode tmp = inode;
                    inode = inode.next;
                    return tmp;
                } else {
                    return null;
                }
            }
        };
    }

    public void rec(ListNode node) {
        if (node.next != null) {
            rec(node.next);
            // <- ;) ->
        }
        out.println(node.toString());
    }
}
