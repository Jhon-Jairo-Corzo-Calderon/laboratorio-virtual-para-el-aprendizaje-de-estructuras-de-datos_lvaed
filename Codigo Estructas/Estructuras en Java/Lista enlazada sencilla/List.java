package com.company;

import java.util.Iterator;
import static java.lang.System.*;

public class List implements IList, Iterable<ListNode>{

    private ListNode inode; //Nodo iterable
    private int size;

    public ListNode head; //Punteros para saber donde está el inicio y el fin
    public ListNode tail;

    /**
     * List
     */
    public List() { //Constructor para borrar el contenido
        clear();
    }

    /*
    ok
     */
    public List(Object object) {
        add(object); // Constructor para agregar contenido
    }

    /*
    ok
     */
    public boolean isEmpty() { // Un metodo boleano que va a ser muy utilizado a lo largo de la estructura para verificar si esta vacia o no
        return head == null; // Si la cabeza es null , retorna false , esto significa que no existe ningún elemento que tome esta posición y por consecuente , la estructura esta vacia
    }

    /*
    ok
     */
    @Override
    public int getSize() {
        return size; // Retornamos la variable la cual va a ser modificada alrededor de los demas metodos
	}

    }

    /*
    ok
     */
    @Override
    public void clear() { // Se elimina tanto la cola como la cabeza , eliminando así todo el contenido de la estructura , además de igualar el tamaño del contenido a "0" por naturalidad del caso
        head = null;
        tail = null;
        size = 0;
    }

    /*
    ok
     */
    @Override
    public Object getHead() {
        return head;// Retorna el contenido de la cabeza
    }

    /*
    ok
     */
    @Override
    public Object getTail() {
        return tail; // Retorna el contenido de la cola
    }

    /*
    ok
     */
     // Si la cola y la cabeza son la misma , retornara el mismo contenido
    @Override
    public ListNode search(Object object) {
        Iterator<ListNode> i = this.iterator();
        ListNode inode;
        while ((inode = i.next()) != null) { //Itera hasta encontrar al contenido de un nodo que sea igual al objeto ingresado con el fin de retornarlo
            if (inode.getObject().toString().equals(object.toString())) {
                return inode;
            }
        }
        return null; // Y en caso que no lo encuentre , retornara Null
    }

    /*
    ok
     */
    @Override
    public boolean add(Object object) {
        return insertTail(object); //El metodo .add funciona con la lógica de la inserción de la cola
    }

    /*
    ok
     */
    @Override
    public boolean insert(ListNode node, Object object) { //Un método boolean el cual busca un nodo y en esa posición inserta el objeto del parámetro (El contenido del nodo anterior simplemente se desplaza)
        try {
            if (node.next == null) { // Primera dirección donde simplemente se agrega a lo ultimo
                add(object);
            } else {
                ListNode newNode = new ListNode(object); // Segunda direción donde se tiene que mover el nodo para almacenar el valor del parametro y así no perder el anterior elemento que estaba en esa posición
                newNode.next = node.next;
                node.next = newNode;
                this.size++; // El tamaño aumenta debido a la integración del nuevo nodo
            }
            return true;
        } catch (Exception e) {
            return false; // Retorna falso si detecta algún error
        }
    }

    /*
    ok
     */
    @Override
    public boolean insert(Object ob, Object object) { // Un método boolean el cual busca el objeto b dentro de la lista y lo reemplaza por el objecto object
        try {
            if (ob != null) {
                ListNode node = this.search(ob); // Mediante el metodo .search se asegura que el objeto esta dentro de la lista y retorna su valor para su posterior uso
                if (node != null) {
                    return insert(node, object);
                } else {
                    return false; // Retorna falso si el objeto ingresado es igual a null
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
// Como sustituyo al contentido de cierto nodo , la lista no se le suma ni resta ya que aunque cambio internamente , estructuralmente sigue igual
    /*
    ok
     */
    @Override
    public boolean insertHead(Object object) { // Se añade y asigna al objeto ingresado como la cabeza de la estructura
        try {
            if (isEmpty()) {
                head = new ListNode(object); //Se crea el nodo
                tail = head; // como esta vacia , al insertar por este metodo , la cabeza se convierte en el nodo que contiene al objeto del parametro y como solo existe un nodo , la cabeza es igual a la cola
            } else {
                head = new ListNode(object, head); // Se le asigna como cabeza de la estructura al nodo ingresado
            }
            this.size++;// Aumenta ya que siempre se va a ingresar un nuevo nodo 
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
    ok
     */
    @Override
    public boolean insertTail(Object object) { // Se añade y asigna al objeto ingresado como la cola de la estructura
        try {
            if (isEmpty()) {
                head = new ListNode(object); //Se crea el nodo
                tail = head; //como esta vacia , al insertar por este metodo , la cola se convierte en el nodo que contiene al objeto del parametro y como solo existe un nodo , la cabeza es igual a la cola
            } else {
                tail.next = new ListNode(object); //tail hace referencia al nodo completo, tail.next hace referencia a la cajita del puntero
                tail = tail.next;
            }
            this.size++; // Aumenta ya que siempre se va a ingresar un nuevo nodo 
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
    ok
     */
    @Override
    public boolean remove(ListNode node) { // convierte y elimina un nodo mediante la lógica del siguiente metodo
        remove(node.getObject());
        return  true;
    }

    /*
    ok
     */
    @Override
    public boolean remove(Object object) {// Remueve un elemento de la lista
        if (!isEmpty()){//Verifica que la lista tenga elementos
            if(head == tail && object == head.getObject()){// En caso de que la cabeza sea igual que la cola y el objeto ingresado sea igual a la cabeza , se vacia la estructura
                head=null;
                tail=null;
            }
            else if(object == head.getObject()){// En caso de que el objeto sea igual a la cabeza , la cabeza actual desaparece dejando este cargo al siguiente nodo (Se sobreescribe)
                head = head.next;
            }
            else{
                ListNode previous = head,temp = head.next;
                while (temp != null && temp.getObject() != object){
                    previous = previous.next;
                    temp = temp.next;
                }
                if (temp != null){
                    previous.next = temp.next;// Se asigna el contenido del nodo anterior al nodo que acaba de perder su elemento
                    if (temp == tail){
                        tail = previous;// En caso de que sea la cola , simplemente se toma el nodo anterior a la misma para asginarle este rol
                    }
                }
            }
            this.size--;
            return true;// Retorna verdadero para avisar que elimino efectivamente un elemento de la lista
        }
        return false;// Retorna falso para avisar que no se hizo ningún procedimiento a causa de que la lista estaba vacia

    }

    @Override
    public boolean contains(Object object) {// Metodo booleano el cual se encarga de verficar unicamente si existe el objeto ingresado en el parametro en la lista
        inode = head;
        while (inode != null){
            if (inode.getObject() == object){
                return true;// En caso de que el contenido de un nodo sea igual al parametro ingresado , se rompe el ciclo y retorna verdadero , así avisando que este si existe dentro de la lista
            }
            inode = inode.next;// El nodo se convierte en el nodo posterior para así recorrer la estructura y alcanzar un momento el null para romper el ciclo
        }
        return false;// Si nunca detecta un parametro igual y rompe el ciclo gracias a que el nodo se "convirtio" en null , se retorna false así avisando que el objeto no existe dentro de la lista
    }


    public Object[] toArray() { // Metodo para convertir la lista en un array
	{
        Object[] object = new Object[size];// Crea un Array con el tamaño de la lista , gracias al atributo "size"
        return toArray(object);// Utiliza el siguiente metodo (To.array con parametros) para así almacenar la lista en el Array anteriormente creado
    }

    @Override
    public Object[] toArray(Object[] object) {// Metodo para convertir la lista en un array
        try{
            inode = head;
            for (int i=0; i<size; i++){
                object[i] = inode.getObject(); // Almacena y convierte en objeto el nodo cabeza mediante el anterior ciclo
                inode = inode.next;// Se recorre los nodos así cambiando el contenido según la lista en cada iteración del nodo
            }
            return object;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public Object getBeforeTo() {
        return getBeforeTo(tail).getObject(); //Si invocan al método getBeforeTo que se encuentra sin parámetro se asume que me están pidiendo el objeto que se encuentra antes de la cola
    }

    /*
    ok
     */
    @Override
    public ListNode getBeforeTo(ListNode node) {
        if (!isEmpty()){
            if (head == tail){
                return null; // Retorna null ya que significa que la lista solo posee un elemento
            }
            else if(node == null){
                return tail; // Retorna la cola debido que si el nodo es null , el anterior será la cola
            }
            else {
                ListNode previous = null;
                inode = head;
                while (inode != null){
                    if (inode.getObject() == node.getObject()){
                        return previous;
                    }
                    else {
                        previous = inode;
                        inode = inode.next;
                    }
                }
                // Identifica el nodo y retorna el anterior 
            }
        }
        return null;
    }

    @Override
    public Object getNextTo() {
        return getNextTo(head); //Si invocan al método getNextTo que se encuentra sin parámetro se asume que me están pidiendo el objeto que se encuentra después de la cabeza, claro está en caso de haya una cabeza o haya algo después de la cabeza
    }

    @Override
    public Object getNextTo(ListNode node) {
        if (!isEmpty()){
            if (head == tail){
                return null; // Retorna null ya que significa que la lista solo posee un elemento
            }
            else {
                ListNode previous = head;
                inode = head.next;
                while (previous != null){
                    if (previous.getObject() == node.getObject()){
                        return inode.getObject();
                    }
                    else {
                        previous = inode;
                        inode = inode.next;
                    }
                }
                // Identifica el nodo y retorna el posterior 
            }
        }
        return null;
    }

    @Override
    public List subList(ListNode from, ListNode to) { // Metodo para crear una Sublista mediante dos nodos (El inicial y el final), digitado por el parametro
        List out = new List(); // Se crea la lista que posteriormente se va a retornar 
        if (!isEmpty()){
            inode = head;
            boolean itsInRange = false;
            for (int i=0; i<size; i++){
                if ( inode.getObject() == from.getObject() || itsInRange){
                    if (inode.getObject() != to.getObject()){ // Se empieza a recorrer desde el nodo ingresado , almacenando todos los nodos hasta que se tope con el segundo nodo del parametro que indica la cola
                        out.add(inode.getObject());
                        itsInRange = true;
                    }
                    else {
                        out.add(inode.getObject());
                        break; // Cuando añade la cola , se rompe el ciclo
                    }
                    inode = inode.next;
                }
            }
        }
        return out;
    }

    @Override
    public List sortList() { // Metodo para organizar la lista
        try{
            Object previous;
            Object actual;
            int cont = 0;
            do{
                inode = head;
                while(inode.next != null)
                {
                    previous = inode.getObject();
                    actual = inode.next.getObject();
                    if((previous.toString().compareTo(actual.toString()) > 0)// Mediante el metodo .compare y la transformaciones de estos a Strings , se organiza la lista iniciando con la cabeza como el Nodo principal y siguiendo con los Nodos de manera descendente hasta llegar a la cola siendo este el ultimo según la organización del metodo
                    {
                        this.remove(previous);
                        this.insertTail(previous);
                        inode = inode.next;
                    }
                    else
                    {
                        inode = inode.next; //Si el nodo por .compare resulta ser "menor" al nodo actual , se le asigne el siguiente
                    }
                }
                cont++; // El count sirve para evitar que exceda la cantidad de elementos de la lista original
            }while(cont < size);
            return null;
        }catch (Exception e){
            return null;
        }

    }

    @Override
    public Iterator<ListNode> iterator() {
        inode = head;
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return inode.next != null;
            }

            @Override
            public ListNode next() {
                if (inode != null) {
                    ListNode tmp = inode;
                    inode = inode.next;
                    return tmp;
                } else {
                    return null;
                }
            }
        };
    }

    public void rec(ListNode node) { // Metodo para recorrer la lista 
        try{
            if (node.next != null) {
                rec(node.next);
                // <- ;) ->
            }
            out.println(node.toString());
        }catch (Exception e){
            out.println("¡Ha ocurrido un error!");
        }
    }
}
