package com.company;

public class Cola implements ICola{

    /*

    Nota : Las colas por listas estan basadas especificamente en las Listas Doblemente Enlazadas , por lo cual hace que este tipo de estructura se dinámica

    */

    private List lista = new List();

    @Override
    public void clear() {
        lista.clear(); // Elimina los elementos de la cola mediante el .clear de la lista
    }

    @Override
    public boolean isEmpty() { // Utiliza el método .clear de las listas para eliminar todo tipo de contenido de la estructura
        return lista.isEmpty();
    }

    @Override
    public Object extract() { 
        Object object = lista.head.getObject(); // Almacena la cabeza de la lista en la variable object
        lista.remove(lista.head); // Remueve la cabeza de la lista (El primer elemento que ingreso)
        return object; // Retorna el elemento object
    }

    @Override
    public boolean insert(Object object) {
        lista.add(object); // Agrega el elemento ingresado en el parametro al final de la cola
        return true;
    }

    @Override
    public int size() { // Retorna el tamaño de la estructura
        return lista.getSize();
    }

    @Override
    public boolean search(Object object) {
        try{
            if (lista.search(object).getObject().toString().equals(object.toString())){ //Mediante el método .search de las listas , recorre la cola para verificar que el elemento este en la estructura
                return true;
            }else {
                return false;
            }
        }catch (Exception e){
            return false;
        }

    }

    @Override
    public void sort() { // Organiza la cola 
        lista.sortList();
    }

    @Override
    public void reverse() {
        List reverseList = new List(); // Crea la lista "Inversa"
        ListNode inode = new ListNode(); // Crea el nodo que va a recorrer la estructura
        inode = lista.tail; // Declara el nodo con el último valor ingresado a la estructura (La cola)
        for (int i = lista.getSize(); i > 0; i-- ){
            reverseList.add(inode.getObject());
            inode = inode.previous; // Recorre la estructura desde la cola aprovechando las cualidades de la lista doblemente enlazada para ir almacenando los elementos en la lista "Inversa"
        }
        lista = reverseList; // Asigan los valores de la lista "Inversa"  a la lista original
    }

    @Override
    public String toString(){
        return lista.recInicioFin(); // Utiliza el método .recInicioFin para crear una concatenación de string con los elementos de la estructura
    }
}
